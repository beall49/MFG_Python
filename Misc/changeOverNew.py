"""
    here are some test values 
    fromSku='C216669-01'; toSku='C263019'; prodLine='BK1'    
    print [str(col) for row in fromTbl for col in row ]  
    print [str(col) for row in toTbl for col in row ]



"""  

def getChangeModelAssociations(model, line):
    from system.db import runQuery as runQry
    qry = "SELECT  DISTINCT UPPER( CHG_ASC_VAL_OR_MX) AS MATRIX FROM ADMIN.V_G3_SEQ_CHG_ASC_ASSOCIATION WHERE  UPPER(MODEL_NAME) = UPPER('%s') AND UPPER(CHG_MODEL_ASC) = UPPER('%s') " % (model, line)        
    tbl = runQry(qry, "giw")
    returnList=sorted([str(row[0]) for row in tbl])
    return returnList 
    
    
def getSkuParameters(fromSku, toSku):
    from system.db import runQuery as runQry
    qry = "SELECT  ITEM_CODE, MODEL_NAME, JDE_PM_CODE, SIZE_CODE, BRAND_CODE FROM ADMIN.V_G3_SEQ_SKU_SKUUDA WHERE ITEM_CODE IN( UPPER('%s'),UPPER('%s') )" % (fromSku, toSku)
    tbl= runQry(qry, "giw")
    fromTbl=([row for row in tbl if row[0]== fromSku])
    toTbl=([row for row in tbl if row[0]== toSku])
    return fromTbl, toTbl    
    
def getSWPquery(fromTbl, toTbl, seqLine):
    ''' Builds and returns the query for Still Wine Poly resources. '''
    for row in fromTbl:
        model = fromTbl[0][1]
        fromPMparm = fromTbl[0][2]
        fromSizeParm = fromTbl[0][3] if not fromTbl[0][3] else 'Null' 
        toPMparm = toTbl[0][2]
        toSizeParm = toTbl[0][3] if not toTbl[0][3] else 'Null' 
        swpSizeMatrix = "MAJ2_4_SIZE_ASCMTX"
        swpPmMatrix = "MAJ2_4_PM_ASCMTX"
        swpSkuAscVal = "MAJ_2_4_SKU_ASCVAL"
        
        query = """SELECT
                  B.DUR,
                  B.DUR_UNITS
                FROM
                  ADMIN.V_G3_SEQ_CHG_ASC_ASSOCIATION A,
                  ADMIN.V_G3_SEQ_CHG_ASC_MATRIX      B
                WHERE
                  A.MODEL_NAME                   = '%s' AND
                  A.MODEL_NAME               = B.MODEL_NAME AND
                  A.CHG_MODEL_ASC         = '%s' AND
                  A.CHG_ASC_VAL_OR_MX = B.CHG_ASC_MX_NAME AND
                  B.CHG_ASC_MX_NAME     = '%s' AND
                  B.ROW_STR                     = '%s' AND
                  B.COL_STR                     = '%s'
                GROUP  BY
                  B.DUR,
                  B.DUR_UNITS
                  
                UNION
                
                SELECT
                  B.DUR,
                  B.DUR_UNITS
                FROM
                  ADMIN.V_G3_SEQ_CHG_ASC_ASSOCIATION A,
                  ADMIN.V_G3_SEQ_CHG_ASC_MATRIX      B
                WHERE
                  A.MODEL_NAME                   = '%s' AND
                  A.MODEL_NAME               = B.MODEL_NAME AND
                  A.CHG_MODEL_ASC         = '%s' AND
                  A.CHG_ASC_VAL_OR_MX = B.CHG_ASC_MX_NAME AND
                  B.CHG_ASC_MX_NAME     = '%s' AND
                  B.ROW_STR                     = '%s' AND
                  B.COL_STR                     = '%s'
                GROUP  BY
                  B.DUR,
                  B.DUR_UNITS
                  
                UNION
                
                SELECT
                  B.CHG_DUR,
                  B.CHG_DUR_UNITS
                FROM
                  ADMIN.V_G3_SEQ_CHG_ASC_ASSOCIATION A,
                  ADMIN.V_G3_SEQ_CHG_ASC_VALUE       B
                WHERE
                  A.MODEL_NAME           = '%s' AND
                  A.MODEL_NAME       = B.MODEL_NAME AND
                  A.CHG_MODEL_ASC = '%s' AND
                  B.CHG_ASC_VAL_NAME LIKE '%s'
                GROUP  BY
                  B.CHG_DUR,
                  B.CHG_DUR_UNITS 
                  
                    """ % (model, seqLine, swpPmMatrix, fromPMparm, toPMparm, model, seqLine, swpSizeMatrix, toSizeParm, fromSizeParm, model, seqLine, swpSkuAscVal)

    return ''.join(query)     
    
    
def getR90query(fromTbl, toTbl, seqLine):
    ''' Builds and returns the query for BK1
        none of these are being used
        model = fromTbl[0][1]
        fromPMparm = fromTbl[0][2]
        fromBrandParm = fromTbl[0][4]
        toPMparm =toTbl[0][2]
        toBrandParm = toTbl[0][4]
    '''
    for row in toTbl:
        #put loop in here to protect the keyErr
        r90BrandMatrix = "bkr90_caps_brand_ascmtx"
        r90BrandAscVals = "%brandascval"
        r90SkuAscVals = "%skuascval"    
        fromBrand = fromTbl[0][4]
        toBrand = toTbl[0][4]
        brandQuery=""
        query = """SELECT
                      B.CHG_DUR,
                      B.CHG_DUR_UNITS
                    FROM
                      ADMIN.V_G3_SEQ_CHG_ASC_ASSOCIATION A,
                      ADMIN.V_G3_SEQ_CHG_ASC_VALUE       B
                    WHERE
                      A.MODEL_NAME = 'bk90_caps' AND
                      A.MODEL_NAME = B.MODEL_NAME AND
                      A.CHG_MODEL_ASC IN ( 'CH_omso', 'CH_bkr90_caps' ) AND
                      B.CHG_ASC_VAL_NAME LIKE '%s'
                    GROUP  BY
                      B.CHG_DUR,
                      B.CHG_DUR_UNITS  
                """ % r90SkuAscVals    
        # Check to see if the brands are different; 
        #if different we need to check the existing brand matrix and attribute value entries 
        #in the query.
        if fromBrand != toBrand:
            brandQuery = '''UNION
                        SELECT
                          B.CHG_DUR,
                          B.CHG_DUR_UNITS
                        FROM
                          ADMIN.V_G3_SEQ_CHG_ASC_ASSOCIATION A,
                          ADMIN.V_G3_SEQ_CHG_ASC_VALUE       B
                        WHERE
                          A.MODEL_NAME = 'bk90_caps' AND
                          A.MODEL_NAME = B.MODEL_NAME AND
                          A.CHG_MODEL_ASC IN ( 'CH_omso', 'CH_bkr90_caps' ) AND
                          B.CHG_ASC_VAL_NAME LIKE '%s'
                        GROUP  BY
                          B.CHG_DUR,
                          B.CHG_DUR_UNITS
                        UNION
                        SELECT
                          B.DUR,
                          B.DUR_UNITS
                        FROM
                          ADMIN.V_G3_SEQ_CHG_ASC_ASSOCIATION A,
                          ADMIN.V_G3_SEQ_CHG_ASC_MATRIX      B
                        WHERE
                          A.MODEL_NAME               = 'bk90_caps' AND
                          A.MODEL_NAME           = B.MODEL_NAME AND
                          B.CHG_ASC_MX_NAME = '%s' AND
                          B.ROW_STR                 = '%s' AND
                          B.COL_STR                 = '%s'
                        GROUP  BY
                          B.DUR,
                          B.DUR_UNITS''' % (r90BrandAscVals, r90BrandMatrix, fromBrand, toBrand)
                          
    return ''.join(query + "\n" + brandQuery)    
                                    
                                    
def getChangeOverTime(fromItem, toItem, prodLine):
    import org.apache.log4j.Logger as log
    from system.db import runQuery as runQry
    from app.changeOverTest import getSkuParameters as skuParms, getSWPquery as SWP, getR90query as R90;
    changeOverTime = 0
    qry="SELECT 1, 'H'"
    
    # Business decision to "hard code" changeover time to 2 hours for BK1.
    if prodLine == "BK2":  
        return 120
    
    lineLookupDict = {"BK1": "'CH_omso'", "BK2": "'CH_bkr90_caps'", "MAJ1":"CH_MAJ1", "MAJ2": "'CH_MAJ2'", "MAJ3":"CH_MAJ3", "MAJ4": "'CH_MAJ4'","FB1":"CH_FIRE","FB2":"CH_FIRE","COATER 1":"CH_SIZING",
                      "COATER 2":"CH_SIZING","PRINTER 1":"CH_PRINTING","PRINTER 2":"CH_PRINTING","PRINTER 3":"CH_PRINTING","PRINTER 4":"CH_PRINTING","PRINTER 5":"CH_PRINTING",
                      "PVC8":"CH_PVC8","PVC9":"CH_PVC9","PVC10":"CH_PVC10","PVC11":"CH_PVC11","PVC13":"CH_PVC13","PVC14":"CH_PVC14","PVC15":"CH_PVC15"}
    seqLine = lineLookupDict[prodLine]
    
    fromTbl, toTbl = skuParms(fromItem, toItem)
    
    if len(fromTbl) == 0: 
        return -1
        
    if fromTbl[0][1] == 'still_wine_poly':   
        qry = SWP(fromTbl, toTbl, seqLine)
        
    if fromTbl[0][1] == 'bk90_caps':  
        qry= R90(fromTbl, toTbl, seqLine)
        
    print qry
    tbl = runQry(qry, "giw")
    
        
    for row in tbl:       
        duration = row[0] * 60 if str(row[1]).upper() == "H" else row[0]      
        if duration > changeOverTime: 
            changeOverTime = duration
    
    return changeOverTime                                        