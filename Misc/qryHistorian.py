def getDates():
	from java.util import Calendar
	cal = Calendar.getInstance()
	startTime= cal.getTime()
	cal.add(Calendar.MINUTE, -60)
	return startTime, cal.getTime()
	
	
endDate, startDate = getDates()	
#Put your comma seperated path to tag in historian here
paths=['[historian]BK1/Run/OEE']
#Get tags in pydataset
tbl= system.dataset.toPyDataSet(system.tag.queryTagHistory(paths, startDate, endDate,0,"LastValue", "Wide",[],0,0,0,0 ))

#loop through
for row in tbl:
	print row[0], row[1]
