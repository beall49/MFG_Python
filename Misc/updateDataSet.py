from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs, getColumnHeaders as getHeaders

#root container, this is just easier for me, it's cleaner
root = system.gui.getWindow('YOUR WINDOW PATH HERE').getRootContainer() 

#chart object
tblChart=root.getComponent('Pie Chart')

#get the column headers
headers= getHeaders(tblChart.data)

#put the dataset into a pyDataSet
tbl= toPyDs(tblChart.data) 

#new dataset
newDs=[]

#new value
val = 30

#loop through every row in the chart
for row in tbl:
	#if it's the one you want to update, 
	#add it to the new dataset manually
	if row[0]=='Out of Tolerance':
		newDs.append([str(row[0]), val])
	else:
		#else just keep appending the old ds to the new
		newDs.append([str(row[0]),row[1]])
		
		
#put the new dataset into the chart		
tblChart.data = toDs(headers, newDs)
		
		
		