#import system, app
import xml.dom.minidom

url = "http://news.google.com/news?output=rss"

header = ["Published", "Title", "Link", "Description"]
data = []
 
try:
	response = system.net.httpGet(url).encode("ascii", "ignore")
	
	# Put it in the dom
	dom = xml.dom.minidom.parseString(response)
	
	for node in dom.getElementsByTagName("item"):
		title = node.getElementsByTagName("title")[0].firstChild.data
		link = node.getElementsByTagName("link")[0].firstChild.data
		pubDate = node.getElementsByTagName("pubDate")[0].firstChild.data
		description = node.getElementsByTagName("description")[0].firstChild.data
		description = description.replace("img src=\"//", "img src=\"http://")
		description = description.replace("<img alt=\"\" height=\"1\" width=\"1\" />", "")
		data.append([pubDate, title, link, description])
		
	print data
except:
	pass

json=(system.util.jsonDecode(system.net.httpGet("http://modapp254/Services/Service1.svc/getOEE/Printer%201").replace("[","").replace("]","")))

print json["OEEA"]