"""
    You need the selenium driver installed for this
    https://pypi.python.org/pypi/selenium#downloads
"""
from selenium import webdriver
from selenium.webdriver import Chrome, ChromeOptions
from random import randint
import time, datetime


class TimeCard:
    def __init__(self):
        self.host = "http://eta.ejgallo.com/wfc/"
        self.inputs = ["T0R0C5", "T0R1C5", "T0R2C5", "T0R3C5", "T0R4C5", "T0R7C5", "T0R8C5", "T0R9C5", "T0R10C5", "T0R11C5"]
        self.outputs = ["T0R0C7", "T0R1C7", "T0R2C7", "T0R3C7", "T0R4C7", "T0R7C7", "T0R8C7", "T0R9C7", "T0R10C7", "T0R11C7"]
        options = ChromeOptions()
        options.add_argument("--test-type")
        self.browser = Chrome(chrome_options=options)
        self.browser.set_window_size(1600, 960)
        self.browser.set_window_position(100, 50)
        self.byId = self.browser.find_element_by_id
        self.byTag = self.browser.find_element_by_tag_name
        self.byXpath = self.browser.find_element_by_xpath
        self.byName = self.browser.find_element_by_name
        self.byClass = self.browser.find_elements_by_class_name

    def main(self):
        """
            handling seasonal hours
        """
        hour = "6" if 9 > datetime.datetime.now().month > 5 else "5"
        self.browser.get(self.host + "logonWithUID")
        self.browser.get(self.host + "applications/suitenav/navigation.do?ESS=true?redirect=/wfc/applications/wtk/html/ess/timestamp.jsp")
        time.sleep(1)
        home = self.byXpath('//*[@id="returnHome"]')
        home.click()
        time.sleep(1)

        """
            you can either find your id, in the table by inspecting the HTML under #document
            #self.browser.get(self.host + "applications/mss/html/portal-launch.jsp?url=-1388235746&from=home")
            or do below, which is traversing the iframe\contenpane\link
        """
        content = self.byId("contentDiv")
        self.browser.switch_to.frame(content.find_element_by_id('contentPane'))

        link = self.browser.find_element_by_link_text('My Timecard')
        link.click()



        """
            if you need to input for last week just uncomment this
        """
        from selenium.webdriver.support.ui import Select
        select = Select(self.byName("timeperiodId"))
        select.select_by_value("0")

		#put in try pass in case of holiday
        for inputDay in self.inputs:
            try:
                text = self.byName(inputDay)
                text.click()
                entry = "7:0" + str(randint(0, 5))
                text.clear()
                text.send_keys(entry)
            except:
                pass

        for outputDay in self.outputs:
            try:
                text = self.byName(outputDay)
                text.click()
                entry = "1" + ("1" if hour == "6" and any([outputDay == self.outputs[-1], outputDay == self.outputs[4]]) else hour) + ":0" + str(randint(0, 5))
                text.clear()
                text.send_keys(entry)
            except:
                pass
        save = self.byId("saveButton")
        save.click()

        """
            if you want to auto click the approve button uncomment it
            I don't really want it too, I want to be able to edit
            before I submit
        """

        # approve = self.byId("approveButton")
        # approve.click()

TimeCard().main()
