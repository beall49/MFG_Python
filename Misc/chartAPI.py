root = system.gui.getWindow('Main Windows/Break Machine').getRootContainer().getComponent('cntCharts')
cht = root.getComponent('chtWaste')
plot = (cht.chart.plot)
#not accesible from within the range/domain api
plot.setOutlineVisible(False)
plot.setRangeGridlinesVisible(False)

#set the axes
range = plot.rangeAxis
domain = plot.domainAxis

#remove range tick marks, labels, and lines
range.setTickMarksVisible(False)
range.setTickLabelsVisible(False)
range.setAxisLineVisible(False)

#remove value lables and tick marks
domain.setAxisLineVisible(False)
domain.setTickLabelsVisible(False)

#how to set a series color
plot.getRendererForDataset(plot.getDataset(0)).setSeriesPaint(0, (someColor))



###LABEL STUFFS
from org.jfree.chart import labels
from org.jfree.ui import TextAnchor
from org.jfree.chart.labels import CategoryItemLabelGenerator, ItemLabelPosition

class CustomCategoryItemLabelGenerator(CategoryItemLabelGenerator):
   def generateLabel(self,dataset,row,column):
      return "%s" % (column)
#      return "CUP #: %s = %d %%"%(dataset.getRowKey(row),(dataset.getValue(row,column)))
      

      
root = system.gui.getWindow('Main Windows/Break Machine').getRootContainer()
chart = root.getComponent('cntCharts').getComponent('chtWaste')
plot = (chart.chart.plot)
plot.domainAxis.setTickLabelsVisible(False)
renderer = chart.getChart().getPlot().getRenderer()

renderer.setBaseItemLabelGenerator(CustomCategoryItemLabelGenerator())
x = labels.ItemLabelAnchor.CENTER
y = TextAnchor.BOTTOM_CENTER

position = labels.ItemLabelPosition(x,y)
renderer.setBasePositiveItemLabelPosition(position, True)
#renderer.setPositiveItemLabelPositionFallback(position)