from java.awt import Dimension
windows= system.gui.getOpenedWindows()

"""
	This can be used if you'd like to be able to 
	grow and shrink a left side nav bar
"""
nav = system.gui.getWindow('Navigation')
d = nav.getSize()
newWidth = 25 if d.width == 200 else 200
event.source.text = ">>" if newWidth==25 else "<<<"

nav.resize(newWidth, d.height)

for window in windows:
	if window.name!='Navigation':
		d = window.getSize()
		window.setLocation(newWidth,0)
		window.setSize(d.width + (175 if newWidth==25 else -175), d.height)

			
