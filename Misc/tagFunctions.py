"""
	this is just a collection of small scripts I've used to 
	play with tags progamatically

"""

#get line names from tags
from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs
badLines=['RTO Closure', 'RTO Label', 'Sim_BK1', 'Sim_BK2', 'Sim_Printer1', 'Utilities_Closure', 'Utilities_Label', 'Common']

root=system.gui.getWindow('Temps').getRootContainer()

tags=toPyDs(toDs([['Lines']], sorted([[str(tag.name)] for tag in system.tag.browseTags("[G3Closure]") if str(tag.name) not in badLines])))





#copy and paste (not done, couldn't figure out how to do expressions, plus tag.type sucks)
lines=["PVC 13","PVC 14","PVC 15","Majer 1","Majer 2","Majer 3","Majer 4"]
tagTypes={'Folder':'Folder', 'DB':"Memory" }
mainTags = system.tag.browseTags(parentPath="[G3Closure]PVC 12/PLC/Boxing", recursive=True) 
mainLine="PVC 12"
for line in lines:
    for tag in mainTags:
        parentPath=(tag.fullPath).replace("/" + tag.name, "").replace(mainLine, line)
            if tag.isFolder():                    
                print tag.type                    
#                system.tag.addTag(parentPath=parentPath, name=tag.name, tagType="Folder")
            else:
                if tag.isExpression():
                    tagType='EXPRESSION'
                elif tag.isMemory():
                    tagType='MEMORY'
                    
                    
#if not system.tag.exists(parentPath):        


#remove folder
import system, app
lines=["Majer 3","Majer 4","Majer 1","Majer 2","PVC 14","PVC 15","PVC 12","PVC 13"]      
for line in lines:
    path = "[G3Closure]" + str(line) +"/PLC/Boxing"
    system.tag.removeTag(path) 

#adding tags
import system, app
path = "[G3Closure]"
tags = system.tag.browseTags(path)
for tag in tags:
    tagPath=path + "/" + tag.name + "/Factors/"

    try:
        system.tag.addTag(parentPath=tagPath, name="Electrician", tagType="MEMORY", dataType="String", value="")
    except:
        print tagPath    
        
        
#remove tags        
import system, app
path = "[G3Closure]"
tags = system.tag.browseTags(path)
for tag in tags:
    tagPath=path + "/" + tag.name + "/Factors/Engineer"
    
    try:
        system.tag.removeTag(tagPath)
    except:
        print tagPath   
        
#rename a tag in a folder structure
import system, app
path = "[G3Closure]"
tags = system.tag.browseTags(path)
for tag in tags:
    tagPath=path + "/" + tag.name + "/Factors/"
    
    try:        
        #add the new tag
        system.tag.addTag(parentPath=tagPath, name="Electrician", tagType="MEMORY", dataType="String", value="")
        
        #remove the old tag
        system.tag.removeTag(tagPath = "/Engineer")
    except:
        print tagPath                      


lines=["BK1","BK2","Printer 1","Printer 2","Printer 3","Printer 4","Printer 5","FB 1","FB 2","Coater 2","Coater 2"]

for line in lines:
    tags = system.tag.browseTags("[G3Closure]" + str(line) +"/Util")
    for tag in tags: 
        if tag.name=='checkList':
            system.tag.write(tag.path,"")
        


def recursiveFolderSearch(folderName):
    print 'do something recursively'                
                
tags = system.tag.browseTags("[G3Closure]")

for tag in tags:
    if tag.isFolder:
        recursiveFolderSearch("[G3Closure]\\" + tag.name)
        
        
path = "[G3Closure]BK1/Util"
tags = system.tag.browseTags(path)
for tag in tags:
    print tag.name
            
#Example 1: Browse every OPC server
 
tags = system.opc.browse()
for row in tags:
    print row.getOpcServer(), row.getOpcItemPath(), row.getType(), row.getDisplayName(), row.getDisplayPath(), row.getDataType()
 
#Example 2: Browse Ignition OPC-UA
 
tags = system.opc.browse(opcServer="Ignition OPC-UA Server")
 
#Example 3: Browse Specific Device
 
tags = system.opc.browse(opcServer="Ignition OPC-UA Server", device="Dairy Demo Simulator")
 
#Example 4: Browse Specific Folder Path (not OPC item path)
 
tags = system.opc.browse(opcServer="Ignition OPC-UA Server", folderPath="*Overview/AU 1*")        


devices = system.dataset.toPyDataSet(system.device.listDevices())
for device in devices:
print device[0]
#device="CORK_COATER2",    
tags = system.opc.browse(opcServer="Ignition OPC-UA Server",   folderPath="CORK_COATER2/Global/RecordedBatch*")    
print len(tags)



from __future__ import with_statement
import re
def getTagVal(path):
	if system.tag.exists(path):
		return system.tag.getTagValue(path) == True
	
	return False
'''
	loop through alarms looking for faults
'''
def findDownTimeReason():
	tagPath = "[default]Breaker/Alarms/BreakerAlarm"        
	tags = system.tag.browseTags(tagPath)
	tagList = []
	
	for tag in tags:
		if tag.isFolder:
			newPath = tagPath + "/" + tag.name 
			opcPath = "ns=4;s=BreakerAlarm." + tag.name +".bFaultIn"
			words =  re.findall('[A-Z][^A-Z]*', tag.name)
			if getTagVal(newPath + "/bEnable") == True :
				tagList.append([opcPath, ' '.join(word for word in words)])
	return tagList
					

tags = findDownTimeReason()
for tag in tags:
	if tag[1]not in ["Alarms","Washer Alarm1","Washer Alarm2"]:
		system.tag.addTag(parentPath="[default]Breaker/Alarms/", 
							name=str(tag[1]).replace("Alarm ","") , 
							tagType="OPC", 
							dataType="Boolean",
							attributes={"OPCServer":"BreakRoom", "OPCItemPath":tag[0]})
#with open("C:\\tags.txt", "w") as f:
#	f.write(',\n'.join(str(t) for t in ts))
#	f.close ()	


def findDownTimeReason():
	tagPath = "[default]Breaker/Alarms/"        
	tags = system.tag.browseTags(tagPath)
	for tag in tags:
		if not tag.isFolder():
			print tagPath + tag.name
			
findDownTimeReason()			