#!/usr/bin/python
# -*- coding: utf-8 -*-
from system.dataset import toPyDataSet as toPyDs
from system.gui import getWindow as form
from system.db import runPrepQuery as qry
from system.production.utils import getProductCodeLineInfo as getPCLI

root = form('Main Windows/Integration').getRootContainer()
data = toPyDs(root.Schedule)
productCodes = {}

for (idx, row) in enumerate(data):
    productCodes[str(row[0]).rstrip()] = str(row[17]).rstrip()

for (sku, jdeLineNumber) in productCodes.items():
    lineName = ''.join(qry("SELECT ISNULL((SELECT TOP 1 MesLineName FROM [mes_runtime].[dbo].[G3_LineLookup] where JdeProductionLine = ? ), '') ", [jdeLineNumber])[0][0])
    tbl = toPyDs(getPCLI(True, sku, lineName))
    try:
        id = (tbl[0][5], sku, lineName)
    except IndexError:
        print 'PROBLEM CHILD!!!!!!!!!!!!!', sku, lineName, jdeLineNumber
    continue