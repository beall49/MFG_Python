def jde_time_conversion(jdeTime):
    import time, system    , app
    returnTime={0:"00:00:00",1:"00:00:0" + str(jdeTime), 2:"00:00:" + str(jdeTime), 3:"00:0" + str(jdeTime)[:1] + ":" + str(jdeTime)[-2:], 4: "00:" + str(jdeTime)[:2] + ":" + str(jdeTime)[-2:], 5: "0"+str(jdeTime)[:1]+":" + str(jdeTime)[1:-2] + ":" + str(jdeTime)[-2:], 6: str(jdeTime)[:2]+":" + str(jdeTime)[2:-2] + ":" + str(jdeTime)[-2:]}
    return returnTime.get(len(str(jdeTime)),"00:00:00")

def checkSchedules(lineID, date):
    import system , app
    query = "SELECT COUNT(ID) FROM SCHEDULE WHERE FINISHDATETIME >= '%s' AND LineID = %s" %(date.strftime("%Y-%m-%d %H:%M:%S"), lineID)
    return system.db.runScalarQuery(query)
  
def getIdForWorkOrder(workOrder):
    import system , app
    query = "SELECT ID FROM WORKORDER WHERE WORKORDER = ?"
    results = system.db.runPrepQuery(query,[workOrder])
    return results[0][0]  if len(results)>0 else -1        

def getWorkOrderStatus(workOrder):
    import system , app
    query = "SELECT CLOSED FROM WORKORDER WHERE WORKORDER = ?"
    results = system.db.runPrepUpdate(query,[workOrder])
    return results[0][0]  if len(results)>0 else -1        

def addMsgToWorkOrderIn(msg, id):
    import system , app
    query = """UPDATE  G3_WORKORDERS_IN SET PROCESSED = 'Y', PROCESSEDMSG = ?, PROCESSEDDATETIME = CURRENT_TIMESTAMP WHERE ID = ?""" 
    system.db.runPrepUpdate(query,[msg, id])
    
def createOrUpdateProduct(productCode, description):
    import system , app
    tbl = app.Utils.runStoredProc([["PRODCODE", "VARCHAR", productCode],["INCOMINGDESCR", "VARCHAR", description]], "[MES_RUNTIME].[DBO].[USP_GET_PRODUCT_CODE_STATUS]")    
    #if there are no results from the above query, the product code doesn't exist and it will be added, otherwise the sp will handle the update piece
    if  tbl[0][0]==0 : system.production.utils.addproductCode(productCode, description) 
    
def resetLineCache():
    import system, app            
    qry="SELECT L.ID FROM [MES_RUNTIME].[DBO].[LINE] L, ( SELECT [NAME], MAX( [TIMESTAMP] ) TS FROM [MES_RUNTIME].[DBO].[LINE] L GROUP BY NAME ) LINES WHERE L.NAME = LINES.NAME AND L.TIMESTAMP = LINES.TS ORDER BY 1"
    tbl=system.db.runPrepQuery(qry)
    for row in tbl:
        system.schedule.resetScheduleCache(row[0])
    
def schedulesExistForWorkOrderId(workOrderId):
    import system , app
    query = "SELECT COUNT(*) FROM SCHEDULE WHERE WORKORDERID= ?"
    results = system.db.runPrepQuery(query, [workOrderId])
    return True if results[0][0] > 0 else False

def addProductLineProperty(linePath, productCodeLineId, rate, dispUOM, convUOM):
    import system , app
    parms=[["LINEPATH","VARCHAR",  linePath],["productCodeLINEID", "INTEGER",productCodeLineId],["RATE","VARCHAR", rate], ["DISPUOM","VARCHAR",dispUOM],["CONVERSTIONDISPLAY","VARCHAR",convUOM] ]
    app.Utils.runStoredProc(parms, "[MES_RUNTIME].[DBO].[USP_SET_PROD_LINE_PROPERTY]")        

def addNewSchedule(lineId, woId, workorderRate, startTimeStr, schedChangeOverInMins, changeOverHandling, quantity, Note, userId, finishTimeStr=""):
    import system,  app
    from java.util import Calendar
    from java.lang import String
    from datetime import datetime, timedelta

    schedDuration = int(quantity / workorderRate)

    if changeOverHandling == 0:
            RunStartDateTimeFormatted = datetime.strptime(startTimeStr, "%Y-%m-%d %H:%M:%S")  
            StartDateTimeFormatted= (datetime.strptime(startTimeStr, "%Y-%m-%d %H:%M:%S") + timedelta(minutes=( -1 * (schedChangeOverInMins))))
            RunFinishTimeFormatted= (datetime.strptime(finishTimeStr, "%Y-%m-%d %H:%M:%S") + timedelta(minutes=(schedChangeOverInMins))) if finishTimeStr!="" else (RunStartDateTimeFormatted + timedelta(minutes=( schedDuration)))
    else:
            StartDateTimeFormatted = datetime.strptime(startTimeStr, "%Y-%m-%d %H:%M:%S")  
            RunStartDateTimeFormatted= (datetime.strptime(startTimeStr, "%Y-%m-%d %H:%M:%S") + timedelta(minutes=(schedChangeOverInMins)))    
            RunFinishTimeFormatted= (datetime.strptime(finishTimeStr, "%Y-%m-%d %H:%M:%S") + timedelta(minutes=(schedChangeOverInMins))) if finishTimeStr!="" else (RunStartDateTimeFormatted + timedelta(minutes=( schedDuration)))

    parms=[
                    ["LINEID"    ,      "INTEGER"        ,  lineId],
                    ["WOID"  ,                   "INTEGER", woId],
                    ["NOTE"       ,            "VARCHAR",     Note],
                    ["STARTDATETIMEFORMATTED"   ,"VARCHAR", StartDateTimeFormatted],
                    ["FINISHTIMEFORMATTED",      "VARCHAR",    RunFinishTimeFormatted],
                    ["QUANTITY",               "INTEGER"  ,    quantity],
                    ["ENTEREDBY",                "VARCHAR",    userId],
                    ["RUNSTARTDATETIMEFORMATTED","VARCHAR",    RunStartDateTimeFormatted]
                    ]
    app.Utils.runStoredProc(parms, "[MES_RUNTIME].[DBO].[USP_INSERT_NEW_SCHEDULE_ENTRY]")

def pollWorkOrderIn():
    from org.apache.log4j import Logger
    import system,  app
    prevOrder = "";     
    currOrder = "";   
    txId = system.db.beginTransaction("mes_runtime",system.db.READ_UNCOMMITTED,timeout=5000)    
    tbl = app.Utils.runStoredProc([], "[MES_RUNTIME].[DBO].[USP_GET_INTEGRATION_POLL_IN]")
    log = Logger.getLogger("app.Integration.pollWorkOrderIn")
    log.info( "Polling Work Order In Table" )
    #for each work order
    for row in tbl:            
        #get the required elements    
        id = row["ID"]
        workorder = row["WORKORDER"]
        productCode = row["FULLSKU"]
        description = row["FULLSKUDESC"]
        linePath = row["MESLINEPATH"]
        lineName = row["MESLINENAME"]
        quantity = row["EACHESTOPRODUCE"] 
        conversionToDisplay = row["EACHESTODISPLAYUOM"]
        dispUom = row["DISPLAYUOM"]
        startdatetime = row["STARTDATETIME"]
        changeover = row["DEFAULTCHANGEOVER"]
        schedulerate = row["SCHEDULE_RATE"]
        autoSchedule = row["AUTOSCHEDULE"]
        overrideDefaultRate = row["OVERRIDEDEFAULTRATE"]
        lineID = row["LINEID"]
        currOrder = workorder
        system.production.utils.updateproductCodeLineStatus(productCode, linePath, 1)
        productCodeLineInfo=system.dataset.toPyDataSet(system.production.utils.getproductCodeLineInfo(True, productCode, lineName));
        
        if currOrder!= prevOrder:
            productCodeID = app.Integration_new.createOrUpdateProduct(productCode, description)
            workOrderID = app.Integration_new.getIdForWorkOrder(workorder)        
            log.info("  Found a new work order: "+ workorder + " adding/updating product code " + productCode)
            #Check if work order exists, if it does, update it, if it doesn't add it            
            if workOrderID > 0:
                try:
                    #inductive prevents updates to work orders that have schedule entries against them. 
                    system.schedule.updateWorkOrderEntry(workOrderID, workorder, productCode, quantity)
                    log.info("updated work order")                
                except:
                    log.info("failed updating work order")
                    app.Integration_new.addMsgToWorkOrderIn("  Error updating work order entry, order scheduled",id)
            else:
                system.schedule.addWorkOrderEntry(workorder, productCode, quantity)
                log.info("added new work order ")
                workOrderID = app.Integration_new.getIdForWorkOrder(workorder)            

        productCodeLineId = productCodeLineInfo[0][5]  if len(productCodeLineInfo) >0 else  0 #if there's an id, get it otherwise set it to zero
        log.info(str(productCodeLineId))
        rate = 0
        #if the product code has a line id, set the Schedule Rate and the product outfeed properties
    
        if productCodeLineId==0:
            log.info("NO PCL for product code %s and line name of %s" %( productCode, lineName)) 
        else:
            ##if override the default rate is specified, then take the rate from JDE else go out and get the default rate from the model and assign it
            if overrideDefaultRate == 'Y':             
                rate= schedulerate    
            else:
                properties = system.dataset.toPyDataSet(system.production.utils.getproductCodeLinePropertiesInfo(productCodeLineId, True))
                rate= ([str(row[3]) for row in properties if row[2]==('Schedule Rate')])[0]
    
            #not sure what to do here, should I not add it becuase the PCLid might be zero or change the way that pclId piece works
            app.Integration_new.addProductLineProperty(linePath, productCodeLineId, rate, dispUom, conversionToDisplay)
    
        ###########################
        #    if the Auto Schedule Flag = 'Y' then schedule the work order, else do nothing, the auto schedule flag was added to support the cork room scheduling process
        #    cork order are deliverd to the system, loaded and left unscheduled. 
        ##########################
        if autoSchedule == 'Y':
            #check to see if a schedule entry already exists for the item
            if  app.Integration_new.schedulesExistForWorkOrderId(workOrderID)==False:                 
                app.Integration_new.addNewSchedule(lineID, workOrderID, schedulerate ,startdatetime, changeover, 0, quantity, "", "integration" )                
            else:
                app.Integration.addMsgToWorkOrderIn("A schedule entry exists for this order, work order updated, schedule entry not updated", id)
    
        '''make a transaction to process all of the workorders at the same time'''        
        prevOrder = currOrder        
    system.db.runUpdateQuery("UPDATE G3_WORKORDERS_IN SET PROCESSED = 'Y',PROCESSEDDATETIME = CURRENT_TIMESTAMP WHERE PROCESSEDDATETIME IS NULL")
    resetLineCache()    
     


        
        
def callingMethod():
    from datetime import datetime
    print datetime.now()
    txId = system.db.beginTransaction("mes_runtime",system.db.READ_UNCOMMITTED,timeout=5000)  
    schedule = event.source.parent.getComponent('Table').data
    pds = system.dataset.toPyDataSet(schedule)
    
    curTime = datetime.now()
    workOrderList = ""
    qry="""
                    INSERT INTO G3_WORKORDERS_IN
                    (
                        FULLSKU,FULLSKUDESC,WORKORDER,QUANTITYTOPRODUCE,UOM,EACHESTOPRODUCE,EACHESTODISPLAYUOM,DISPLAYUOM,CHANGEOVERDURATIONINMIN,STARTDATE,
                        STARTTIME,ENDDATE,ENDTIME,PRODUCTIONRESOURCE,PRODUCTIONMETHOD,SOPFAMILY,SUPPLYFAMILY,JDEPRODUCTIONLINE,SCHEDULINGRATEPERHOUR,
                        TRANSACTIONDATETIME,PROCESSED,PROCESSEDDATETIME,PROCESSEDMSG
                    )
                    VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,NULL,NULL) 
                    """
    for item in pds:
        sku =  str(item["SKU"]).strip()
        desc = str(item["SKU_DESC"]).strip()
        workorder =  str(item["WORK_ORDER"]).strip()
        qtyToProduce = item["WO_QTY"]
        uom =  str(item["PRD_UOM"]).strip()
        convToDisplay = str(item["EA_TO_DISPLAY"])
        displayUom = item["DISP_UOM"]
        eaToProduce = item["EA"]
        coDuration = 0 #vestigial, this will be set when the integration script fires, 
        #based on a lookup against the G3_LineLookup table. Eventually this will migrate to a lookup against the JDA changeover matrix
        ##skip lowest uom will always be EA
        startDate = item["START_DATE"]
        startTime = int(item["START_TIME"])
        endDate = item["END_DATE"]
        endTime = int(item["END_TIME"])
        prodRsc =  str(item["PRD_RSC"]).strip()
        prdMthd =  str(item["PRD_MTHD"]).strip()
        #skipping QQ@SZC
        supFmly =  str(item["imprp6"]).strip()
        sopFmly = str(item["imsrp5"]).strip()
        prdLne =  str(item["RATE_LINE"]).strip()
        casesPerHour = item["RATE"]
        transactionDateTime = str(curTime)[:-7]
        
        strStartTime = app.Integration.jde_time_conversion(startTime)
        strEndTime = app.Integration.jde_time_conversion(endTime)
            
        if str(item["LOAD"]) == 'Y':
            system.db.runPrepUpdate(qry, [sku,desc,workorder, qtyToProduce, uom, eaToProduce, convToDisplay, 
                                            displayUom, coDuration, startDate, strStartTime,  endDate, strEndTime, 
                                            prodRsc, prdMthd, sopFmly, supFmly,  prdLne, casesPerHour,transactionDateTime,"N"],tx=txId)
            workOrderList = workOrderList + "'%s',"%(workorder)
    
    system.db.commitTransaction(txId)
    system.db.closeTransaction(txId) 
    app.Integration_new.pollWorkOrderIn()
    
    workOrderList = workOrderList[:-1]
    system.nav.openWindow('Popups/Integration/WorkOrderErrors', {'WorkOrderList' : workOrderList})