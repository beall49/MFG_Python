def var(X):
    S, SS = 0.0, 0.0
    for x in X:
        S += x
        SS += x*x
    xbar = S/float(len(X))
    return (SS - len(X) * xbar * xbar) / (len(X) -1.0)

def cov(X,Y):
        n = len(X)
        xbar = sum(X) / n
        ybar = sum(Y) / n
        try: 
            return sum([(x-xbar)*(y-ybar) for x,y in zip(X,Y)])/(n-1)
        except:
            print str(n-1)
            return 0

#returns the Standardized coefficient of a series of values
def beta(y):
	x = range(1,len(y)+1)
	return cov(x,y)/var(x)		

def setDirection(val):
	from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs, getColumnHeaders as getHeaders
	root = self
	#manual list of oee averages
	data= toPyDs(root.oeeVals)
	headers = getHeaders(root.oeeVals)
	tempList = []
	#current direction for short circuit
	flag = root.direction
	
	#if there's no data
	if sum(int(row[0]) for row in data if row[0]>0) <1:
		#insert just this row
		ds = toDs(headers, [[val]])
		root.oeeVals = ds
	else:	
		tempList.extend([val])
		#only the last 30 minutes of averages
		tempList.extend ([int(row[0]) for indx, row in enumerate(data) if row[0]>0 and indx< 30])			
		#if that list >0
		if sum(tempList)>0: 
			#call the algo
			flag = 1 if project.stats.beta(tempList)> 1 else ( -1 if project.stats.beta(tempList)<0 else 0)
		else:
			flag = 0
		#set the oeevals
		root.oeeVals = toDs(headers, ([[i] for i in tempList]))

	root.direction = flag


if event.propertyName=='oeeDataSet':
	# get data from historical tag
	tbl= system.dataset.toPyDataSet(event.source.oeeDataSet)
	#sum the values
	val = sum(row[1] for row in tbl)
	#make the avg
	val = val/len(tbl) if val > 0 else 0
	#to an int
	val = int(val*100)
	#arrow direction
	event.source.setDirection(val)
	
	#set value
	event.source.getComponent('root').getComponent('lblOEE').text = str(val)		