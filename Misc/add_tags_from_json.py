from __future__ import with_statement
import ast
path = "C:\\projects\\workScripts\\downtime_reasons.json"
tag_path = "[default]Breaker/DownTime"
with open(path) as json_file:
	downtime_reasons =  json_file.readlines()

tags = {}
count = 0

for row in downtime_reasons:
	tags[count] = ast.literal_eval(row)
	count +=1


for k, v in tags.iteritems():
	array_number = int(v['array_number'])
	bit = int(v['bit'])
	string_reason = v['string_reason']
	expression = "getBit({[.]DownTimeCodes/arrAlarmBreaker%i},%i)" % (array_number, bit)
	
	#print 	expression % (array_number, bit), string_reason
	system.tag.addTag(parentPath=tag_path, 
				name=string_reason, 
				tagType="EXPRESSION", 
				dataType="Boolean", 
				attributes={"Expression":expression})