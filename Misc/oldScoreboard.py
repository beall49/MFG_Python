def getGembaWaste(runID, uom):		
	from org.apache.log4j import Logger
	log = Logger.getLogger("app.Scoreboard.GembaTargets")		
	import time, datetime, system, app, math, traceback
	from java.text import SimpleDateFormat
	from java.util import Calendar

	try:
		totalWaste = 0
		
		e = system.gui.getWindow('Main Windows/Scoreboard').getRootContainer().getComponent('GembaBoard').getComponent
		
		tbl = app.Utils.runStoredProc([["RUNID", "BIGINT", runID]], "[MES_RUNTIME].[DBO].[USP_GET_WASTE_BY_RUN_BY_SHIFT]")
		for row in tbl:		
			totalWaste += (int(row[5])/uom)
			e(str(row[1])).Shift1Actual -= (int(row[5])/uom) if row[2] == 1 else 0
			e(str(row[1])).Shift2Actual -= (int(row[5])/uom) if row[2] == 2 else 0
			e(str(row[1])).Shift2Actual -= (int(row[5])/uom) if row[2] == 3 else 0
				
		return totalWaste
	
	except ValueError:
		log.error(ValueError)
		return 0
	except:
		log.error("Gemba Waste" + str(traceback.format_exc()))	
		return 0
			
#############HANDLE THE ACTUALS
def getGembaActuals(line):
	from org.apache.log4j import Logger
	log = Logger.getLogger("app.Scoreboard.GembaTargets")		
	import time, datetime, system, app, math, traceback
	from java.text import SimpleDateFormat
	from java.util import Calendar
	
	try:
		cal = Calendar.getInstance();
		f = SimpleDateFormat("EEEE")
		totalProduced 	 = 				0
		
		e = system.gui.getWindow('Main Windows/Scoreboard').getRootContainer().getComponent('GembaBoard')
		
		gembaController = e.getComponent('GembaController')
		gembaController.update()	
		
		tbl = system.dataset.toPyDataSet(gembaController.tableData)
		e.ActualDataSet = tbl
			
		product=""; conv=0;
		for row in tbl:	
			shift = row["Shift"]
			dt =  row["Shift Date"]			
			
			#############TRAP FOR NEGATIVE PROD COUNTS		
			actual = 0 if row["Line Production Count"] < 0 else row["Line Production Count"] 		
			
			#get the day name from the run date -- adds a day to the date if it's shift 3 because it puts shift 3 in the prior day otherwise			
			cal.setTime(dt)
			cal.add(Calendar.DATE, 1) if shift == 3 else cal.add(Calendar.DATE, 0)
			dayName =  f.format(cal.getTime())
			
			##############TODO: THIS IS PROBABLY NOT VERY EFFICIENT TO CALL THIS STORED PROC IN A LOOP
			#############NOT SURE HOW ELSE YOU WOULD DO IT
			if product !=row["Product Code"]:
				product = row["Product Code"]
				conv = app.Product.getUomConv(product, line)
		
			#actual = app.Product.convertToCases(product, actual, line)
			actual=round(actual/conv)
	
			totalProduced += actual
	
			#set values for actuals
			e.getComponent(dayName).Shift1Actual += actual if shift == 1 else 0 	
			e.getComponent(dayName).Shift2Actual += actual if shift == 2 else 0	
			e.getComponent(dayName).Shift3Actual += actual if shift == 3 else 0
			
		return totalProduced
	except ValueError:
		log.error(ValueError)
		return 0
	except:
		log.error("Gemba Actuals" + str(traceback.format_exc()))
		return 0
		
		
		
def getGembaTargets(periodID, line):
	try:
		from org.apache.log4j import Logger
		log = Logger.getLogger("app.Scoreboard.GembaTargets")		
		import time, datetime, system, app, math, traceback
		e = system.gui.getWindow('Main Windows/Scoreboard').getRootContainer().getComponent('GembaBoard')
		
		shiftNumber 	 = 				1; 
		dayName          = 				'Monday'
		shiftTarget      = 				0	
		########POPULATE THE TARGET COUNTS
		tbl = 	app.Schedule.getShiftTargets(periodID, line)
		
		#############I SET THIS AS A VALUE OF LAST MONDAY BECAUSE THIS IS WHERE THE NULL ERR WAS COMING FROM -SHOULD BE OVERWRITTEN BY TBL DATA
#+		e.WeekStartDateTime = tbl[0]["SHIFT_START"]
		
		#############SET TEH INSTANCE VAL TO NEXT MONDAY AT 6AM?	-SHOULD BE OVERWRITTEN BY TBL DATA
#		e.WeekEndDateTime = tbl[len(tbl)-1]["SHIFT_END"]
		
		#############LOOP THROUGH TBL (I TRY AND CALL ALL RETURN SETS TBL TO KEEP IT UNIFORM)
		for row in tbl:		
			shiftTarget = 				row["SHIFT_TARGET"]
			dayName = 					row["DAY_NAME"]
			shiftNumber = 				row["SHIFT_NUMBER"]			
			
			if shiftNumber == 1: e.getComponent(dayName).Shift1Target = shiftTarget	
			if shiftNumber == 2: e.getComponent(dayName).Shift2Target = shiftTarget 
			if shiftNumber == 3: e.getComponent(dayName).Shift3Target = shiftTarget
	
			
	except ValueError:
		log.error(ValueError)
	except:
		log.error("Gemba targets" + str(traceback.format_exc()))	
						
def getGembaValues(line):
	try:
		from org.apache.log4j import Logger
		log = Logger.getLogger("app.Scoreboard.GembaBoard")		
		import time, datetime, system, app, math, traceback
		
		#############GEMBA BOARD  PROPERTIES
		root = system.gui.getWindow('Main Windows/Scoreboard').getRootContainer()
		e = root.getComponent('GembaBoard')
			
		today 			 = 								datetime.datetime.now().strftime("%A")
		periodID 		 =								system.db.runScalarQuery ("SELECT CONVERT(NVARCHAR(8),GETDATE(), 112) AS PERIODID") 	
		currentShift 	 = 								system.tag.getTagValue(line + "/Run/Shift")
		uom				 =								root.CurrentSkuEaToCs		
		runID 			 = 								str(system.tag.getTagValue(line + "/Run/Run ID"))	
	
		#clear out targets and actuals
		for day in ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday']:		
			e.getComponent(day).Shift1Actual=0;		e.getComponent(day).Shift2Actual=0;		e.getComponent(day).Shift3Actual=0
			e.getComponent(day).Shift1Target=0;		e.getComponent(day).Shift2Target=0;		e.getComponent(day).Shift3Target=0
			
		#call target function
		app.Scoreboard.getGembaTargets(periodID,line)
		
		#call actual function
		totalProduced 	 = 		app.Scoreboard.getGembaActuals(line)								
		
		#call waste function
		totalWaste		 =		app.Scoreboard.getGembaWaste(runID, uom)
				
		#this finds the end of the currentShift time
		weekTarget = app.Schedule.getScheduledQuantity(e.WeekStartDateTime, root.CurrentShiftEndTime, line)
		
		#set vals
		e.CurrentWeekActualCases =	(totalProduced-totalWaste)
		
		e.CurrentWeekTargetCases =	weekTarget
		e.CurrentWeekDeltaCases = (totalProduced-totalWaste) - weekTarget
		
	except ValueError:
		log.error(ValueError)
	except:
		log.error("Gemba values" + str(traceback.format_exc()))	
			
def getDownTimeValues(line):
	import app, system, traceback
	from operator import itemgetter    
	from org.apache.log4j import Logger
	log = Logger.getLogger("app.Scoreboard.DowntTimeValues")    
	#SHORTEN THE COMPONENET NAMES    
	
	try:
		e = system.gui.getWindow('Main Windows/Scoreboard').getRootContainer().getComponent('DowntimeContainer')
		
		lnName = line
		
		#sort the reasons by time
		tbl = e.getComponent('DTAnalysisController').tableData
		tbl = system.dataset.sort(tbl, "Downtime Minutes", 0)
		tbl = system.dataset.toPyDataSet(tbl)
		
		#SET DEFAULTS
		minutes         = 0
		totalMinutes     = 0
		otherMinutes     = 0    
		headers         = ["Reason","Downtime Minutes"]
		data             = []
		
		#LOOP THROUGH TBL
		if len(tbl) > 0:
			for rowIdx in range(len(tbl)):
				#GET THE TOP 5 REASONS
				if rowIdx < 5:
					reason = tbl[rowIdx]["Reason"]
					minutes = tbl[rowIdx]["Downtime Minutes"]
				else :
					otherMinutes =     otherMinutes  + tbl[rowIdx]["Downtime Minutes"]
						
				#SHORTEN IF STATEMENT TO INLINE
				totalMinutes = totalMinutes + tbl[rowIdx]["Downtime Minutes"] if minutes >= 0 else totalMinutes 
				
				#MAKE NEW DATA SET
				dt = [reason, minutes]
				data.append(dt)
				
				
			#CUSTOM SORT TO MAKE THE DATA SET SORT IN THE EXPECTED ORDER...
			sortedData = data
			
			sortedData.append(["Other", otherMinutes]) if otherMinutes > 0 else None
				
			sortedData.append(["Line Total", totalMinutes])
			
			newDataSet = system.dataset.toDataSet(headers, sortedData)
			
			#INSERT INTO PARETO AND CUSTOM PROPS
			e.getComponent('DTPareto').data = newDataSet
			e.TotalDT = totalMinutes
			e.DTData = newDataSet
			
	except ValueError:
		log.error(ValueError)
	except:
		log.error("Downtime failed" + str(traceback.format_exc()))
		            
def getWasteValues(line):
	import app, system, math, traceback
	from org.apache.log4j import Logger
	log = Logger.getLogger("app.Scoreboard.WasteValues")        
	#SHORTEN COMPONENT NAMES
	try:
		root = system.gui.getWindow('Main Windows/Scoreboard').getRootContainer()
		lnName = root.lineName
		e = system.gui.getWindow('Main Windows/Scoreboard').getRootContainer().getComponent('WasteContainer')
		
		#GET TBLE DATA
		tbl = system.dataset.toPyDataSet(e.getComponent('WasteAnalysisController').tableData)
		
		#SET DEFAULTS
		uom                      =                root.CurrentSkuEaToCs
		currentWaste         =                 int(math.floor(root.currentShiftWaste)) 
		waste = 0
		totalWaste = 0
		headers = ["Cell","Cell Waste Count"]
		data = []
		
		#LOOP THROUGH DATA SET
		for row in tbl:    
			name = row["Cell Name"]
			waste = row["Cell Waste Count"]
			
			#INLINE IF IGNORE NEGATIVES
			totalWaste = totalWaste + row["Cell Waste Count"] if waste >= 0 else totalWaste    
				
			#CREATE DATA SET
			cell = [name, waste]
			data.append(cell)
		
		manualWaste = ["Manual Waste", currentWaste]
		data.append(manualWaste)
			
		#CUSTOM SORT TO MAKE THE DATA SET SORT IN THE EXPECTED ORDER...
		sortedData = app.Utils.sortMachineNames(data)
		
		sortedData.append(["Line Total", totalWaste])
		
		newDataSet = system.dataset.toDataSet(headers, sortedData)
		
		#ISNERT INTO PARETO
		e.getComponent('WastePareto').data = newDataSet
		e.TotalWaste = totalWaste
		e.WasteData = newDataSet    
		        
	except ValueError:
		log.error(ValueError)
	except:
		log.error("Waste failed " + str(traceback.format_exc()))
		        		
def getEmptyDataSet():
	import system, app
	headers 		= ["SAMPLE_DATE","TEST_VALUE_AVG"]
	data 			= []
	data.append(["", ""])
	return system.dataset.toPyDataSet(system.dataset.toDataSet(headers, data))	
		

def getQualityData(test, process):
	import system, app
	try:
		qry="""	WITH TBLTEST AS 
			(
			  SELECT
				*
			  FROM
				( SELECT    DISTINCT
					TO_CHAR( TO_DATE( '1-jan-1970' ) + TST.F_SGRP / 86400, 'mm/dd HH24:MM' ) SAMPLE_DATE,
					AVG( TST.F_VAL )           AS VALUEOFTEST,
					PRCS.F_NAME                AS PROCESSNAME,
					PART.F_NAME                AS PARTNAME,
					TEST.F_NAME                AS TESTNAME,
					TST.F_TEST                 AS TESTID,
					PRCS.F_PRCS                AS PROCESSID,
					PART.F_PART                AS PARTID,
					sqrt(COUNT( TST.F_VAL ))         AS COUNT_OF_SAMPLES
				  FROM
					SPCCLS.SGRP_TST TST
				  JOIN      SPCCLS.SGRP_INF INF ON
					INF.F_SGRP = TST.F_SGRP
				  JOIN      SPCCLS.PRCS_DAT PRCS ON
					PRCS.F_PRCS = INF.F_PRCS
				  JOIN      SPCCLS.PART_DAT PART ON
					PART.F_PART = INF.F_PART
				  JOIN      SPCCLS.TEST_DAT TEST ON
					TEST.F_TEST = TST.F_TEST
				  LEFT JOIN SPCCLS.SPEC_LIM SPEC ON
					SPEC.F_PART = INF.F_PART AND
					SPEC.F_TEST = TST.F_TEST
				  WHERE
					  TST.F_SGRP > ( ( SYSDATE - 2 ) - TO_DATE( '1-jan-1970' ) ) * 86400 AND
					  TEST.F_NAME IN (?) AND
					  PRCS.F_NAME IN (?)
				  GROUP     BY
				  TO_CHAR( TO_DATE( '1-jan-1970' ) + TST.F_SGRP / 86400, 'mm/dd HH24:MM' ),
				  PRCS.F_NAME,
				  PART.F_NAME,
				  TEST.F_NAME,
				  TST.F_TEST,
				  PRCS.F_PRCS,
				  PART.F_PART
				  ORDER     BY
				  SAMPLE_DATE DESC 
				) A
			  WHERE
				ROWNUM = 1 
			),
	
	
			TBLCONTROL AS 
			  (
				SELECT
				  MAX( F_CTRL ) AS CONTROL
				FROM
				  SPCCLS.CTRL_LIM CTRL,
				  TBLTEST T
				WHERE
				CTRL.F_PRCS = T.PROCESSID AND
				CTRL.F_PART = T.PARTID AND
          		CTRL.F_TEST =  T.TESTID  
			  )
	
	
			  SELECT
			      F_CTRL                               AS CONTROL,      
			      TRUNC(F_MEAN,3)                      AS MEAN,
			      TRUNC(F_SP,3)                        AS SIGMA,
			      TRUNC((F_MEAN - (3*(F_SP/(SELECT COUNT_OF_SAMPLES FROM TBLTEST)))),2)         AS LCL,
			      TRUNC((F_MEAN + (3*(F_SP/(SELECT COUNT_OF_SAMPLES FROM TBLTEST)))),2)         AS UCL,
			      TRUNC((SELECT VALUEOFTEST FROM TBLTEST),2) AS Test_Value_AVG,
			      (SELECT SAMPLE_DATE FROM TBLTEST) AS SAMPLE_DATE,
			      TO_CHAR( TO_DATE( '1-jan-1970' ) + CTRL.F_EFTM / 86400, 'mm/dd HH24:MM' ) AS EFFECTIVEDATE
			    FROM
			      SPCCLS.CTRL_LIM CTRL,
			      TBLCONTROL T
			    WHERE
			      CTRL.F_CTRL=T.CONTROL"""
	
		return system.db.runPrepQuery(qry,[str(test),str(process)],"infinity_qs")
	except:
		return app.Scoreboard.getEmptyDataSet()		