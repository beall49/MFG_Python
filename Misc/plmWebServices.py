import system
'''
*******************************************************************************
        how to get a document, this logs in
        and retrieves the stream
        it then creates the word doc
        need to make it do it in memory
        use this in mes
*******************************************************************************
'''


    
from __future__ import with_statement
from java.net import URL
from java.nio.file import Files, Path
import jarray, base64, subprocess
from javax.swing.text import View

#urls
host="https://g3enterprises.autodeskplm360.net"
authUrl=host+"/rest/auth/1/login"
getUrl=host+"/api/v2/workspaces/59/items/%s/files/%s" % ('159595','10924')

#put base64 of password
password='2234lkqsd0efnmfalsdkfa['

#login to the api
headers={"Content-Type" : "application/json", "Accept": "application/json", "userID" : "ryan.beall", "password" : base64.b64decode(password)}
sesh=system.util.jsonDecode(system.net.httpPost(authUrl , headers))

#get authorization
token, sessionId= sesh['customerToken'], sesh['sessionid']
output=Path
path=Files.createTempFile(None, "inductivetempfile.doc")
output=Files.newOutputStream(path)

#open conn, add headers and get stream
url=URL(getUrl)
conn=url.openConnection()
conn.setRequestProperty("Accept", "application/octet-stream")
conn.setRequestProperty("Cookie", str("customer=%s; JSESSIONID=%s" % (token, sessionId)))
input=(conn.getInputStream())

#create buffer
buffer =jarray.zeros(4096, 'b')
n=input.read(buffer)

#until it's empty
while (n!=-1):
    output.write(buffer, 0, n);
    n=input.read(buffer)
    
#close input and output    
input.close()    
output.close()


#open the file
p=subprocess.Popen('start winword "' + str(path) +'"',  shell=True)
proc=p.wait()




'''
*******************************************************************************
to find the dmsid by sku
*******************************************************************************
'''

#payload you pass over
data={
    'sort': 
        {'sortDescending': "False", 'fieldID': 'JDE_LONG_ITEM_NUMBER', 'fieldTypeID': "0"}, 
    'fields': 
        {'fieldID': 'JDE_LONG_ITEM_NUMBER', 'fieldTypeID': "0"}, 
    'filter': 
        {'fieldID': 'JDE_LONG_ITEM_NUMBER', 'fieldTypeID': "0", 'filterValue': 'C3559', 'filterType': {'filterID': "4"}}
}

import pprint, base64, itertools
host="https://g3enterprises.autodeskplm360.net"
authUrl=host + "/rest/auth/1/login"
getUrl=host   + "/api/v2/workspaces/53/items/"
searchUrl=host + "/api/rest/v1/workspaces/53/items/search" 


#auth
password='sdfasfw2e333='
headers={"Content-Type" : "application/json", "Accept": "application/json", "userID" : "ryan.beall", "password" : base64.b64decode(password)}
sesh=system.util.jsonDecode(system.net.httpPost(authUrl , headers))
token, sessionId= sesh['customerToken'], sesh['sessionid']

#request to POST
headers={"Accept" : "application/json","Content-Type" : "application/json", "Cookie": str("customer=%s; JSESSIONID=%s" % (token, sessionId))}
sesh=system.util.jsonDecode(system.net.httpPost(searchUrl ,postData=system.util.jsonEncode(data), headerValues=headers, bypassCertValidation=True))
print sesh#['row'][0]['dmsId']



'''
*******************************************************************************
this will get all of the items, and loop through them, putting the dmsid in a list
*******************************************************************************
'''


import base64

from system.util import jsonDecode as json
from system.net import httpPost as POST, httpGet as GET
from system.db import beginTransaction as beginTx, runPrepUpdate as UPDATE, commitTransaction as commitTX, closeTransaction as closeTx

host="https://g3enterprises.autodeskplm360.net"
authUrl=host + "/rest/auth/1/login"
getUrl=host   + "/api/v2/workspaces/53/items/?page=1&page-size=400" 


#auth
password='1fsadv243=='

headers={"Content-Type" : "application/json", "Accept": "application/json", "userID" : "ryan.beall", "password" : base64.b64decode(password)}
sesh=json(POST(authUrl , headers))
token, sessionId= sesh['customerToken'], sesh['sessionid']

#headers to get meta data
headers={"Content-Type" : "application/json", "Accept": "application/json", "Cookie": str("customer=%s; JSESSIONID=%s" % (token, sessionId))}

#create a table of everything after the elements key
tbl=json(GET(getUrl, headerValues=headers))['elements']

#pull every rootid out of the table of data we got back
dmsid=list(str(row['rootId']) for row in tbl)

#start the tran
txId = beginTx(timeout=50000)    

#now loop through all the dmsids we got
for row in dmsid:
    #make the call and dig down into the return all the way down to the sku
    sku=json(GET(getUrl + str(row), headerValues=headers))['fields']['JDE_LONG_ITEM_NUMBER']    
    
    #make a transaction insert, should be faster
    UPDATE("INSERT INTO [dbo].[G3_PLM_SKU_DMSID_MATRIX](SKU,DMSID,T_STAMP) VALUES (?, ?, GETDATE())", args=[sku, row], txt=txId)

#close and commit
#might get an err if it took too long
commitTX(txId)        
closeTx(txId) 




'''
*******************************************************************************
 this is the python version of getting the dmsid by sku
*******************************************************************************
'''


import urllib2, urllib, json, base64, pprint

proxie = {'https': 'http://127.0.0.1:3128'}
proxy = urllib2.ProxyHandler(proxie)
opener = urllib2.build_opener(urllib2.HTTPSHandler, proxy)

host="https://g3enterprises.autodeskplm360.net"
authUrl=host + "/rest/auth/1/login"
searchUrl=host + "/api/rest/v1/workspaces/53/items/search"


def wsPost(url,  headers):
    post = urllib.urlencode( headers )
    result = opener.open(url,  post)
    return json.loads(result.read())

def wsPost2(url,  data, headers):
    request = urllib2.Request(url)
    request.add_data(json.dumps(data))
    for key, val in headers.iteritems():
        request.add_header(key, val)
    
    request.get_method = lambda: 'POST'
    result = opener.open(request)
    return json.loads(result.read())

password='1234rrwbv3454=='
headers={"Content-Type" : "application/json", "Accept": "application/json", "userID" : "ryan.beall", "password" :base64.b64decode(password)}
session=wsPost(authUrl ,  headers)

token, sessionId= session['customerToken'], session['sessionid']
data={
    "fields": [ 
        { 
            "fieldID":"JDE_LONG_ITEM_NUMBER",
            "fieldTypeID":0
        }
                ],
    "sort" : [ 
        {
            "fieldID":"JDE_LONG_ITEM_NUMBER",
            "fieldTypeID":0,
            "sortDescending":False
        }],
    "filter" : [
        {
            "fieldID":"JDE_LONG_ITEM_NUMBER",
            "fieldTypeID":0,
            "filterType" : { "filterID" : 4 },
            "filterValue":"C3559"
        }
    ]
}


headers={"Accept" : "application/json","Content-Type" : "application/json", "Cookie": str("customer=%s; JSESSIONID=%s" % (token, sessionId))}
print (wsPost2(searchUrl, data, headers))['row'][0]['dmsId']


'''
*******************************************************************************
    how to download an attachment from plm
*******************************************************************************
'''

import urllib2, urllib, json, pprint, os
proxie = {'https': 'http://127.0.0.1:3128'}
proxy = urllib2.ProxyHandler(proxie)
opener = urllib2.build_opener(urllib2.HTTPSHandler, proxy)
authUrl="https://g3enterprises.autodeskplm360.net/rest/auth/1/login"
getUrl="https://g3enterprises.autodeskplm360.net/api/v2/workspaces/53/items/87091/files/142"

def wsPost(url, headers):
 
    post = urllib.urlencode( headers )

    result = opener.open(url, post)
    return json.loads(result.read())


def wsGet(url, headers):  
    request = urllib2.Request(url)
    for key, val in headers.iteritems():
        request.add_header(key, val)
    
    request.get_method = lambda: 'GET'
    result = opener.open(request)
    return (result.read())

def walk_dict(d, depth = 0):
    for k, v in sorted(d.items(),key = lambda x: x[0]):
        if isinstance(v, dict):
            print((" " * depth) + ("%s" % k))
            walk_dict(v, depth + 1)
        else:
            print((" " * depth) + "%s %s" % (k, v))



password = ''#getpass.getpass('Please enter your PLM password')
headers={"Content-Type" : "application/json", "Accept": "application/json", "userID" : "ryan.beall", "password" :password}
sesh=wsPost(authUrl , headers)

token, sessionId= sesh['customerToken'], sesh['sessionid']
#application/json octet-stream
headers={"Content-Type" : "application/json", "Accept": "application/octet-stream", "Cookie": str("customer=%s; JSESSIONID=%s" % (token, sessionId))}

#print pprint.pprint(wsGet(getUrl, headers))

obj= wsGet(getUrl, headers)
# print type(obj)
path=r'C:\Users\rbeall\Desktop\eclipse.doc'
 
with open(path, 'wb') as f:
    f.write(obj)
      
os.startfile(path)




'''
read file from memory into pdf viewer

'''

from __future__ import with_statement
from java.net import URL
from java.nio.file import Files, Path
from java.io import ByteArrayOutputStream, BufferedOutputStream
import jarray, base64, subprocess

#urls
host="https://g3enterprises.autodeskplm360.net"
authUrl=host+"/rest/auth/1/login"
getUrl=host+"/api/v2/workspaces/59/items/%s/files/%s" % ('159595','10924')

root=system.gui.getWindow('PATH TO YOUR WINDOW HERE').getRootContainer()

pdf=root.getComponent('PDF VIEWER OBJECT NAME HERE')


password='3 sedrfgfgvefvf '

#login to the api
headers={"Content-Type" : "application/json", "Accept": "application/json", "userID" : "ryan.beall", "password" : base64.b64decode(password)}
sesh=system.util.jsonDecode(system.net.httpPost(authUrl , headers))

#get authorization
token, sessionId= sesh['customerToken'], sesh['sessionid']
output=ByteArrayOutputStream()
path=Files.createTempFile(None, "inductivetempfile.doc")


#open conn, add headers and get stream
url=URL(getUrl)
conn=url.openConnection()
conn.setRequestProperty("Accept", "application/octet-stream")
conn.setRequestProperty("Cookie", str("customer=%s; JSESSIONID=%s" % (token, sessionId)))
input=(conn.getInputStream())

#create buffer
buffer =jarray.zeros(4096, 'b')
n=input.read(buffer)

#until it's empty
while (n!=-1):
    output.write(buffer, 0, n);
    n=input.read(buffer)

input.close()

pdf.setBytes(output.toByteArray())





'''
*******************************************************************************
get all data for a dmsid
*******************************************************************************
'''


import base64

from system.util import jsonDecode as json
from system.net import httpPost as POST, httpGet as GET
from system.db import beginTransaction as beginTx, runPrepUpdate as UPDATE, commitTransaction as commitTX, closeTransaction as closeTx

host="https://g3enterprises.autodeskplm360.net"
authUrl=host + "/rest/auth/1/login"
getUrl=host   + "/api/v2/workspaces/53/items/?page=1&page-size=400" 


#auth
password='23rfv4w35b45t'

headers={"Content-Type" : "application/json", "Accept": "application/json", "userID" : "ryan.beall", "password" : base64.b64decode(password)}
sesh=json(POST(authUrl , headers))
token, sessionId= sesh['customerToken'], sesh['sessionid']

#headers to get meta data
headers={"Content-Type" : "application/json", "Accept": "application/json", "Cookie": str("customer=%s; JSESSIONID=%s" % (token, sessionId))}

#create a table of everything after the elements key
tbl=json(GET(getUrl, headerValues=headers))['elements']






'''
*********************************************
get all stuff from grid View (basically get attachments)
*********************************************
'''
import base64, pprint
from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs, getColumnHeaders as getHeaders
from system.util import jsonDecode as jsonD, jsonEncode as jsonE
from system.net import httpPost as POST, httpGet as GET
from system.db import beginTransaction as beginTx, runPrepUpdate as UPDATE, commitTransaction as commitTX, closeTransaction as closeTx

host="https://g3enterprises.autodeskplm360.net"
authUrl=host + "/rest/auth/1/login"
getUrl=host   + "/api/rest/v1/workspaces/82/items/178816/grids" 
root=system.gui.getWindow('Main Windows/WebBrowser').getRootContainer()
dd=root.getComponent('ddDocs')


token, sessionId="G3ENTERPRISES","7B38831EE373B215FA09E39CF23AA081"

#headers to get meta data
headers={"Content-Type" : "application/json", "Accept": "application/json", "Cookie": str("customer=%s; JSESSIONID=%s" % (token, sessionId))}

#create a table of everything after the elements key
tbl=str(GET(getUrl, headerValues=headers))


if tbl:
    contents= list(jsonD(tbl)['grid']['row'])
    documents, ds=[], []
    
    for content in contents:
        docs={}
        data= list((content['columns'].items()[0])[1])
    
        for d in data:
            docs[d['key']]=d['fieldData']['value']
        documents.append(docs)
  
    ds.extend([[doc['WORKSPACE_ID'],doc['FILE_NAME'],doc['DMSID'],doc['FILEID']] for doc in documents])

    dd.data= toPyDs(toDs(getHeaders(dd.data), ds)) if ds else toPyDs(toDs([], []))
    
    
    
    
'''
use this to get the dmsid of the item in the jj workspace
so search for dmsid by sku

'''    
    
import urllib2, urllib, json, base64, pprint
data={
    "fields": [ 
        { 
            "fieldID":"FINISHED_GOOD_SKU",
            "fieldTypeID":0
        }
                ],
    "sort" : [ 
        {
            "fieldID":"FINISHED_GOOD_SKU",
            "fieldTypeID":0,
            "sortDescending":False
        }],
    "filter" : [
        {
            "fieldID":"FINISHED_GOOD_SKU",
            "fieldTypeID":0,
            "filterType" : { "filterID" : 4 },
            "filterValue":"C243264-01"
        }
    ]
}
proxie = {'https': 'http://127.0.0.1:3128'}
proxy = urllib2.ProxyHandler(proxie)
opener = urllib2.build_opener(urllib2.HTTPSHandler, proxy)

host="https://g3enterprises.autodeskplm360.net"
authUrl=host + "/rest/auth/1/login"
searchUrl=host + "/api/rest/v1/workspaces/82/items/search"


def wsPost(url,  headers):
    post = urllib.urlencode( headers )
    result = opener.open(url,  post)
    return json.loads(result.read())

def wsPost2(url,  data, headers):
    request = urllib2.Request(url)
    request.add_data(json.dumps(data))
    for key, val in headers.iteritems():
        request.add_header(key, val)
    
    request.get_method = lambda: 'POST'
    result = opener.open(request)
    return (result.read())


token, sessionId= 'G3ENTERPRISES', 'B118CAB36035590ACDD210F5AECAFF06'



headers={"Accept" : "application/json","Content-Type" : "application/json", "Cookie": str("customer=%s; JSESSIONID=%s" % (token, sessionId))}
pprint.pprint (wsPost2(searchUrl, data, headers))#['row'][0]['dmsId']




"""
    used this to return all jj data
"""


import base64, pprint
from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs, getColumnHeaders as getHeaders
from system.util import jsonDecode as jsonD, jsonEncode as jsonE
from system.net import httpPost as POST, httpGet as GET
from system.db import beginTransaction as beginTx, runPrepUpdate as UPDATE, commitTransaction as commitTX, closeTransaction as closeTx

host="https://g3enterprises.autodeskplm360.net"

getUrl=host + "/api/rest/v1/workspaces/82/items/" 

sessionId=system.tag.getTagValue('[Client]sessionId')            
if sessionId=='':        
    sessionId=project.ws.getAuthorization()
    system.tag.write('[Client]sessionId', sessionId)

#headers to get meta data
headers={"Content-Type" : "application/json", "Accept": "application/json", "Cookie": str("customer=G3ENTERPRISES; JSESSIONID=%s" % (sessionId))}

#create a table of everything after the elements key
tbl=str(GET(getUrl, headerValues=headers))
print tbl




def getAuthorizationOLD():
    import base64, system
    from system.util import jsonDecode as jsonD, jsonEncode as jsonE
    from system.net import httpPost as POST, httpGet as GET
    host="https://g3enterprises.autodeskplm360.net"
    authUrl=host + "/rest/auth/1/login"
    
    #auth
    headers={"Content-Type" : "application/json", "Accept": "application/json"}
    data='{"userID": "ryan.beall", "password": "' + base64.b64decode('QW5uaWthMDI=') + '"}'
    sesh=jsonD(POST(url=authUrl,
                    contentType="application/json",
                    postData=data,
                    connectTimeout=10000,
                    readTimeout=10000, 
                    username="", 
                    password="", 
                    headerValues=headers,                                                
                    bypassCertValidation=True)
                    )
                                                        
    if sesh:
        system.tag.write('[Client]sessionId', sesh['sessionid'])
        return sesh['sessionid']
    else:
        return ''
    
    
    
def getAuthorization():
    import base64, system, urllib2, urllib
    from system.util import jsonDecode as jsonD
    host="https://g3enterprises.autodeskplm360.net"
    authUrl=host + "/rest/auth/1/login"
        
    headers={"Content-Type" : "application/json", "Accept": "application/json"}
    data=urllib.urlencode({"userID": "ryan.beall", "password": base64.b64decode('QW5uaWthMDI=')})
    proxy = urllib2.ProxyHandler({'http': 'http://network\rbeall:' + base64.b64decode('QW5uaWthMDQ==') + '@modpxymwg:9090'})
    auth = urllib2.HTTPBasicAuthHandler()
    opener = urllib2.build_opener(proxy, auth, urllib2.HTTPHandler)
    urllib2.install_opener(opener)
    request = urllib2.Request(authUrl, data)
    sesh = urllib2.urlopen(request)
    

    if sesh.code==200:
        response=jsonD(sesh.read())
#        system.tag.write('[Client]sessionId', response['sessionid'])
        return response['sessionid']
    else:
        return ''
        
def setCookies(browser):
    import system
    from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs
    if system.gui.findWindow("Main Windows/WebBrowser"):    
        #if sessionid is blank, get a new one and write it for all to use    
        sessionId=system.tag.getTagValue('[Client]sessionId')            
        if sessionId=='':        
            sessionId=project.ws.getAuthorization()
            system.tag.write('[Client]sessionId', sessionId)
            
        #the cookies
        cookies=toPyDs(system.tag.getTagValue('[Client]cookies'))
        
        #delete all cookies
        browser.getCookieStorage().deleteAll() 
        
        #move cookies
        for cookie in cookies:
            browser.getCookieStorage().setSessionCookie(cookie['url'], cookie['name'], (sessionId if all([cookie['name']=='JSESSIONID', cookie['domain']!='.beacon-2.newrelic.com']) else cookie['value']), cookie['domain'], cookie['path'], cookie['secure'], cookie['httpOnly'])  
        
        print 'cookies transfered'    
		
		
import base64

from system.util import jsonDecode as json
from system.net import httpPost as POST, httpGet as GET
from system.db import beginTransaction as beginTx, runPrepUpdate as UPDATE, commitTransaction as commitTX, closeTransaction as closeTx

host="https://g3enterprises.autodeskplm360.net"
authUrl=host + "/rest/auth/1/login"
getUrl=host   + "/api/v2/workspaces/53/items/" 


#auth
password='f bvwebsbdfbv sdf'
headers={"Content-Type" : "application/json", "Accept": "application/json", "userID" : "ryan.beall", "password" : base64.b64decode(password)}
sesh=json(POST(authUrl , headers))
token, sessionId= sesh['customerToken'], sesh['sessionid']

#headers to get meta data
headers={"Content-Type" : "application/json", "Accept": "application/json", "Cookie": str("customer=%s; JSESSIONID=%s" % (token, sessionId))}

#create a table of everything after the elements key
tbl=json(GET(getUrl, headerValues=headers))['elements']

#pull every rootid out of the table of data we got back
dmsid=list(str(row['rootId']) for row in tbl)

#start the tran
txId = beginTx(timeout=50000)    

#now loop through all the dmsids we got
for row in dmsid:
    #make the call and dig down into the return all the way down to the sku
    sku=json(GET(getUrl + str(row), headerValues=headers))['fields']['JDE_LONG_ITEM_NUMBER']    
    
    #make a transaction insert, should be faster
    UPDATE("INSERT INTO [dbo].[G3_PLM_SKU_DMSID_MATRIX](SKU,DMSID,T_STAMP) VALUES (?, ?, GETDATE())", args=[sku, row], txt=txId)

#close and commit
#might get an err if it took too long
commitTX(txId)        
closeTx(txId)   
		
		
		
'''gets ws data back and parses the resturn json object'''

import xml.etree.ElementTree as ET
tbl = system.util.jsonDecode(system.net.httpGet("http://localhost:8080/users/1"))
#if you want to turn it into a dict
print tbl.keys(), tbl.values()
for col, row in tbl.iteritems():
	print col, row


import pprint
authUrl="https://g3enterprises.autodeskplm360.net/rest/auth/1/login"
getUrl="https://g3enterprises.autodeskplm360.net/api/v2/workspaces/53/items/86375/files/141/"

password='YOUR PASSWORD HERE'
headers={"Content-Type" : "application/json", "Accept": "application/json", "userID" : "ryan.beall", "password" :password}
sesh=system.util.jsonDecode(system.net.httpPost(authUrl , headers))

token, sessionId= sesh['customerToken'], sesh['sessionid']


headers={"Content-Type" : "application/json", "Accept": "application/octet-stream", "Cookie": str("customer=%s; JSESSIONID=%s" % (token, sessionId))}
results=system.util.jsonDecode(system.net.httpGet(getUrl, headerValues=headers, bypassCertValidation=True))

print pprint.pprint(results)




"""

		how to get a document, this logs in
		and retrieves the stream
		it then creates the word doc
		need to make it do it in memory
"""


	
from __future__ import with_statement
from java.net import URL
from java.nio.file import Files, Path
import jarray, base64, subprocess

#urls
host="https://g3enterprises.autodeskplm360.net"
authUrl=host+"/rest/auth/1/login"
getUrl=host+"/api/v2/workspaces/53/items/87091/files/142"

#base64 of my password
password='sbfebsdfbvsdf'

#login to the api
headers={"Content-Type" : "application/json", "Accept": "application/json", "userID" : "ryan.beall", "password" : base64.b64decode(password)}
sesh=system.util.jsonDecode(system.net.httpPost(authUrl , headers))

#get authorization
token, sessionId= sesh['customerToken'], sesh['sessionid']
output=Path
path=Files.createTempFile(None, "inductivetempfile.doc")
output=Files.newOutputStream(path)

#open conn, add headers and get stream
url=URL(getUrl)
conn=url.openConnection()
conn.setRequestProperty("Accept", "application/octet-stream")
conn.setRequestProperty("Cookie", str("customer=%s; JSESSIONID=%s" % (token, sessionId)))
input=(conn.getInputStream())

#create buffer
buffer =jarray.zeros(4096, 'b')
n=input.read(buffer)

#until it's empty
while (n!=-1):
	output.write(buffer, 0, n);
	n=input.read(buffer)
	
#close input and output	
input.close()	
output.close()


#open the file
p=subprocess.Popen('start winword "' + str(path) +'"',  shell=True)
proc=p.wait()



'''
to find the dmsid by sku

'''
data={
	'sort': 
		{'sortDescending': "False", 'fieldID': 'JDE_LONG_ITEM_NUMBER', 'fieldTypeID': "0"}, 
	'fields': 
		{'fieldID': 'JDE_LONG_ITEM_NUMBER', 'fieldTypeID': "0"}, 
	'filter': 
		{'fieldID': 'JDE_LONG_ITEM_NUMBER', 'fieldTypeID': "0", 'filterValue': 'C3559', 'filterType': {'filterID': "4"}}
}

import pprint, base64, itertools
host="https://g3enterprises.autodeskplm360.net"
authUrl=host + "/rest/auth/1/login"
getUrl=host   + "/api/v2/workspaces/53/items/"
searchUrl=host + "/api/rest/v1/workspaces/53/items/search" 


#auth
password='sdfvbsdfvbsdfgbvsd'
headers={"Content-Type" : "application/json", "Accept": "application/json", "userID" : "ryan.beall", "password" : base64.b64decode(password)}
sesh=system.util.jsonDecode(system.net.httpPost(authUrl , headers))
token, sessionId= sesh['customerToken'], sesh['sessionid']


headers={"Accept" : "application/json","Content-Type" : "application/json", "Cookie": str("customer=%s; JSESSIONID=%s" % (token, sessionId))}
sesh=system.util.jsonDecode(system.net.httpPost(searchUrl ,postData=system.util.jsonEncode(data), headerValues=headers, bypassCertValidation=True))
print sesh['row'][0]['dmsId']





'''

	web dev module


json={}

from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs
	
root=system.gui.getWindow('WebDevWindow').getRootContainer()
ac=root.getComponent('acRun')
tbl=toPyDs(ac.tableData)

for row in tbl:	
	json[str(row[0])]=str(row[1])
	
print {'json': json}	

'''		