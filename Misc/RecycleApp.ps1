# Load IIS module:
Import-Module WebAdministration
# Set a name of the site we want to recycle the pool for:
$site = "JobJackets"
# Get pool name by the site name:
$pool = (Get-Item "IIS:\Sites\$site"| Select-Object applicationPool).applicationPool
# Recycle the application pool:
Restart-WebAppPool $pool
#do it from cmd line 
#cd C:\\g3mesprd\c$\Windows\System32\inetsrv\
#appcmd.exe recycle apppool "ASP.NET v4.0"
