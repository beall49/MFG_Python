"""
	@param array_value : incoming integer value from array
	@param slot : slot to test against
	
"""
def get_bit(array_value, slot ):
    return ((array_value & (1 << slot)) != 0);
	
print get_bit(256, 8) #True
print get_bit(256, 9) #False



"""
	@param array_value : 
		incoming integer value from array
	@param slots : 
		how many slots the array has	
"""
def get_bit(array_value, slots=16):
	reason_codes = []
	for slot in range(slots):
		if ((array_value & (1 << slot)) != 0):
			reason_codes.append(slot)

	return reason_codes
	
print get_bit(16449) #[0,6,14]
print get_bit(256) #[8]