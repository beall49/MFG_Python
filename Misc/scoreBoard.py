class Scoreboard():
    
    def __init__(self):
        import calendar, datetime, system; 

        from system.db import runScalarQuery as runQry 
        from system.tag import getTagValue as getTag
        from app.Schedule import getScheduledQuantity as getQuantities
        self.now                 =                                      datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        try:
            #set the class properties
            self.days                  =                                list(calendar.day_name)    
            self.periodID               =                                runQry ("SELECT CONVERT(NVARCHAR(8),GETDATE(), 112) AS PERIODID")             
            self.root                 =                                system.gui.getWindow('Main Windows/Scoreboard').getRootContainer()    
            
            
            #get window properties    
            self.line                 =                                 self.root.lineName                
            self.gemba                  =                                self.root.getComponent('GembaBoard')
            self.gemba.getComponent('GembaController').update()
            self.downtime              =                                 self.root.getComponent('DowntimeContainer')
            self.uom                 =                                self.root.CurrentSkuEaToCs        
            self.waste                  =                                 self.root.getComponent('WasteContainer')
            
            #get tag properties        
            self.currentShift           =                                 getTag(self.line + "/Run/Shift")        
            self.runID                   =                                 str(getTag(self.line + "/Run/Run ID"))
            
            #get current week qty            
            self.weekTarget             =                                 getQuantities(self.gemba.WeekStartDateTime, self.root.CurrentShiftEndTime, self.line)
                    
            #call target function
            self.getGembaTargets()
            
            #call actual function
            totalProduced               =                                 self.getGembaActuals()                                
            
            #call waste function
            totalWaste                  =                                self.getGembaWaste()
                
            
            self.gemba.CurrentWeekTargetCases =    self.weekTarget
            
            self.gemba.CurrentWeekActualCases =    (totalProduced-totalWaste)                    
            self.gemba.CurrentWeekDeltaCases = (totalProduced-totalWaste) - self.weekTarget
            
            #if running do the waste and downtime, otherwise skip it
            if self.root.lineState!= 'No Run'    :
                self.getWasteValues()
                self.getDownTimeValues()
                    
            self.makeLog("Gemba Board Finished")

        except:
            import traceback; self.makeLog("\nGemba Main failed at %s\n %s" %(self.now , str(traceback.format_exc())))
                
                                                        
    def getGembaTargets(self):
        try:
            from app.Schedule import getShiftTargets as getTargets
            tbl =     getTargets(self.periodID, self.line)
            #empty em
            [self.gemba.getComponent(day).setPropertyValue('Shift' + str(x) + 'Target',0) for x in range(1,4) for day in self.days]
            #set the vals to the correct day and shift
            [self.gemba.getComponent(row['DAY_NAME']).setPropertyValue('Shift' + str(row['SHIFT_NUMBER']) + 'Target', row['SHIFT_TARGET']) for row in tbl]
                            
        except:
            import traceback; self.makeLog("\nGemba targets at %s\n %s" %(self.now , str(traceback.format_exc())))
                    
                        
    #############HANDLE THE ACTUALS
    def getGembaActuals(self):        
        from datetime import datetime, timedelta 
        from  system.dataset import toPyDataSet as toPyDs
        from app.Product import getUomConv as getConv
        try:
        
            #clear out targets and actuals
            [self.gemba.getComponent(day).setPropertyValue('Shift' + str(x) + 'Actual',0) for x in range(1,4) for day in self.days]

            product=""
            totalProduced , conv    = [0]*2
            tbl = toPyDs(self.gemba.getComponent('GembaController').tableData)
                        
            
            for row in tbl:    
                #get shift
                shift = row["Shift"]
                #get day name
                dayName =  (datetime.strptime(str(row[3]),"%Y-%m-%d %H:%M:%S") + timedelta(days=(1 if shift ==3 else 0))).strftime("%A")
                
                if product !=row["Product Code"]:                    
                    product =   row["Product Code"]
                    conv = getConv(product, self.line)
                
                if product != "WFMaintenance": 
                    #if <1 return 0 else do conversion    
                    actual = 0 if row["Line Production Count"] < 1 else round(row["Line Production Count"]     /conv)    
            
                    totalProduced += actual
                    self.gemba.getComponent(dayName).setPropertyValue('Shift' + str(shift) + 'Actual', (actual + self.gemba.getComponent(dayName).getPropertyValue('Shift' + str(shift) + 'Actual')))

                
            return totalProduced
        except:
            import traceback; self.makeLog("\nGemba Actuals failed at %s\n %s" %(self.now , str(traceback.format_exc())))
            return 0
            
            
    def getGembaWaste(self):        
        from app.Utils import runStoredProc as EXEC
        try:
            #get the manual waste by shift
            tbl = EXEC([["RUNID", "BIGINT", self.runID]], "[MES_RUNTIME].[DBO].[USP_GET_WASTE_BY_RUN_BY_SHIFT]")
            totalWaste=sum([(row[5]/self.uom) for row in tbl ]) 
            
            [self.gemba.getComponent(row['DAY_NAME']).setPropertyValue('Shift' + str(row['SHIFT_NUMBER']) + 'Actual', (row['SHIFT_TARGET']/self.uom)) for row in tbl]

            return totalWaste
        except:
            import traceback; self.makeLog("\nGemba Waste failed at %s\n %s" %(self.now , str(traceback.format_exc())))
            return 0                                                                

    def getDownTimeValues(self):        
        from  system.dataset import toPyDataSet as toPyDs, toDataSet as toDs, sort as sort
        try:
            #SET DEFAULTS
            minutes ,    totalMinutes,     otherMinutes = [0]*3
            headers , data        = ["Reason","Downtime Minutes"],  []
            tbl = self.downtime.getComponent('DTAnalysisController').tableData
            
            #if table exists
            if tbl:                        
                tbl = toPyDs(sort(tbl, "Downtime Minutes", 0))
                
                #append all downtime to ds
                data.extend([[str(row["Reason"]), row["Downtime Minutes"]] for index, row in enumerate(tbl) if index<6 ])
                #get downtime outside of top 5
                otherMinutes = sum([row["Downtime Minutes"] for index, row in enumerate(tbl) if index>5 ])
                
                #get all downtime
                totalMinutes += sum([row["Downtime Minutes"]  for row in tbl if row["Downtime Minutes"]>0])

                #appending others and line total to the bottom ...        
                if otherMinutes > 0: data.append(["Other", otherMinutes]) 
                
                data.append(["Line Total", totalMinutes])
                
                tbl = toDs(headers, data)
                
                #INSERT INTO PARETO AND CUSTOM PROPS
                self.downtime.getComponent('DTPareto').data = tbl
                self.downtime.TotalDT = totalMinutes
                self.downtime.DTData = tbl
                
        except:
            import traceback; self.makeLog("\nDowntime failed at %s\n %s" %(self.now , str(traceback.format_exc())))
                        

    def getWasteValues(self):
        import math
        from  system.dataset import toPyDataSet as toPyDs, toDataSet as toDs
        try:        
            #GET TBLE DATA
            tbl = toPyDs(self.waste.getComponent('WasteAnalysisController').tableData)
            
            #SET DEFAULTS
            currentWaste         =                 int(math.floor(self.root.currentShiftWaste)) 
            headers, data = ["Cell","Cell Waste Count"], list([["Manual Waste", currentWaste]])
            
            #sum all waste
            totalWaste = sum([row["Cell Waste Count"] for row in tbl if row["Cell Waste Count"]>0])
            
            #get cell name and waste and append it to the list
            data.extend([[str(row["Cell Name"]), row["Cell Waste Count"]] for row in tbl if row["Cell Waste Count"]>0])
                
            #CUSTOM SORT TO MAKE THE DATA SET SORT IN THE EXPECTED ORDER...
            sortedData = self.sortMachineNames(data)
            
            sortedData.append(["Line Total", totalWaste])
            
            tbl = toDs(headers, sortedData)
            
            #ISNERT INTO PARETO
            self.waste.getComponent('WastePareto').data = tbl
            self.waste.TotalWaste = totalWaste
            self.waste.WasteData = tbl    
        except:
            import traceback; self.makeLog("\nWaste failed at %s\n %s" %(self.now , str(traceback.format_exc())))
    
    def makeLog(self, msg):    
        from org.apache.log4j import Logger
        log = Logger.getLogger("Scoreboard Logger")    
        log.error(msg)            
        
        
    #magic to sort the machine names in the order of line flow
    def sortMachineNames(self, data):
        alphabet = "frolabcdeghijkmnopqstuvwxyz "
        sortedData = sorted(data, key=lambda word: [alphabet.index(c) for c in word[0].lower()])
        return sortedData        
        
   