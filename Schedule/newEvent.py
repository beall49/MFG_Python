#system.gui.messageBox(str(system.security.getRoles()))
if event.source.componentEnabled:
	role = event.source.Role
	lineRole = event.source.lineRole
	if role in lineRole:
		tblWO = event.source.parent.parent.getComponent('cntWorkOrder').getComponent('tblWrkOrdr')
		if tblWO.selectedRow > -1:
			e = event.source.parent.parent		
			lnPath = event.source.selectedLinePath
			lineName = lnPath[lnPath.rfind('\\')+1:]
			
			parms=[	["PRODUCT", "VARCHAR", e.productCode],["LINE_NAME", "VARCHAR", lineName]]
			tbl = app.Utils.runStoredProc(parms, "DBO.USP_GET_LINE_BY_PROD")
	
			if tbl[0][0] == 1:
				system.nav.openWindow('Popups/Schedule/ScheduleNew', 
				{
					'startDate': event.source.selectedDate, 	
					'linePath' : lnPath,
					'lineName' : lineName,
					'productCode' : e.productCode,
					'workOrder' : e.workOrder,
					'workOrderId' : e.workOrderId,
					'quantity' : e.quantity,
					'scheduled' : e.scheduled,
					'remaining' : e.remaining,
					'description' : e.description,
					'changeOver' : tbl[0][1]
				})
				system.nav.centerWindow('Popups/Schedule/ScheduleNew')		
			else:
				system.gui.warningBox("This product cannot be scheduled on this line.", "Please select another line.")
		else:
			system.gui.warningBox("Please select a work order above. " + str(tblWO.selectedRow), "No work order selected.")
	else:
		system.gui.warningBox("You do not have the sufficient privileges to continue.")
else:
	system.gui.warningBox("You do not have the sufficient privileges to continue.")		