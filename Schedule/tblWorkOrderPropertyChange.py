"""
	This will just put the correct attributes at root
	when a row is selected
"""

if event.propertyName == 'selectedRow':
	tbl 					= event.source
	row 					= tbl.selectedRow
	if row != -1:
		root 				= system.gui.getWindow('Main Windows/Schedule').getRootContainer()
		root.workOrderId 	= tbl.data.getValueAt(row, 0)
		root.workOrder 		= str(tbl.data.getValueAt(row, 3))
		root.productCode 	= str(tbl.data.getValueAt(row, 4))
		root.description 	= tbl.data.getValueAt(row, 5)
		root.quantity 		= tbl.data.getValueAt(row, 6)
		root.produced 		= tbl.data.getValueAt(row, 7)
		root.scheduled 		= tbl.data.getValueAt(row, 8)
		root.remaining 		= tbl.data.getValueAt(row, 9)