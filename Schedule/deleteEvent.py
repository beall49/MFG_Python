if event.source.componentEnabled:
	from time import sleep
	role = event.source.Role
	lineRole = event.source.lineRole
	if role != "" and role in lineRole:#if True:
		scheduleID = event.source.selectedEvent
	
		#verify the user is clicking on an event'
		if scheduleID > 0:		
			#get info about the workorder
			parms = [["SCHEDULEID", "INTEGER", scheduleID ]]
			rslt = app.Utils.runStoredProc(parms, "USP_GET_WORK_ORDER_INFO")
			
			#make sure the run hasn't started yet
			if not rslt[0]["RUNID"]:
				# gather parameters required to add a schedule entry
				linePath = event.source.selectedLinePath
		
				# call the add schedule entry method of the schedule entry controller
				delete = system.gui.confirm("Do you really want to delete schedule entry " + str(scheduleID),"Delete Entry")
				result = event.source.parent.getComponent('Schedule Entry Controller').deleteScheduleEntry(linePath, scheduleID) if(delete == 1) else  0
				
				tbl = event.source.parent.parent.getComponent('cntWorkOrder').getComponent('Work Order Table')
				tbl.refreshList()							
			else:
				system.gui.warningBox("You cannot delete a run after it has been started.")	
		else:
			system.gui.warningBox("Please select a run.", "No run selected.")						
	else:
		system.gui.warningBox("You don't have sufficient privileges to continue.")
else:
	system.gui.warningBox("You don't have the sufficient privileges to continue.")	