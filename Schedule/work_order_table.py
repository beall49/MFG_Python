def cellEdited():
    import system
    def updateTbl(id = event.source.parent.parent.workOrderId, tbl = event.source.parent.getComponent('Work Order Table')):
        import system, app
        print id
        qry ="UPDATE [MES_RUNTIME].[DBO].[WORKORDER] SET CLOSED = 1 WHERE ID = %s" %(str(id))
        system.db.runUpdateQuery(qry)
        tbl.refreshList()
        
    #    app.Schedule.getWorkOrderTable()    
    if "Cork Scheduler" in system.security.getRoles() or 1==1:
        if event.column == 1 and event.oldValue != event.newValue: 
            tbl = event.source
            row = tbl.selectedRow
            delete = system.gui.confirm("Do you really want to close work order: " + str(tbl.data.getValueAt(row, 3)),"Close Work Order")
            if delete:
                event.source.removeRow(event.row)
                event.source.selectedRow = -1
                system.util.invokeAsynchronous(updateTbl)
    
            else:
                event.source.parent.getComponent('Work Order Table').refreshList()
    else:
        system.gui.messageBox("You don't have sufficient privileges to continue.")      



def propertyChange():
    if event.propertyName == 'selectedRow':
        tbl = event.source
        row = tbl.selectedRow
    
    
        if row != -1:
            e = event.source.parent.parent
            e.workOrderId = tbl.data.getValueAt(row, 0)
            e.workOrder = str(tbl.data.getValueAt(row, 3))
            e.productCode = str(tbl.data.getValueAt(row, 4))
            e.description = tbl.data.getValueAt(row, 5)
            e.quantity = tbl.data.getValueAt(row, 6)
            e.produced = tbl.data.getValueAt(row, 7)
            e.scheduled = tbl.data.getValueAt(row, 8)
            e.remaining = tbl.data.getValueAt(row, 9)
        
        
        
            