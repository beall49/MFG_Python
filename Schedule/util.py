def getWeekStartDateTime(currentPeriod,line):
    try:
        import system, app
        parms = [["CURRENTPERIOD", "BIGINT", currentPeriod],["CURRENTLINE", "VARCHAR", line]]
        tbl = app.Utils.runStoredProc(parms, "[MES_RUNTIME].[DBO].[USP_GET_SCHED_QTY]")
        if len(tbl) > 0:
            return tbl[0]["SHIFT_START"]
        else:
            return None
    except:
        return None
    
    
def getShiftTargets(period, line):
    try:
        import system, app
        parms = [["CURRENTPERIOD", "BIGINT", period],["CURRENTLINE", "VARCHAR", line]]
        return app.Utils.runStoredProc(parms, "[MES_RUNTIME].[DBO].[USP_GET_SCHED_QTY]")
    except:
        return None    
    
def getScheduledQuantity(startDateTime, endDateTime, line):
    try:
        import system, app
        parms = [
                    ["SHIFTSTARTDATE", "TIMESTAMP", startDateTime],
                    ["SHIFTENDDATE", "TIMESTAMP", endDateTime],
                    ["PRODUCTIONLINE", "VARCHAR", line]
                ]
        tbl = app.Utils.runStoredProc(parms, "[MES_RUNTIME].[DBO].[USP_GET_SCHED_QTY_BY_TIME_LINE]")
        return tbl[0][0]
    except:
        return 0    

#returns the schedule entry preceeding the current one(strictly based on the schedule times)
def getPriorScheduleEntry(scheduleId, lineId):
    try:
        import system
        results = system.db.runPrepQuery("""
        select top 1 ID from Schedule
         where FinishDateTime <= (select StartDateTime from schedule where id = ?) 
         and lineID = ? order by FinishDateTime desc
         """, [scheduleId, lineId])
        if len(results) > 0:
            return[0][0]
        else:
            return -1
    except:
        return -1        
         
#returns the schedule entry following the current one(strictly based on the schedule times    )     
def getNextScheduleEntry(scheduleId, lineId):
    try:
        import system
        results = system.db.runPrepQuery("""
        select top 1 ID from Schedule
         where StartDateTime >= (select FinishDateTime from schedule where id = ?) 
         and lineID = ? order by StartDateTime
         """, [scheduleId, lineId])
        if len(results) > 0:
            return results[0][0]
        else:
            return -1
    except:
        return None        
         
def getScheduleEntryInfo(scheduleId):
    try:
        import system    
        results = system.db.runPrepQuery("""
                SELECT ID,LineID,WorkOrderID,ScheduleType
                ,Note,StartDateTime,FinishDateTime
                ,Shift,Quantity,RunID,ActualStartDateTime
                ,ActualFinishDateTime,ActualQuantity
                ,EnteredBy,RunStartDateTime
                ,ActualRunStartDateTime
                ,TimeStamp,
                datediff(minute, StartDateTime, RunStartDateTime) as ChangeOverTime, 
                datediff(minute, StartDateTime, FinishDateTime) as TotalTime,
                datediff(minute, RunStartDateTime, FinishDateTime) as RunningTime
            FROM Schedule
                where ID = ?     
          """,[scheduleId])
        return results
    except:
        return None    

##based on the requirement that we get the next unrun schedule item from the beginning of the week
def getNextWorkOrder(weekStartTime, lineID):
    try:
        import system
        results = system.db.runPrepQuery("""
            SELECT TOP 1 WO.WorkOrder, WO.ProductCode, PC.Description, ScheduleType
            FROM Schedule S
            LEFT JOIN WorkOrder WO ON WO.ID = S.WorkOrderID
            LEFT JOIN ProductCode PC ON WO.ProductCode = PC.ProductCode
            WHERE S.ActualStartDateTime is null 
            AND S.ActualFinishDateTime IS NULL
            AND StartDateTime >= ?
            and S.LineID = ?
            AND Coalesce(WO.Closed, 0) <> 1
            ORDER BY StartDateTime
            """,[weekStartTime, lineID])
        return results
    except:
        return None    
 
## finds the next best start time by looking at either the prior item's schedule or actual finish time
## adds one second to the finish time to get a new start time
def getNextAvailableStartTime(scheduleId, lineId):
    try:
        import system
        from java.util import Calendar
        results = system.db.runPrepQuery("""
            select top 1 * from (
            select [FinishDateTime] from Schedule where [FinishDateTime] < (select dateadd(second, -61, StartDateTime) from [Schedule] s  where s.id = ? ) and lineID = ? 
            union all
            select [ActualFinishDateTime] from Schedule where [ActualFinishDateTime] < (select dateadd(second, -61, StartDateTime) from [Schedule] s  where s.id = ? ) and lineID = ? 
            ) a order by FinishDateTime desc
            """, [ scheduleId, lineId, scheduleId ,lineId ])
        if len(results) > 0:
            priorItemFinishTime = results[0][0]
            cal = Calendar.getInstance()
            cal.setTime(priorItemFinishTime)
            cal.add(Calendar.SECOND, 1)
            newStartTime = cal.getTime()
            return newStartTime
        else:
            return None
    except:
        return None        
        
