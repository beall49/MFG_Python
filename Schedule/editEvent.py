if event.source.componentEnabled:
	role = event.source.Role
	lineRole = event.source.lineRole
	if role != "" and role in lineRole:
		e = event.source
		#get event (run) id
		eventId = e.getSelectedEvent()
		#verify the user is clicking on an event'
		if eventId > 0:
			#get info about the workorder
			parms = [["SCHEDULEID", "INTEGER", eventId ]]
			rslt = app.Utils.runStoredProc(parms, "USP_GET_WORK_ORDER_INFO")
			
			#make sure the run hasn't started yet
			if not rslt[0]["RUNID"]:
				lnPath = e.selectedLinePath
				lineName = lnPath[lnPath.rfind('\\')+1:]
				#open the edit window and pass the parms
				system.nav.openWindow('Popups/Schedule/ScheduleEdit', 
				{
					'startTime': rslt[0]["STARTDATETIME"], 	
					'linePath' : lnPath,
					'lineName' :lineName,
					'productCode' : rslt[0]["PRODUCTCODE"] ,
					'workOrder' : rslt[0]["WORKORDER"],
					'workOrderId' : rslt[0]["WORKORDERID"],
					'workOrderQuantity' : rslt[0]["WO_QUANTITY"],
					'scheduled' : rslt[0]["TOTAL_SCHEDULED"],
					'runQuantity' : rslt[0]["RUN_QUANTITY"],
					'remaining' : rslt[0]["REMAINING"],
					'description' : rslt[0]["DESCRIPTION"], 
					'finishTime' : rslt[0]["FINISHDATETIME"],
					'scheduleID' :  eventId
				})
				system.nav.centerWindow('Popups/Schedule/ScheduleEdit')			
			else:
				system.gui.warningBox("You cannot edit a run after it has been started.")
		else:
			system.gui.warningBox("Please select a run.", "No run selected.")		
	else:
		system.gui.warningBox("You don't have sufficient privileges to continue.")
else:
	system.gui.warningBox("You don't have the sufficient privileges to continue.")	