import system
#update the work order to closed
def updateTbl(id = event.source.parent.parent.workOrderId, tbl = event.source.parent.getComponent('Work Order Table')):
	import system, app
	qry ="UPDATE [MES_RUNTIME].[DBO].[WORKORDER] SET CLOSED = 1 WHERE ID = %s" %(str(id))
	system.db.runUpdateQuery(qry)
	tbl.refreshList()
	
#if user can update workorders
if "Cork Scheduler" in system.security.getRoles() or 1==1:
	#if it's the closed column
	if event.column == 1 and event.oldValue != event.newValue: 
		tbl = event.source
		row = tbl.selectedRow
		#you wanna delete for reals?
		delete = system.gui.confirm("Do you really want to close work order: " + str(tbl.data.getValueAt(row, 3)),"Close Work Order")
		if delete:
			event.source.removeRow(event.row)
			event.source.selectedRow = -1
			system.util.invokeAsynchronous(updateTbl)

		else:
			event.source.parent.getComponent('Work Order Table').refreshList()
else:
	system.gui.messageBox("You don't have sufficient privileges to continue.")			