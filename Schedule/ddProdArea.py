"""
	fires when an area is selected
"""

me = system.gui.findWindow("Main Windows/Schedule")

#if im open (wierd inductive bug)
if len(me)>0:
	import system, app

	root=system.gui.getWindow('Main Windows/Schedule').getRootContainer()
	workorder=root.getComponent('cntWorkOrder')
	schedule=root.getComponent('cntSchedule').getComponent('Line Schedule View')
	
	#set work order table to no selected line
	workorder.getComponent('Work Order Table').selectedRow = -1
	#get the selected area
	prodArea = workorder.getComponent('ddProdArea').selectedLabel
	
	#loop up lines by area
	tbl = shared.SQLs.runQry([["PROD_AREA", "VARCHAR", prodArea]], "[MES_RUNTIME].[DBO].[USP_GET_WO_LOOKUP]")
	
	#create list of lines from results
	lines= ', '.join([row[0] for row in tbl])			
	
	#filter workorders by thelines
	workorder.getComponent('Work Order Table').lineNameFilter = lines
	
		
	workorder.getComponent('tblWrkOrdr').selectedRow = -1
	#too many Cap 'type' lines, it won't filter correctly without this hardcode
	schedule.itemPath = prodArea if prodArea != "Cap" else "Caps_"	
	#setting correct role
	schedule.lineRole  = 'Scheduler' if 'Cap' in prodArea else 'Cork Scheduler'		