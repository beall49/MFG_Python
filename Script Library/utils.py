def writeCounts(lineName, machineName):
	import system
	tagPath = lineName + "/PLC/" + machineName + "/OEE/"
	counts = int(system.tag.read(tagPath + "Count_Out_Cs").value) * int(system.tag.read(lineName + "/Util/UOM").value)
	system.tag.write(tagPath + "Count_In", counts)	
	system.tag.write(tagPath + "Count_Out", counts)
	
def writeCountsWIP(lineName, machineName):
	import system
	countIn = system.tag.read(lineName + "/PLC/" + machineName + "/OEE/Count_Out").value
	system.tag.write(lineName + "/PLC/WIP/OEE/Count_In", countIn)	
	
def breakExpired(lineName):
	import system		
	system.tag.write(lineName + "/Util/lineStateId", 1)