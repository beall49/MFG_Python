from system.tag import getTagValue as GET
from system.nav import swapTo as SWAP, openWindow as OPEN
#figure out what screen was passed in 
#plant overview has a space so I pass in just plant, then convert
defaultScreen = 'Main Windows/' + ('Plant Overview' if GET('[Client]DefaultScreen') == 'Plant' else GET('[Client]DefaultScreen'))
	
#note, this depends on an 'auto-login' setup for the scoreboard user with a password of test
if (defaultScreen in ["Main Windows/Scoreboard", "Main Windows/Plant Overview"]):	
	from javax.swing import SwingUtilities,JFrame
	from java.awt import GraphicsEnvironment,Rectangle
	
	#Get the screen dimensions from the Client URL
	x,y,h,w=GET('[Client]X'), GET('[Client]Y'), GET('[Client]H'), GET('[Client]W')
	
	#handle setting the common client variables (e.g. line name)
	shared.login.setClientVars()
	
	#switch to the scoreboard
	window = SWAP(defaultScreen)	
	frame = SwingUtilities.getAncestorOfClass(JFrame, window)
	
	#set the dimensions of the scoreboard
	frame.setBounds(x,y,h,w)
	
	#remove the annoying 4px frame border
	window2 = OPEN(defaultScreen)
	window2.parent.parent.parent.setBorder(None)
	
elif (defaultScreen == "Main Windows/Simulator"):
	SWAP("LineSimulator/Sim_BK2")	
else:
	SWAP("Main Windows/Login Window")