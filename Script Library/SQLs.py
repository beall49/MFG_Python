def runQry( parms, sp, cn="MES_RUNTIME"):
	from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs
	from system.db import createSProcCall as spCall, execSProcCall as EXEC, runPrepQuery as runQry
	dataType={"BIT":-7,"BLOB":2004,"NCHAR":-15,	"DATE":91,"NULL":0,"NVARCHAR":-9,"TIME":92,"INTEGER":4,"TIMESTAMP":93,"BIGINT":-5,"CHAR":1,"BINARY":-2,"VARCHAR":12}										
	statements={'SELECT': 1, 'INSERT':1, 'UPDATE':1}
	try:
		if all([not parms, statements.get(sp[:6])]):
			return toPyDs(toDs(runQry(sp)))
		else:
			###################CREATE THE SP CALL
			call = spCall(sp, cn)
					
			###################LOOP THROUGH THE ROWS AND REGISTER EACH ONE OF THE PARAMETERS IN THE LIST
			[call.registerInParam(row[0], dataType[row[1]], row[2]) for row in parms]	
			
			###################EXECUTE THE SP
			EXEC(call)
			
			###################RETURN A TBL, IF THERE'S NO DATA MEH NO ERR
			return toPyDs(call.getResultSet())	
	except:
		#RETURN EMPTY DATASET
		return toPyDs(toDs([], []))	
		
