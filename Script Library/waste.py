#on new run
def updateWaste(lineName):
	import system, app, sys
	#get the old and new runids
	#old runid
	runID = str(system.tag.getTagValue(lineName + "/Util/RunId"))	
	newRunId = str(system.tag.getTagValue(lineName + "/Util/runIdChg"))
	
	#if the run really changed
	if runID !=  newRunId:
		parms=[["RUNID","BIGINT",runID]]
		waste = shared.SQLs.runQry(parms, "[dbo].[USP_GET_WASTE_TOTAL_BY_RUN]")[0][0]
		
		tbl = shared.SQLs.runQry(parms, "[dbo].[USP_GET_RUN_INFO]")
		
		
		#if the waste is greater than 0
		if waste > 0:
			#if you got some data back (trap to make sure there's a run)
			if tbl != 0:		
				originalWaste , originalProduction, originalInfeed, runUUID = tbl[0][0] , tbl[0][1], tbl[0][2],  tbl[0][3]	
				newWaste =  waste + originalWaste
				newProdCount = originalInfeed - newWaste			
				system.production.adjustRunData(runUUID, "", 'WasteCount', newWaste, 0) 
				system.production.adjustRunData(runUUID, "", 'ProductionCount', newProdCount, 0) 
		
		#reset the shift waste (which is the root of all the waste)
		[system.tag.writeToTag(lineName + "/Waste/wasteShift" + str(x), 0) for x in range (1,4)]			
		
		#write the new runid and uom
		system.tag.writeToTag(lineName + "/Util/RunId", newRunId)
		mainTags = system.tag.browseTags(parentPath="[default]%s/Factors" % lineName) 
		[system.tag.write(tag, "") for tag in mainTags if tag.name not in ['Operator']]
		[system.tag.write(tag, "1970-01-01") for tag in mainTags if tag.name[-4:] =='Date']
		
		
		
#to add waste to an aggregate tag during the run	
def aggregateWaste(lnName, addedWaste):
	import system, app
	shift = system.tag.getTagValue(lnName + "/Run/Shift")
	runID =  system.tag.getTagValue(lnName + "/Util/RunId")
	#PACKAGE THEM UP IN A PARAMETER MATRIX
	parms = [
					["RUNID", "INTEGER", runID],
					["WASTEVALUE", "INTEGER", addedWaste],
					["LINE", "VARCHAR", lnName],
					["USERNAME","VARCHAR", str(system.security.getUsername())],
					["SHIFT", "INTEGER", shift]
				]
				
	#PASS IT OVER FOR A LOG
	shared.SQLs.runQry(parms, "USP_INSERT_STAGE_WASTE")		
	
	#for getting stuff by shift for curr day
	parms = [["RUNID", "BIGINT", runID]]
	tbl = shared.SQLs.runQry(parms, "[MES_RUNTIME].[DBO].[USP_GET_WASTE_CURR_DAY_BY_SHIFT]")
		
	for x in range (1,4): 
		system.tag.writeToTag(lnName + "/Waste/wasteShift" + str(x), tbl[0][x]) 		