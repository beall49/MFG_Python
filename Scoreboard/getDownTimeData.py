"""
	this gets downtime for this line for this shift/run and sums it up
"""

from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs, getColumnHeaders as getHeaders
tbl= toPyDs(event.source.tableData)
downtime = 0
if tbl:	
	downtime = sum(row[1] for row in tbl)

event.source.parent.parent.runShiftDownTime = int(downtime) 