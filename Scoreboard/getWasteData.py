#sums waste for this shift/run
from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs, getColumnHeaders as getHeaders
tbl= toPyDs(event.source.tableData)
waste = 0
if tbl:	
	waste = sum(int(row[1]) for row in tbl)
	
event.source.parent.parent.wasteShiftRun = waste	