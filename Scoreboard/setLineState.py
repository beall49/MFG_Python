	from java.awt import Font
	import system
	root = self
	lineName = root.lineName
	cellVal=reasonVal=subReasonVal=""

	if root.lineState not in [ "Running","Changeover", "Break", "Lunch"]:
		lstReason = (root.lineDownReason).split("-")
		
		cellVal 	 = lstReason[0].upper().rstrip() if len(lstReason)>0 else cellVal
		reasonVal 	 = lstReason[1].upper().rstrip() if len(lstReason)>1 else reasonVal
		subReasonVal = lstReason[2].upper().rstrip() if len(lstReason)>2 else subReasonVal


		
	root.lineDownCellReason = cellVal	
	root.lineDownMainReason = reasonVal
	root.lineDownSubReason = subReasonVal