from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs
this = event.source

#if linename changes, reset gemba
if event.propertyName == "lineName":
	this.setGemba()

#if any of these events fire call line setup and clean up chart
if event.propertyName in ["lineName","lineState", "lineDownReason", "workOrder", "dataQuality"]:	
	this.setLineState()
	system.util.invokeLater(this.setChartView,1000)
	
#when run changes, reset oee dataset				
if event.propertyName=='runId':
	this.runOEE60Vals =	toDs(["vals"], [])
	this.runOEE60Direction = 0

#if the run rate dataset changes get the real run rate	
if event.propertyName=='runRateDs':	
	tbl = toPyDs(this.runRateDs)
	this.runRate = 0 if len(tbl)<1 else int(tbl[0][1]/60)

#get avg oee	
if event.propertyName=='runOEE60Ds':	
	val = int(this.getAverage(toPyDs(this.runOEE60Ds)) *100)
	this.setDirection(val)	
	this.runOEE60 = val	
	

#if event.propertyName in ['shiftActual','actual','shiftTarget','workOrderTarget','runInfeedCount']:	
#	this.refreshGemba()	