	from java.awt import Color
	this = self.getComponent('cntText').getComponent('cntVision').getComponent('chtVision')
	plot = (this.chart.plot)
	
	range = plot.rangeAxis
	domain = plot.domainAxis
	#remove outline
	plot.setOutlineVisible(False)
	#remove girdlines
	plot.setRangeGridlinesVisible(False)
	
	#remove tickmarks, labels, and lines
	range.setTickMarksVisible(False)
	range.setTickLabelsVisible(False)
	range.setAxisLineVisible(False)
	
	#remove lines and tickmarks
	domain.setAxisLineVisible(False)
	domain.setTickLabelsVisible(False)
