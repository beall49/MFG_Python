	if self.lineName == 'BK1':
		from system.dataset import toPyDataSet as toPyDs
		tbl= toPyDs(self.runOMSOSpeedDs)
		val = 0
		if len(tbl)>0:	
			val = int(sum(int(row[1]) for row in tbl if row[1]>0)/len(tbl))
			if 10000>val>1000:
				val = str(val)[:1] + "K"
			elif val>9999:
				val = str(val)[:2] + "K"			
		self.runOMSOSpeed = str(val)