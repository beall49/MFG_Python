	import calendar
	from shared.SQLs import runQry
	from datetime import datetime, timedelta
	from  system.dataset import toPyDataSet as toPyDs, toDataSet as toDs, sort as sort
	root = self
	gemba = root.getComponent('cntGemba') 
	line = root.lineName
	weekTarget, totalProduced  = 0,0
	days = list(calendar.day_name)
	dayDict={'MONDAY':0,'TUESDAY':1, 'WEDNESDAY':2, 'THURSDAY':3, 'FRIDAY':4, 'SATURDAY':5, 'SUNDAY':6}
	
	#this is the actual shift order
	shiftsDict = {3:[3], 2:[3,1,2], 1:[1,3]}
	today=datetime.today().weekday()				
	#errs in the designer otherwise (when no line is in root.linename)
	if line!="":	
		shift = root.lineShift  if root.lineShift in [1,2,3] else system.tag.getTagValue(line + "/Run/Shift")
		
		#clear out old values
		for x in range(1,4):
			for day in days:
				gemba.getComponent(day).setPropertyValue('Shift' + str(x) + 'Target',0)
				gemba.getComponent(day).setPropertyValue('Shift' + str(x) + 'Actual',0)
		
		#get values from prod schedule/gemba
		tblVals = shared.SQLs.runQry([["LINE", "VARCHAR", line]], "USP_GET_GEMBA_DATA" )
		#sum actuals
		totalProduced = sum(row[1] for row in tblVals if str(row[0])[:6]=='ACTUAL')
		
		for row in tblVals:
			data = str(row[0]).split("_")
			if data:
				#figures out the correct component based on current row and gives it a value
				gemba.getComponent(str(data[1]).title()).setPropertyValue('Shift' + str(data[2]) + str(data[0]).title(),row[1])
				
				#figuring out current over/under for the week (only looking up to right now)
				if str(data[0]).upper()=="TARGET":
					if shift in [1,2,3]:
						if dayDict.get(data[1],0)<today:
							weekTarget += row[1]
						elif dayDict.get(data[1],0)==today:
							if int(data[2]) in shiftsDict.get(shift,0):				
								weekTarget += row[1]

		root.lineWeekTarget = weekTarget
		root.lineWeekProduced = totalProduced
		root.lineWeekDelta = (totalProduced-weekTarget)	