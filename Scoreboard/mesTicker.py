"""
	if you want to make a scrolling ticker bar
"""
root = system.gui.getWindow('Main Windows/Scoreboard2').getRootContainer()
ticker = root.getComponent('cntTicker').getComponent('txtTicker')

fullString = root.productCode + "           " + root.productCodeDesc + "    " + root.workOrder + "     "
lstTxt =  list(fullString)

text = 		ticker.text
count = 	ticker.tickerLetterCount
maxLength = ticker.maxChars


count = 0 if count>=len(lstTxt) else count

txt = text[1:] + lstTxt[count] if len(text)>=maxLength-1 else text + lstTxt[count]
ticker.tickerLetterCount = count + 1
ticker.text = txt