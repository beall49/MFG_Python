"""
	sets the arrow direction for oee
	@parms val = incoming new oee value
"""
from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs, getColumnHeaders as getHeaders
root = self
#oee values over the last hour
data= toPyDs(root.runOEE60Vals)
headers = getHeaders(root.runOEE60Vals)
tempList = []
#current direction
flag = root.runOEE60Direction

# if the sum of the current ds <1 reset it with the new value
if sum(int(row[0]) for row in data if row[0]>0) <1:
	ds = toDs(headers, [[val]])
	root.runOEE60Vals = ds		
else:	
	#add new val to ds and only keep last 30 entries
	tempList.extend([val])
	tempList.extend ([int(row[0]) for indx, row in enumerate(data) if row[0]>0 and indx< 30])			
	if sum(tempList)>0: 
		#check stats algo and set flag direction
		flag = 1 if project.stats.beta(tempList)> 0 else ( -1 if project.stats.beta(tempList)<-0.09 else 0)
	#insert ds as new oee ds
	root.runOEE60Vals = toDs(headers, ([[i] for i in tempList]))

root.runOEE60Direction = flag