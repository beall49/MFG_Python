if(event.propertyName == 'act' or event.propertyName == 'tgt' or event.propertyName == 'tot'): 
	from java.awt import Color
	clear = system.gui.color(255,255,255,0)
	white = Color.white
	black = system.gui.color(33,33,33) 
	grey = system.gui.color(213,213,213)
	warning = system.gui.color(240,173,78)
	bg = (event.source.cssBG)
	success = system.gui.color(92,184,92)
	
	colorsAhead = [grey, black, grey, bg]
	colordBehind = [grey, bg, black, bg]
	colorsOnTarget = [grey, grey, black, bg]
	
	actual = event.source.act
	target = event.source.tgt
	total  = event.source.tot
	
	spacer = .02 * total	
	
	#Cap at the total217,83,79
	if target >= total:
		target = total
	if actual >= total:
		actual = total
	
	#dataset = event.source.PacerDataSet
	headers = ["Label", "pos1", "pos2", "pos3", "pos4"]
	rows = []
	
	pacerbar = event.source.getComponent('PacerBar')
	if target > actual:
	#we are behind
		pacerbar.seriesColors = colordBehind
		pos1 = actual
		pos2 = target - actual
		pos3 = spacer
		pos4 = total - (pos1+pos2+pos3)	
	elif target==actual:
		pacerbar.seriesColors = colorsOnTarget		
		pos1 = actual
		pos2 = 0
		pos3 = spacer
		pos4 = total - (pos1+pos2+pos3)
	else:
		pacerbar.seriesColors = colorsAhead 	
		pos1 = target
		pos2 = spacer
		pos3 = actual-(target+spacer)
		pos4 = total - (pos1+pos2+pos3)

	# check is pos2 is < 0, if so set it to 0
	if (pos1 < 0):
		pos1 = 0	
	
	# check is pos2 is < 0, if so set it to 0
	if (pos2 < 0):
		pos2 = 0	
		
	# check is pos2 is < 0, if so set it to 0
	if (pos3 < 0):
		pos3 = 0		

	# check is pos4 is < 0, if so set it to 0
	if (pos4 < 0):
		pos4 = 0
	
	# Set data
	onerow = ["", pos1, pos2, pos3, pos4]
	rows.append(onerow)
	data = system.dataset.toDataSet(headers, rows)
	pacerbar.data = data