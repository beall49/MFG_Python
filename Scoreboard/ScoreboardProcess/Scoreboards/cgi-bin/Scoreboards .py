from __future__ import with_statement
#MUST PRINT THIS LINE FOR HEADER REASONS
print "Status: 200 OK;Content-Type: text/plain;charset=utf-8\n"

from datetime import datetime, timedelta
import os,subprocess, glob,  sys, time, shutil
from threading import Thread

#//////////////////////////////SET GLOBAL VARS
newDataSet = [];pids = []
snapShotLocation = 'C:\\Program Files\\RealVNC\\VNC Viewer\\vncsnapshot-1.2a\\vncsnapshot.exe'
password='-passwd'
passwordLocation='C:\\Program Files\\RealVNC\\VNC Viewer\\vncsnapshot-1.2a\\passwd'
folder = '\\\\MODAPP254\\c$\\inetpub\\wwwroot\\Scoreboard\\'
destination=folder+datetime.today().strftime("%H")
txtFile=folder +'runtimes.txt'

#remove everything from destination
for f in glob.glob(destination+ '\\*.jpg'): os.remove(f)

#///////////////////////////////WRITE TO RUNTIMES
def writeRunTime():
    with open(txtFile, "a") as text_file:    text_file.write(datetime.now().strftime("%m/%d/%Y %H:%M:%S") +" -- " + os.getenv('USERNAME') + "\n")
    
#//////////////////////////////START OF REFRESH DATE DEF
def refreshDate():
    #CREATE A REPLACE EXPRESSION
    replaceExp = "Refresh Time: " + (datetime.now() + timedelta(days=0)).strftime("%m/%d/%Y %H:%M:%S") +"</td>\n"
    scrbrd = os.path.normpath(folder + 'Scoreboards.html')
     
    #LOOP THROUGH AND FIND THE REFRESH DATE
    with open(scrbrd, "r") as txt:       
        for line in txt:
            if "Refresh Time: " in line:
                replaceMe = line            
                txt.close()
                break
            
    #PUT THE CONTENTS IN A VAR AND REPLACE THE TEXT                
    contents = open(scrbrd, "r").read() 
    replaced_contents = contents.replace(replaceMe,replaceExp)
    txt.close
     
    #WRITE THE NEW HTML TO THE FILE
    txt = open(scrbrd, "w").write(replaced_contents)



#//////////////////////////////SCREENSHOT FUNCTION
def getScreenShot(ip, filename): 
    from subprocess import Popen 
    args =[snapShotLocation,password,passwordLocation,ip + '::5900',filename ]
    p = Popen(args, shell=True)
    pids.append(p.pid)      

def timestampImages():
    from PIL import Image, ImageDraw, ImageFont
    import time, os
    #import _imagingft
    # *** parameters -- Modify according to your needs! ***
    mydir = "\\\\MODAPP254\\c$\\inetpub\\wwwroot\\Scoreboard\\"
    #\\\\MODAPP254\\c$\\inetpub\\wwwroot\\Scoreboard\\ "C:\\Users\\rbeall\\Desktop\\"
    topLeftWidthDivider = 6 # increase to make the textbox shorter in width
    topLeftHeightDivider = 23 # increase to make the textbox shorter in height
    textPadding = 2 # 
    
    # *** real work ***
    fileList = os.listdir(mydir)
    for fileName in fileList:
        if fileName.endswith(".jpg"):
            fileName2 = fileName.split('.')
            fileInfo = os.stat(mydir + fileName)
            timeInfo = time.strftime("%m.%d %H:%M:%S", time.localtime(fileInfo.st_mtime))       
            im = Image.open(mydir + fileName)
            topLeftWidth = int(im.size[0] - (im.size[0] / topLeftWidthDivider))
            topLeftHeight = int(im.size[1] - (im.size[1] / topLeftHeightDivider))
            draw = ImageDraw.Draw(im)
            draw.rectangle([topLeftWidth, topLeftHeight, im.size[0], im.size[1]], fill="black")
            font = ImageFont.truetype("arial.ttf", 45)
            draw.text([topLeftWidth + textPadding, topLeftHeight + textPadding], timeInfo, fill="lime", font=font)
            del draw
            
            #write image
            im.save(mydir + fileName2[0] + ".jpg", 'JPEG')

 
#//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
''''START OF THE MAIN RUN'''

#//////////////////////////////GRAB THE FOLDER
files = glob.glob(folder + '*.jpg')

#//////////////////////////////CLEAR EM OUT
for f in files: os.remove(f)   
    
#//////////////////////////////GET THE LIST    
ip_list = os.path.normpath(folder + 'ips.txt')

#//////////////////////////////OPEN THE LIST AND PUT INTO DATASET
with open(ip_list, "r") as txt: dataSet = txt.readlines()

#//////////////////////////////SPLIT UP THE DATAS
for row in dataSet:  newDataSet.append(row.split())
  
#//////////////////////////////GET THE PICS    
for row in newDataSet: 
    t = Thread(target=getScreenShot,args=(str(row[2]),(folder + str(row[0]) +'_' + str(row[1]) + '.jpg')))
    t.daemon = True
    t.start()
    t.join()
    
#//////////////////////////////CHANGE THE REFRESH DATE IN HTML
# refreshDate()                 
writeRunTime()


#//////////////////////////////WE HAVE TO MANUALLY EXIT TO CLEAN UP ALL THE THREADS

time.sleep(30)
timestampImages()
#DO THE COPY AND PASTE
for src in glob.glob(folder + '*.jpg'): shutil.copy2(src, destination )
subprocess.call("taskkill /f /im vncserver.exe ", shell=True)
subprocess.call("exit ", shell=True)
sys.exit()
