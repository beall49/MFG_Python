from __future__ import with_statement
import os, glob, sys, time, shutil, subprocess
from datetime import datetime, timedelta
from threading import Thread
from subprocess import PIPE, Popen
from PIL import Image, ImageDraw, ImageFont


class Scoreboard():

    # SET GLOBAL VARS
    def __init__(self):        
        self.snapShotLocation = r"C:\\Program Files\RealVNC\VNC Viewer\vncsnapshot-1.2a\vncsnapshot.exe"
        self.password = "-passwd"
        self.passwordLocation = r"C:\\Program Files\RealVNC\VNC Viewer\vncsnapshot-1.2a\passwd"
        self.mainFolder = r"\\MODAPP254\c$\inetpub\wwwroot\JobJackets\Scoreboards" + "\\" 
        
        self.txtFile = self.mainFolder + "runtimes.txt"
        self.hourFolder = self.mainFolder + datetime.today().strftime("%H") + "\\"
        self.ipTxt = os.path.normpath(self.mainFolder + "ips.txt")
        self.ipList = []
        self.pids = []
        
        
    def main(self):
        self.moveFiles()
        self.deleteFiles()
        self.createIpList()
        self.threadScreenShots()
        time.sleep(30)
        self.timestampImages()
        self.cleanUp()
        
	#this will put a timestamp on the bottom right of the img
    def timestampImages(self):   
        topLeftWidthDivider = 6  # increase to make the textbox shorter in width
        topLeftHeightDivider = 23  # increase to make the textbox shorter in height
        textPadding = 2  # 

        #loop through all images, get the current time, and write it to the image
        for fileName in glob.glob(self.mainFolder + "*.jpg"):
            timeInfo = time.strftime("%m.%d %H:%M:%S")       
            im = Image.open(fileName)
            topLeftWidth = int(im.size[0] - (im.size[0] / topLeftWidthDivider))
            topLeftHeight = int(im.size[1] - (im.size[1] / topLeftHeightDivider))
            draw = ImageDraw.Draw(im)
            draw.rectangle([topLeftWidth, topLeftHeight, im.size[0], im.size[1]], fill="black")
            font = ImageFont.truetype("arial.ttf", 45)
            draw.text([topLeftWidth + textPadding, topLeftHeight + textPadding], timeInfo, fill="lime", font=font)
            del draw

            # write image
            im.save(fileName, 'JPEG') 
   
    #takes an ip and filename and passes it to the vnc snapshot function
    def getScreenShot(self, ip, filename): 
        args = [self.snapShotLocation, self.password, self.passwordLocation, ip + '::5900', filename ]
        p = Popen(args, shell=True)
        self.pids.append(p.pid)     
       
    #makes it multithreaded so it goes faster
    def threadScreenShots(self):
        for row in self.ipList:
            t = Thread(target=self.getScreenShot, args=(str(row[2]), (self.mainFolder + str(row[0]) + '_' + str(row[1]) + '.jpg')))
            t.daemon = True
            t.start()
            t.join() 
    
	#write refresh time in html (not using it anymore)
    def writeTime(self):
        with open(self.txtFile, "a") as text_file:    
            self.text_file.write(datetime.now().strftime("%m/%d/%Y %H:%M:%S") + " -- " + os.getenv('USERNAME') + "\n")
          
	#get ips from txt file
    def createIpList(self):
        with open(self.ipTxt, "r") as txt: 
            for row in txt.readlines():  
                self.ipList.append(row.split())
      
	#move the files to their correct folder
    def moveFiles(self):
        # DO THE COPY AND PASTE
        [shutil.copy2(src, self.hourFolder) for src in glob.glob(self.mainFolder + '*.jpg')] 
	
	#delete the old ones
    def deleteFiles(self):
        for src in glob.glob(self.mainFolder + '*.jpg'):
            os.remove(src)      
                   
    def cleanUp(self):        
        subprocess.call("taskkill /f /im vncserver.exe ", shell=True)
        subprocess.call("exit ",  shell=True)
        sys.exit()

#if you wanna call this from teh web you need this
print """Status: 200 OK;Content-Type: text/plain;charset=utf-8\n
        <HTML>
            <H1>The old page is <br/>
                <a href="Scoreboards.html" onclick="setTimeout(function(){var ww = window.open(window.location, '_self'); ww.close(); }, 1000);">
                    If you click on this the window will be closed after 1000ms
                </a>
            </H1>
        </html>\n\n\n\n\n\n\n"""
Scoreboard().main()
