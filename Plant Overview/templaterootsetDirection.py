	from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs, getColumnHeaders as getHeaders
	root = self
	data= toPyDs(root.oeeVals)
	
	headers = getHeaders(root.oeeVals)
	tempList = []
	flag = root.direction
	if sum(int(row[0]) for row in data if row[0]>0) <1:
		ds = toDs(headers, [[val]])
		root.oeeVals = ds		
	else:	
		tempList.extend([val])
		tempList.extend ([int(row[0]) for indx, row in enumerate(data) if row[0]>0 and indx< 30])			
		if sum(tempList)>0: 
			flag = 1 if project.stats.beta(tempList)> 0 else ( -1 if project.stats.beta(tempList)<-0.09 else 0)

		root.oeeVals = toDs(headers, ([[i] for i in tempList]))
	
	root.direction = flag	