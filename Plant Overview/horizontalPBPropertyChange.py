#removes a lot of the crap on the chart
def styleChart(cht):
	plot = (cht.chart.plot)
	#not accesible from within the range/domain api
	plot.setOutlineVisible(False)
	plot.setRangeGridlinesVisible(False)
	
	#set the axes
	range = plot.rangeAxis
	domain = plot.domainAxis
	
	#remove range tick marks, labels, and lines
	range.setTickMarksVisible(False)

	range.setTickLabelsVisible(False)
	range.setAxisLineVisible(False)
	
	#remove value lables and tick marks
	domain.setAxisLineVisible(False)
	domain.setTickLabelsVisible(False)
	

pacerBar = event.source.getComponent('PacerBar')

#trying to catch on open
if event.propertyName == 'componentRunning':
	styleChart(pacerBar)
	
if(event.propertyName in ['act','tgt','tot']): 
	# This script was generated automatically by the property set
	# script builder. You may modify this script, but if you do,
	# you will not be able to use the property set builder to update
	# this script without overwriting your changes.

			
	from java.awt import Color
	clear = system.gui.color(255,255,255,0)
	white = Color.white
	black = system.gui.color(33,33,33) 
	grey = system.gui.color(213,213,213)
	warning = system.gui.color(240,173,78)
	bg = (event.source.cssBG)
	success = system.gui.color(92,184,92)
	
	colorsAhead = [grey, black, grey, bg]
	colordBehind = [grey, bg, black, bg]
	colorsOnTarget = [grey, grey, black, bg]
	
	actual = event.source.act
	target = event.source.tgt
	total  = event.source.tot
	
	spacer = .02 * total	
	
	#Cap at the total217,83,79
	if target >= total:
		target = total
	if actual >= total:
		actual = total
	
	#dataset = event.source.PacerDataSet
	
	
	headers = ["Label", "pos1", "pos2", "pos3", "pos4"]
	rows = []
	
	
	if target > actual:
	#we are behind
		pacerBar.seriesColors = colordBehind
		pos1 = actual
		pos2 = target - actual
		pos3 = spacer
		pos4 = total - (pos1+pos2+pos3)	
	elif target==actual:
		pacerBar.seriesColors = colorsOnTarget		
		pos1 = actual
		pos2 = 0
		pos3 = spacer
		pos4 = total - (pos1+pos2+pos3)
	else:
		pacerBar.seriesColors = colorsAhead 	
		pos1 = target
		pos2 = spacer
		pos3 = actual-(target+spacer)
		pos4 = total - (pos1+pos2+pos3)

	# check is pos2 is < 0, if so set it to 0
	if (pos1 < 0):
		pos1 = 0	
	
	# check is pos2 is < 0, if so set it to 0
	if (pos2 < 0):
		pos2 = 0	
		
	# check is pos2 is < 0, if so set it to 0
	if (pos3 < 0):
		pos3 = 0		

	# check is pos4 is < 0, if so set it to 0
	if (pos4 < 0):
		pos4 = 0
	
	# Set data
	onerow = ["", pos1, pos2, pos3, pos4]
	rows.append(onerow)
	data = system.dataset.toDataSet(headers, rows)
	pacerBar.data = data
	styleChart(pacerBar)