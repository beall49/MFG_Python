#create a new dataset for the final result
headers = ["SKU", "SKU_DESC", "WORK_ORDER", "WO_QTY", "PRD_UOM", "EA", "LOWEST", "EA_TO_DISPLAY", "DISP_UOM", "START_DATE", "START_TIME", "END_DATE", "END_TIME", 
"PRD_RSC", "PRD_MTHD", "imprp6","imsrp5","LINE","RATE_LINE", "RATE", "LOAD"]
data = []
uomConversions={}

#get startdate, enddate, lines
root =  system.gui.getWindow('Main Windows/Integration').getRootContainer()
startDate = root.getComponent('Dates').getComponent('StartDate').text
endDate = root.getComponent('Dates').getComponent('EndDate').text
lines = root.getComponent('Lines').getComponent('LineList').text

#get jde dates
startDate = system.db.runScalarPrepQuery("SELECT DD@JUL FROM F5GDATE WHERE DD@EDT = ? FETCH FIRST 1 ROWS ONLY", [startDate], 'jde') 
endDate = system.db.runScalarPrepQuery("SELECT DD@JUL FROM F5GDATE WHERE DD@EDT = ? FETCH FIRST 1 ROWS ONLY", [endDate], 'jde') 

#format lines list
lines =  ','.join("'" + str(i) + "'" for i in lines.split(","))

#qry
results = system.db.runPrepQuery("""
WITH prd_rates
		 AS (SELECT
					 CASE WHEN QQ@LIN = 82 THEN 80 ELSE qq@lin END AS QQ@LIN2,
					 qq@lin,
					 QQ@BTC,
					 qq@CSS,
					 qqsrp8,
					 qq@szc
				 FROM
					 f5m201
				 WHERE
					trim(QQMCU) = 'MID' AND
					qqefft >= 114001)
SELECT
	DISTINCT
	( walitm )   AS SKU,
	waitm        AS SHORT_ITEM,
	WADL01       AS SKU_DESC,
	wadcto || wadoco    AS WORK_ORDER,
	wauorg * .01 AS WO_QTY,
	imuom8       AS PRD_UOM,
	1            AS EA_TO_CS,
	wauorg * .01 AS EA,
	imuom1       AS LOWEST,
	D1.dd@edt    AS START_DATE,
	w$ddts       AS START_TIME,
	D2.dd@edt    AS END_DATE,
	w$endt       AS END_TIME,
	QQ@BTC       AS PRD_RSC,
	ibsrp8       AS PRD_MTHD,
	qqsrp8,
	imprp6,
	imsrp5,
	wawr03       AS LINE,
	qq@lin       AS RATE_LINE,
	qq@CSS       AS RATE,
	'Y'          AS LOAD
FROM
	F4101,
	F4801,
	F4801$,
	f4102,
	prd_rates,
	f5gdate AS D1,
	f5gdate AS D2
WHERE
	waitm = w$itm AND
	imitm = waitm AND
	wadoco = w$doco AND
	imitm = ibitm AND
	wamcu = ibmcu AND
	imprp6 = qq@szc AND
	qqsrp8 = ibsrp8 AND
	wastrt = D1.dd@jul AND
	W$ENDD = D2.dd@jul AND
	wastrt >= ? AND
	wastrt <= ? AND
	qq@btc != 'BKR90' AND
	qq@lin2 = wawr03 AND
	wawr03 IN ( """ + lines + """) AND
	wasrst IN ( 15, 20, 27, 30, 31, 92 )
ORDER  BY
	D1.dd@edt DESC """, [startDate, endDate  ], 'JDE')

#create list of shortitems
shortItems = ','.join(str(int(row["SHORT_ITEM"])) for row in results if str(row["PRD_UOM"]).strip() != str(row["LOWEST"]).strip())

if len(shortItems)>0:
	#get all the production to lowest converions for those items 
	uomResults = system.db.runQuery(
	"""select umum as FROM_UOM, umconv *.0000001 as CONVERSION, umitm as SHORT_ITEM from f41002 where umitm in 
	("""+shortItems+""")
	""", 'JDE')
	#put the results into a dictionary SHORT_ITEM UOM (e.g. 123456 CS = 1000) for easy querying
	for uom in uomResults:
		key = str(int(uom["SHORT_ITEM"]))+" " + str(uom["FROM_UOM"])
		uomConversions[key] = int(uom["CONVERSION"])
	

"""
	loop back through the original list adding the conversions, if production = primary, then the uom conversion is 1->1
    note, we might want to make this 1000->1 (maybe)
	else use the production to primary conversion
"""	
for result in results:
	prdUom =  str(result["PRD_UOM"]).strip()
	lowestUom = str(result["LOWEST"]).strip()
	shortItem =  str(int(result["SHORT_ITEM"]))
	#default the conversion as 1 to 1, if there is another conversion, we will find it in the dictionary 
	conv = 1
	if prdUom != lowestUom:
		#build the appropriate key
		key = str(int(result["SHORT_ITEM"]))+ " " + str(result["PRD_UOM"])
		conv = uomConversions[key]
		
	sku = str(result["SKU"]).rstrip()
	sku_desc = str(result["SKU_DESC"]).rstrip()

	wo_qty = result["WO_QTY"]
	prd_uom = str(result["PRD_UOM"]).strip()
	ea_to_display_uom = conv
	ea = wo_qty * conv
	lowest = result["LOWEST"]
	disp_uom = prdUom
	#hardcode to handle the display conversion from eaches to thousands
	if prdUom == lowestUom:
		ea_to_display_uom = 1000
		disp_uom = " K"
	start_date = result["START_DATE"]
	start_time = result["START_TIME"]
	end_date = result["END_DATE"] 
	end_time = result["END_TIME"] 
	prd_rsc = str(result["PRD_RSC"]).strip()
	prd_mthd = str(result["PRD_MTHD"] ).strip()
	imprp6 = str(result["IMPRP6"]).strip()
	imsrp5 = str(result["IMSRP5"]).strip()
	line = str(result["LINE"]).strip()
	rate_line = str(result["RATE_LINE"]).strip()
	rate = int(result["RATE" ] )
	load = str(result["LOAD"]).strip()
	
	# Attention: Here is the hard code to deal with the printing and firebranding
	work_order = str(result["WORK_ORDER"]).strip() + '-F' if prd_mthd == 'CRKFEP' and rate_line == '82' else str(result["WORK_ORDER"]).strip()
		
	data.append([sku,sku_desc,work_order,wo_qty,prd_uom,ea,lowest,ea_to_display_uom, disp_uom, start_date,start_time,end_date,end_time,prd_rsc,prd_mthd,imprp6,imsrp5,line,rate_line,rate,load])

root.Schedule = system.dataset.toDataSet(headers, data)
                      
