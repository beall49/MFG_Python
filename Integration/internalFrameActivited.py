qry = """SELECT CONVERT(VARCHAR(10), cast(cast(WEEK_START AS VARCHAR(8)) AS DATETIME2), 101)   AS WEEK_START, 
CONVERT(VARCHAR(10), cast(cast(week_end AS VARCHAR(8)) AS DATETIME2), 101) AS WEEK_END
FROM [dbo].[G3_Calendar] CUR_DATE WHERE CUR_DATE.Period_ID =	(SELECT		 CONVERT(NVARCHAR(8), GETDATE(), 112)) """


root =  system.gui.getWindow('Main Windows/Integration').getRootContainer()

results=system.db.runPrepQuery(qry)
weekStartDate, weekEndDate = results[0][0],  results[0][1]	

#get a list of lines from the line lookup table
lineListRestuls = system.db.runPrepQuery("select distinct JdeProductionLine from G3_LineLookup where JdeProductionLine is not null")
lines =  ','.join(str(row[0]) for row in lineListRestuls)


root.WeekStartDate = weekStartDate

root.WeekEndDate = weekEndDate

root.LineList = lines
