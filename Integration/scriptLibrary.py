def pollWorkOrderIn():
	from org.apache.log4j import Logger
	from java.util import Calendar
	from java.text import SimpleDateFormat
	import system
	import app
	import math
	
	log = Logger.getLogger("app.Integration.pollWorkOrderIn")
	log.info( "Polling Work Order In Table" )
	
	#
	# Notes: Need to handle change over time from JDA
	#
	
	#
	# Sadowski - 9/24/13, revisions to integration to get ready for startup
	# 	
	# Schedule Factor Comment: get all orders flagged as a 'N', lookup the appropriate MES line name based on the JDE line name
	# determine the schedule factor (this is used to account for breaks and lunches in the schedule rate). Apparently,
	# the schedule rate in JDE backs out the breaks and lunches, so we need a multiplier to bring them back in
	#  
	#
	# Sadowski - 1/15/2014, revisions to the integration for cork scheduling
	# 
	# Cork Scheduling: Bring down the work orders from JDE, determine the appropriate schedule rate for the production line and product
	# Because JDE doesn't have all the production lines in the system, mark the product associated with the work order as eligible 
	# to run on more than one production line by using line 80 or line 82. Don't schedule any work order that has the don't schedule flag = 'N'
	#
	# Sadowski - 9/9/2014, revisions to the integration to support a dynamic standard rate
	#
	# Sadowski - 10/17/2014, revisions to populate G3_WorkOrder and G3_ProductCode 
	#
	#
	
	query = """select w.*, 
		l.MesLinePath, 
		l.MesLineName,
		s.ShiftsPerDay,
		l.AutoSchedule,
		l.DefaultChangeover,
		l.OverrideDefaultRate,
		s.PlannedUnavailableTimePerShift,
		(1440.0 - (s.ShiftsPerDay * s.PlannedUnavailableTimePerShift)) / 1440.0 as schedule_factor
	from 
		G3_WorkOrders_In w, G3_LineLookup l, G3_ShiftLine s
	where
		w.JdeProductionLine = l.JdeProductionLine
		and l.MesLineName = s.Line
		and W.processed = 'N'		
	ORDER BY
		w.TransactionDateTime"""
	data = system.db.runQuery(query)
	
	#create a counter for logging
	orderCount = len(data)

	#with the addition of the cork room, the work order table now has the opportunity 
	# to get duplicate order numbers (e.g. Order 1 - Printer 1, Order 1 - Printer 2)
	# we will only update the work order quantuty, SKU, etc when the currOrder != prevOrder
	prevOrder = ""
	currOrder = ""
	#create a list of lineIds for a schedule cache update at the end of integration
	lineIdList = []
	
	#for each work order
	for row in data:			
		#get the required elements	
		id = row["ID"]
		workorder = row["WorkOrder"]
		productcode = row["FullSku"]
		description = row["FullSkuDesc"]
		linePath = row["MesLinePath"]
		lineName = row["MesLineName"]
		quantity = row["EachesToProduce"] 
		conversionToDisplay = row["EachesToDisplayUom"]
		dispUom = row["DisplayUom"]
		startdatetime = row["StartDate"] + " " + row["StartTime"]
		enddatetime = row["EndDate"] + " " + row["EndTime"]
		changeover = row["DefaultChangeover"]
		schedulerate = row["SchedulingRatePerHour"]
		scheduleRateFactor = row["schedule_factor"]
		autoSchedule = row["AutoSchedule"]
		overrideDefaultRate = row["OverrideDefaultRate"]
		productionMethod = row["ProductionMethod"]
		sopFamily = row["SOPFamily"]
		supplyFamily = row["SupplyFamily"]
		productionMethod = row["ProductionMethod"]
		jdeProductionLine = row["JdeProductionLine"]
		
		#set the previous work order to determine whether or not a change has occured
		currOrder = workorder
		log.info("Processing Order: " + str(workorder))
		###
		### Attention: See above comment for more details on the schedule rate
		###
		schedulerate = math.floor((scheduleRateFactor * schedulerate) / 60) #Sadowski 8/13/2013 -  / 60 to get the schedule in eaches per minute	
		###


		#Sadowski - 2/14/2014, I don't think that we should be checking for this since orders are scheduled by duration
		# Validation, make sure enddatettime >= startdatetime		
		#if (enddatetime <= startdatetime):
		#	# add a warning that the end time is before the start time
		#	app.Integration.addMsgToWorkOrderIn("  Error: End Time Prior to Start Time",id)
		#	log.info("  Warning: "+ workorder + " start time "+startdatetime+" <= "+enddatetime)
			
		# check to see if the product exists in the database, if not, update it
		if currOrder != prevOrder:
			
			log.info("  Found a new work order: "+ workorder + " adding/updating product code " + productcode)
			productcodeID = app.Integration.createOrUpdateProduct(productcode, description)
		
		# enable to product code to run on the production lines
		try:
			system.production.utils.updateProductCodeLineStatus(productcode, linePath, 1)
		except:
		 	system.production.updateProductCodeLineStatus(productcode, linePath, 1) 
	
		# Check if work order exists, if it does, update it, if it doesn't add it
		if currOrder != prevOrder:
			workOrderID = app.Integration.getIdForWorkOrder(workorder)
			if workOrderID > 0:
				log.info("  Found order ID: "+ str(workOrderID))
				try:
					#inductive prevents updates to work orders that have schedule entries against them. 
					system.schedule.updateWorkOrderEntry(workOrderID, workorder, productcode, quantity)
					log.info("  Updated order ID: "+ str(workOrderID))
				except:
					log.info("  Error updating work order, order scheduled")
					app.Integration.addMsgToWorkOrderIn("  Error updating work order entry, order scheduled",id)
			else:
				log.info("  Creating new order:")
				system.schedule.addWorkOrderEntry(workorder, productcode, quantity)
				workOrderID = app.Integration.getIdForWorkOrder(workorder)
				log.info("  Order Created with ID: "+str(workOrderID))
		
		#add in the additional factors to the G3_WorkOrder and G3_Products tables:
		if currOrder != prevOrder:	
			log.info("  Adding the following to G3_WorkOrderID: %s JdeScheduleDateTime: %s JdeProductionLine: %s"%(workOrderID, startdatetime, jdeProductionLine))
			app.Integration.updateG3WorkOrders(workOrderID,startdatetime, jdeProductionLine)
			
			log.info("  Adding the following to G3_ProductCode ID: %s, Production Method: %s, SupplyFamily: %s, SOP Family: %s"%(productcodeID, productionMethod, supplyFamily, sopFamily))
			app.Integration.updateG3Products(productcodeID, productionMethod, supplyFamily, sopFamily)
			
			
		# Get LineId
		try:
			lineID = system.production.utils.getLineID(linePath)
		except:
			lineID = system.production.getLineID(linePath)
					
		# Get Product Line Code ID
		#print productcode, lineName
		try:
			productCodeLineInfo = system.production.utils.getProductCodeLineInfo(True, productcode, lineName)
		except:
			productCodeLineInfo = system.production.getProductCodeLineInfo(True, productcode, lineName)
			
		# No Documentation on the return from this call, but I believe that col 5 has the productCodeLine
		try:
			productCodeLineId = productCodeLineInfo.getValueAt(0,5)
		except:
			productCodeLineId=0			
			app.Integration.addMsgToWorkOrderIn("Can't find productCodeLineId; productCode=%s, lineName=%s " % (productcode, lineName), id)
			
		#if the product code has a line id, set the Schedule Rate and the product outfeed properties
		if productCodeLineId > 0:
			log.info("  Got ProductCodeLineId: "+str(productCodeLineId))
			##if override the default rate is specified, then take the rate from JDE else go out and get the default rate from the model and assign it
			if overrideDefaultRate == 'Y':
				app.Integration.addProductLineProperty(linePath, ".Schedule Rate", schedulerate, productCodeLineId)
				log.info("  Updated Properties Schedule Rate: %s"%(schedulerate))
			else:
				defaultRate = 0
				properties = system.production.utils.getProductCodeLinePropertiesInfo(productCodeLineId, True)
				for row in range(properties.rowCount):
					if(str(properties.getValueAt(row, 1)).endswith('.Schedule Rate')):
						defaultRate = properties.getValueAt(row, 3)
						
				if defaultRate == 0:
					app.Integration.addMsgToWorkOrderIn("Unable to find a default schedule rate for the product, can't schedule", id)
					continue
				else:
					app.Integration.addProductLineProperty(linePath, ".Schedule Rate", defaultRate, productCodeLineId)
					log.info(" Set Product Rate to Default Schedule Rate: %s"%(defaultRate))
				
			##check to see if there is a standard rate override, if so, apply it.
			standardRateOverride = app.Integration.getStandardRateOverride(productionMethod)
			log.info(" Got Standard Rate from Override table: %s"%(standardRateOverride))
			if standardRateOverride > 0:
				app.Integration.addProductLineProperty(linePath,'.Product Outfeed.Count_Out.Standard Rate',standardRateOverride, productCodeLineId)	
				log.info(" Applied Standard Rate Override to: %s = %s"%(linePath+'.Product Outfeed.Count_Out.Standard Rate', standardRateOverride))
					
			app.Integration.addProductLineProperty(linePath, ".Eaches Per Case", conversionToDisplay, productCodeLineId)
			app.Integration.addProductLineProperty(linePath, ".Display UoM", dispUom, productCodeLineId)			
			#log.info("  Updated Eaches to Display as: %s %s "%(str(schedulerate), str(conversionToDisplay), str(dispUom)))
		else: 
			#this should never happen
			log.info("  Unable to find ProductCodeLineId")

		###########################
		#    if the Auto Schedule Flag = 'Y' then schedule the work order, else do nothing, the auto schedule flag was added to support the cork room scheduling process
		#    cork order are deliverd to the system, loaded and left unscheduled. 
		##########################
		if autoSchedule == 'Y':
			#check to see if a schedule entry already exists for the item
			scheduleEntryExists = app.Integration.schedulesExistForWorkOrderId(workOrderID)
			if scheduleEntryExists:
				#by design, if the work order already has an schedule entry, we will ignore it (for the time being)
				#Note, as of Aug-2014 Inductive does not support editing a run. There is no way to extend a run once it has started. 
				#The only way is to add a new run
				#I suspect that this will change once we add more lines, and get a more sophisticated integration 
				log.info("  Warning: a schedule entry exists for this order, work order updated, schedule entry ignored")
				app.Integration.addMsgToWorkOrderIn("A schedule entry exists for this order, work order updated, schedule entry not updated", id)
				continue
			else:
				app.Integration.scheduleByRateAndQty(lineID, workOrderID, schedulerate ,startdatetime, changeover, 0, quantity, "", "integration" )
		
		# Update g3_workorders_in table
		markAsProcessed = """
			update 
				G3_WorkOrders_In
			set 
				Processed = 'Y',
				ProcessedDateTime = CURRENT_TIMESTAMP
			where
				ID = ?""" 
		system.db.runPrepUpdate(markAsProcessed, [id])
		log.info("  Marked Work Order ID :" + str(id) + " as processed")
		prevOrder = currOrder
		
		#add the line id to the list for the schedule cache update at the end
		lineIdList.append(lineID)
		
		
	log.info("Completed Polling Work Order In Table")
	
	#for all the lines that had a work order, update the schedule cache
	#make the list unique
	lineIdList = list(set(lineIdList))
	for lineID in lineIdList:
		system.schedule.resetScheduleCache(lineID)	
		


def updateG3WorkOrders(workOrderID,startdatetime, jdeProductionLine):
	import system
	update = "UPDATE G3_WorkOrder SET JdeScheduleDateTime = ?, JdeProductionLine = ?, TimeStamp = CURRENT_TIMESTAMP where WorkOrderId = ?" 
	insert = "insert into G3_WorkOrder (WorkOrderId, JdeScheduleDateTime, JdeProductionLine, TimeStamp) values (?, ?, CURRENT_TIMESTAMP)"

	updateCount = system.db.runPrepUpdate(update, [startdatetime, jdeProductionLine, workOrderID])		
	#this should never run... 
	if updateCount == 0:
		system.db.runPrepUpdate(insert,  [workOrderID,jdeProductionLine, startdatetime])

	
def updateG3Products(productcodeID, productionMethod, supplyFamily, sopFamily):
	import system
	update = "UPDATE G3_ProductCode SET ProductionMethod = ?, SupplyFamily = ?, SOPFamily = ?, TimeStamp = CURRENT_TIMESTAMP where ProductCodeID = ?" 
	insert = "insert into G3_ProductCode (ProductCodeID, ProductionMethod, SupplyFamily, SOPFamily, TimeStamp) values (?, ?, ?, ?, CURRENT_TIMESTAMP)"

	updateCount = system.db.runPrepUpdate(update, [productionMethod, supplyFamily, sopFamily, productcodeID])		
	#this should never run... 
	if updateCount == 0:
		system.db.runPrepUpdate(insert,  [productcodeID, productionMethod, supplyFamily, sopFamily])
	
#
# deal with stupid JDE time formats
#
def jde_time_conversion(jdeTime):
	import time
	length = len(str(jdeTime))
	hours = "00"
	minutes = "00"
	seconds = "00"
	result = "00:00:00"

	if length == 0:
		return result
	elif length == 1:
		result = "00:00:0" + str(jdeTime)
	elif length == 2:
		result = "00:00:" + str(jdeTime)
	elif length == 3:
		seconds = str(jdeTime)[-2:]
		minutes = str(jdeTime)[:1]
		result = "00:0" + minutes + ":" + seconds
	elif length == 4:
		seconds = str(jdeTime)[-2:]
		minutes = str(jdeTime)[:2]
		result = "00:" + minutes + ":" + seconds
	elif length == 5:
		seconds = str(jdeTime)[-2:]
		minutes = str(jdeTime)[1:-2]
		hours = str(jdeTime)[:1]
		result = "0"+hours+":" + minutes + ":" + seconds
	elif length == 6:
		seconds = str(jdeTime)[-2:]
		minutes = str(jdeTime)[2:-2]
		hours = str(jdeTime)[:2]
		result = hours+":" + minutes + ":" + seconds
	#TODO:  fix this, it will throw an uncaught exception
	time.strptime(result, '%H:%M:%S')
	return result
	
#
# create and insert a schedule entry
# changeOverHandling 	0 = subtract the changeOver time from the start time to determine a new start time. Ex, startTime = 2 AM, 30 minute changeover, flag = 0 set the work order start to be 1:30 AM and the run start to be 2 AM
#						1 = add the changeOver time to the start time to determine when the order starts. Ex, startTime = 2 AM, 30 minute changeover, flag = 1 set the work order to start to be 2 AM and the run start to be 2:30 
#
def scheduleByRateAndQty(lineId, woId, workorderRate, startTimeStr, schedChangeOverInMins, changeOverHandling, quantity, note, userId):
  import system
  import app
  from java.util import Calendar
  from java.lang import String
  from org.apache.log4j import Logger

  
  log = Logger.getLogger("app.Integration.createSchedulesByRateAndQty")
  log.info( "  Scheduling Work Order id %s on line id %s by rate"%(woId, lineId) )

  dbDateFormat = '%1$tY-%1$tm-%1$td %1$tT'
	  
  startTime = app.Integration.createDate(startTimeStr)
  #startTime.set(Calendar.SECOND, 0)

  ScheduleType = 0
  Note = note
  EnteredBy = userId
  schedDuration = int(quantity / workorderRate)

  if changeOverHandling == 0:
  
		RunStartDateTime = Calendar.getInstance()
		RunStartDateTime.setTime(startTime)
		RunStartDateTimeFormatted = String.format(dbDateFormat, [RunStartDateTime.getTime()])  
	  
		StartDateTime = Calendar.getInstance()
		StartDateTime.setTime(RunStartDateTime.getTime())
		StartDateTime.add(Calendar.MINUTE, -1 * (schedChangeOverInMins)) # change over start time, add 2 minutes between consecutive entries
		StartDateTimeFormatted = String.format(dbDateFormat, [StartDateTime.getTime()] )
		
  else:
		StartDateTime = Calendar.getInstance()
		StartDateTime.setTime(startTime)
		StartDateTimeFormatted = String.format(dbDateFormat, [StartDateTime.getTime()] )

		RunStartDateTime = Calendar.getInstance()
		RunStartDateTime.setTime(StartDateTime.getTime())
		RunStartDateTime.add(Calendar.MINUTE, schedChangeOverInMins) # run start time
		RunStartDateTimeFormatted =  String.format(dbDateFormat, [RunStartDateTime.getTime()])

  finishTime = Calendar.getInstance()
  finishTime.setTime(RunStartDateTime.getTime())
  finishTime.add(Calendar.MINUTE, schedDuration) # run finish time
  FinishTimeFormatted =  String.format(dbDateFormat, [finishTime.getTime()])

  TimeStamp = Calendar.getInstance()
  TimeStampformatted =  String.format(dbDateFormat, [TimeStamp.getTime()])

  queryPart1 = String.format("INSERT INTO Schedule (LineID, WorkOrderID, ScheduleType, Note, StartDateTime, FinishDateTime, Quantity, EnteredBy, RunStartDateTime, TimeStamp, ActualQuantity)")
  queryPart2 = String.format(" VALUES(%s,%s,%s,'%s','%s','%s',%s,'%s','%s','%s',0)", [lineId, woId, ScheduleType, Note, StartDateTimeFormatted, FinishTimeFormatted, quantity, EnteredBy, RunStartDateTimeFormatted, TimeStampformatted])
  query = queryPart1 + queryPart2
  rowsChanged = system.db.runUpdateQuery(query) 
  log.info( "  Scheduled Work Order id %s on line id by rate, %s from %s to %s"%(woId, lineId, StartDateTimeFormatted, FinishTimeFormatted) )

  return finishTime
  
#
# create and insert a schedule entry
# changeOverHandling 	0 = subtract the changeOver time from the start time to determine a new start time. Ex, startTime = 2 AM, 30 minute changeover, flag = 0 set the work order start to be 1:30 AM and the run start to be 2 AM
#						1 = add the changeOver time to the start time to determine when the order starts. Ex, startTime = 2 AM, 30 minute changeover, flag = 1 set the work order to start to be 2 AM and the run start to be 2:30 
#
def scheduleByStartAndFinishTime(lineId, woId, startTimeStr, finishTimeStr, schedChangeOverInMins, changeOverHandling, quantity, note, userId):
    import system
    import app
    from java.util import Calendar
    from java.lang import String
    from org.apache.log4j import Logger
      
    log = Logger.getLogger("app.Integration.pollWorkOrderIn")
    log.info( "  Scheduling Work Order id %s on line id %s by time"%(woId, lineId) )
    
    dbDateFormat = '%1$tY-%1$tm-%1$td %1$tT'

    startTime = app.Integration.createDate(startTimeStr)
    finishTime = app.Integration.createDate(finishTimeStr)
    #startTime.set(Calendar.SECOND, 0)
  
    ScheduleType = 0
    Note = note
    EnteredBy = userId
    if changeOverHandling == 0:
    
		RunStartDateTime = Calendar.getInstance()
		RunStartDateTime.setTime(startTime)
		RunStartDateTimeFormatted = String.format(dbDateFormat, [RunStartDateTime.getTime()])  
	  
		StartDateTime = Calendar.getInstance()
		StartDateTime.setTime(RunStartDateTime.getTime())
		StartDateTime.add(Calendar.MINUTE, -1 * (schedChangeOverInMins)) # change over start time, add 2 minutes between consecutive entries
		StartDateTimeFormatted = String.format(dbDateFormat, [StartDateTime.getTime()] )
		
		RunFinishTime = Calendar.getInstance()
		RunFinishTime.setTime(finishTime)
		RunFinishTimeFormatted =  String.format(dbDateFormat, [RunFinishTime.getTime()])
    else:
		StartDateTime = Calendar.getInstance()
		StartDateTime.setTime(startTime)
		StartDateTimeFormatted = String.format(dbDateFormat, [StartDateTime.getTime()] )

		RunStartDateTime = Calendar.getInstance()
		RunStartDateTime.setTime(StartDateTime.getTime())
		RunStartDateTime.add(Calendar.MINUTE, schedChangeOverInMins) # run start time
		RunStartDateTimeFormatted =  String.format(dbDateFormat, [RunStartDateTime.getTime()])
   
		RunFinishTime = Calendar.getInstance()
		RunFinishTime.setTime(finishTime)
		RunFinishTime.add(Calendar.MINUTE, schedChangeOverInMins) # run start time
		RunFinishTimeFormatted =  String.format(dbDateFormat, [RunFinishTime.getTime()])
  
    TimeStamp = Calendar.getInstance()
    TimeStampFormatted =  String.format(dbDateFormat, [TimeStamp.getTime()])
  
    queryPart1 = String.format("INSERT INTO Schedule (LineID, WorkOrderID, ScheduleType, Note, StartDateTime, FinishDateTime, Quantity, EnteredBy, RunStartDateTime, TimeStamp, ActualQuantity)")
    queryPart2 = String.format(" VALUES(%s,%s,%s,'%s','%s','%s',%s,'%s','%s','%s',0)", [lineId, woId, ScheduleType, Note, StartDateTimeFormatted, RunFinishTimeFormatted, quantity, EnteredBy, RunStartDateTimeFormatted, TimeStampFormatted])
    query = queryPart1 + queryPart2
    rowsChanged = system.db.runUpdateQuery(query) 
    log.info( "  Scheduled Work Order id %s on line id %s by time, from %s to %s"%(woId, lineId, StartDateTimeFormatted, RunFinishTimeFormatted) )
   
    return finishTime  

#
# check if schedules exist to be run or completed as of the date
#
def checkSchedules(lineID, date):
  import system
  from java.lang import String
  
  dbDateFormat = '%1$tY-%1$tm-%1$td %1$tT'
  
  dateformatted = String.format(dbDateFormat, [date])
  query = "SELECT COUNT(ID) FROM Schedule WHERE FinishDateTime >= %s AND LineID = %s" %(dateformatted, lineID)
  idCount = system.db.runScalarQuery(query, dbConnectionName)
  return idCount

	  
# TODO: make this more robust, currently hardcoded to the format that I expect from the database
def createDate(strDate):
	from java.text import SimpleDateFormat
	df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
	return df.parse(strDate)

#
#	searches the work order table for the specified order, returns -1 or the WorkOrderID (-1 if nothing is found)
#
def getIdForWorkOrder(workOrder):
	import system
	query = "SELECT ID FROM WorkOrder WHERE WorkOrder = ?"
	results = system.db.runPrepQuery(query,[workOrder])
	id = -1
	if len(results)>0:
		return results[0][0] 
	return -1

#
#	searches the work order table for the specified order, returns -1 or the status of the work order (-1 if nothing is found)
#
def getWorkOrderStatus(workOrder):
	import system
	query = "SELECT Closed FROM WorkOrder WHERE WorkOrder = ?"
	results = system.db.runPrepUpdate(query,[workOrder])
	id = -1
	if len(results)>0:
		return results[0][0] 
	return -1

def addMsgToWorkOrderIn(msg, id):
	import system
	query = """
		UPDATE 
			G3_WorkOrders_In
		SET Processed = 'Y',
			ProcessedMsg = ?,
		ProcessedDateTime = CURRENT_TIMESTAMP
		WHERE ID = ?""" 
	system.db.runPrepUpdate(query,[msg, id])

#
# check to see if the product code and the product description match to what's in the database. 
# If the code exists, update the description (if different)
# 
def createOrUpdateProduct(productcode, description):
	import system
	query = "select * from productcode where productcode = ?"
	productdata = system.db.runPrepQuery(query, [productcode])
	
	#if there are no results from the above query, the product code doesn't exist
	#go ahead and add it using the method. If i
	if len(productdata) < 1:		
		system.production.utils.addProductCode(productcode, description)
		# Get product code id of the recently added item
		query = "select id from ProductCode where ProductCode = ?" 
		productdata = system.db.runPrepQuery(query, [productcode])
		if (len(productdata) > 0):
			productcodeid = productdata[0][0]	
		else:
			productcodeid = -1	
	else:
		desc = productdata[0][1]
		productcodeid = productdata[0][0]
		if desc != description:
			query = "update ProductCode set Description = ? where productcode = ?"
			system.db.runPrepUpdate(query, [description, productcode])
	return productcodeid


		
def addProductLineProperty(linePath, property, value, productCodeLineId):
	import system
	#note that the property should be passed in as ".Property"
	propertyPath = linePath + property
	value = str(value)
	update = """
		update
		 ProductCodeLineProperty
		SET 
		 PropertyValue = ?,
		 TimeStamp = CURRENT_TIMESTAMP
		WHERE ProductCodeLineId = ?
		AND PropertyPath = ?""" 
	insert = """
		insert into 
		 ProductCodeLineProperty(ProductCodeLineID, PropertyPath, PropertyValue, TimeStamp)
		values
		 (?, ?, ?, CURRENT_TIMESTAMP)						 
		""" 
	updateCount = system.db.runPrepUpdate(update, [value, productCodeLineId, propertyPath])		
	if updateCount == 0:
		system.db.runPrepUpdate(insert, [productCodeLineId, propertyPath, value])
	
def schedulesExistForWorkOrderId(workOrderId):
	import system
	query = " select count(*) from Schedule where WorkOrderId = ?"
	results = system.db.runPrepQuery(query, [workOrderId])
	if results[0][0] > 0:
		return True
	else:
		return False	

#checks for	an override to the standard rate, returns -1 if no override exists	
def getStandardRateOverride(productionMethod):
	import system
	query = 'select StandardRate from G3_StandardRateOverride where ProductionMethod = ?'
	results = system.db.runPrepQuery(query, [productionMethod])

	if len(results)>0:
		return results[0][0]
	else:
		return -1		
	return -1

		
#original calling method
def callingMethod():
	from datetime import datetime
	
	schedule = event.source.parent.getComponent('Table').data
	pds = system.dataset.toPyDataSet(schedule)
	
	curTime = datetime.now()
	workOrderList = ""
	
	for item in pds:
		sku =  str(item["SKU"]).strip()
		desc = str(item["SKU_DESC"]).strip()
		workorder =  str(item["WORK_ORDER"]).strip()
		qtyToProduce = item["WO_QTY"]
		uom =  str(item["PRD_UOM"]).strip()
		convToDisplay = str(item["EA_TO_DISPLAY"])
		displayUom = item["DISP_UOM"]
		eaToProduce = item["EA"]
		coDuration = 0 #vestigial, this will be set when the integration script fires, 
		#based on a lookup against the G3_LineLookup table. Eventually this will migrate to a lookup against the JDA changeover matrix
		##skip lowest uom will always be EA
		startDate = item["START_DATE"]
		startTime = int(item["START_TIME"])
		endDate = item["END_DATE"]
		endTime = int(item["END_TIME"])
		prodRsc =  str(item["PRD_RSC"]).strip()
		prdMthd =  str(item["PRD_MTHD"]).strip()
		#skipping QQ@SZC
		supFmly =  str(item["imprp6"]).strip()
		sopFmly = str(item["imsrp5"]).strip()
		prdLne =  str(item["RATE_LINE"]).strip()
		casesPerHour = item["RATE"]
		transactionDateTime = str(curTime)[:-7]
		
		strStartTime = app.Integration.jde_time_conversion(startTime)
		strEndTime = app.Integration.jde_time_conversion(endTime)
		
		add = item["LOAD"]
		if add == 'Y':
			system.db.runPrepUpdate("""insert into G3_WorkOrders_In 
			(FullSku,FullSkuDesc,WorkOrder,QuantityToProduce,uom,
			EachesToProduce,EachesToDisplayUom,DisplayUom,ChangeOverDurationInMin,
			StartDate,StartTime,EndDate,EndTime,ProductionResource,
			ProductionMethod,SOPFamily,SupplyFamily,JdeProductionLine,
			SchedulingRatePerHour,TransactionDateTime,Processed,ProcessedDateTime,ProcessedMsg) 
			values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,null,null)""",
			[sku,desc,workorder, qtyToProduce, uom, 
			eaToProduce, convToDisplay, displayUom, coDuration, startDate, strStartTime, 
			endDate, strEndTime, prodRsc, prdMthd, sopFmly, supFmly, 
			prdLne, casesPerHour,transactionDateTime,"N"])
			workOrderList = workOrderList + "'%s',"%(workorder)
			
	app.Integration.pollWorkOrderIn()
	
	workOrderList = workOrderList[:-1]
	system.nav.openWindow('Popups/Integration/WorkOrderErrors', {'WorkOrderList' : workOrderList})
		