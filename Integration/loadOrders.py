#I didn't write this
INSERT = """
			INSERT INTO G3_WorkOrders_In
			(
				FullSku,
				FullSkuDesc,
				WorkOrder,
				QuantityToProduce,
				uom,
				EachesToProduce,
				EachesToDisplayUom,
				DisplayUom,
				ChangeOverDurationInMin,
				StartDate,
				StartTime,
				EndDate,
				EndTime,
				ProductionResource,
				ProductionMethod,
				SOPFamily,
				SupplyFamily,
				JdeProductionLine,
				SchedulingRatePerHour,
				TransactionDateTime,
				Processed,
				ProcessedDateTime,
				ProcessedMsg
			)
			VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,getdate(),'N',null,null)
"""
root =  system.gui.getWindow('Main Windows/Integration').getRootContainer()
tbl = system.dataset.toPyDataSet(root.getComponent('Table').data)
workOrderList = str(','.join( str(row["WORK_ORDER"]).strip() for row in tbl if str(row["LOAD"]) == 'Y').split(",")).strip("[]")

for row in tbl:
	sku =  str(row["SKU"]).strip()
	desc = str(row["SKU_DESC"]).strip()
	workorder =  str(row["WORK_ORDER"]).strip()
	qtyToProduce = row["WO_QTY"]
	uom =  str(row["PRD_UOM"]).strip()
	convToDisplay = str(row["EA_TO_DISPLAY"])
	displayUom = row["DISP_UOM"]
	eaToProduce = int(row["EA"])
	#based on a lookup against the G3_LineLookup table. Eventually this will migrate to a lookup against the JDA changeover matrix
	##skip lowest uom will always be EA
	startDate = row["START_DATE"]
	startTime = int(row["START_TIME"])
	endDate = row["END_DATE"]
	endTime = int(row["END_TIME"])
	prodRsc =  str(row["PRD_RSC"]).strip()
	prdMthd =  str(row["PRD_MTHD"]).strip()
	#skipping QQ@SZC
	supFmly =  str(row["imprp6"]).strip()
	sopFmly = str(row["imsrp5"]).strip()
	prdLne =  str(row["RATE_LINE"]).strip()
	casesPerHour = int(row["RATE"])
	
	strStartTime = app.Integration.jde_time_conversion(startTime)
	strEndTime = app.Integration.jde_time_conversion(endTime)
	
	if str(row["LOAD"]) == 'Y':
		system.db.runPrepUpdate(INSERT,
		[sku,desc,workorder, qtyToProduce, uom,  eaToProduce, convToDisplay, displayUom, 0, startDate, strStartTime, 
		endDate, strEndTime, prodRsc, prdMthd, sopFmly, supFmly,  prdLne, casesPerHour])
		
app.Integration.pollWorkOrderIn()

system.nav.openWindow('Popups/Integration/WorkOrderErrors', {'WorkOrderList' : workOrderList})
