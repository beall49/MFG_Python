﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;

namespace SQL_Scripter
{
    class Program
    {
        public const String root = @"\\g3mesacc\d$\utils\Scripts\";
        static void Main(string[] args)
        {
            List<int> myList = new List<int> { 0,7 };
            if(!myList.Contains((int) DateTime.Today.DayOfWeek))
            {
                cleanScripts();
                createScripts();
            }
        }

        static void cleanScripts()
        {
            //only delete sql files (keep the last week)
            while (Directory.GetFiles(root, "*" + ".sql", SearchOption.TopDirectoryOnly).Length > 30)
            {
                //get all the files into array
                string[] fileEntries = Directory.GetFiles(root, "*" + ".sql", SearchOption.TopDirectoryOnly);
                //set the oldest, just use the first as starting point
                string oldestFile = fileEntries[0];
                
                //loop through .sql files
                foreach (string fileName in fileEntries)
                {
                    //if it's older than oldestFile set it to oldest
                    if (System.IO.File.GetLastWriteTime(fileName) < System.IO.File.GetLastWriteTime(oldestFile))
                    {
                        oldestFile = fileName;
                    }
                }
                //if it's oldest champ delete it
                File.Delete(oldestFile);
            }
        }

    
        static void createScripts()
        {
            //create script path
            String path = root + "output" + DateTime.Today.ToString("MMddyyyy") + ".sql";
            //create script file
            File.CreateText(path).Close();
            //create conn
            ServerConnection svrConn = new ServerConnection("g3mesacc");
            //login stuff
            svrConn.LoginSecure = false;
            svrConn.Login = "ignition";
            svrConn.Password = "F348diaB";
            //instantiate server/db
            Server svr = new Server(svrConn);
            Database db = svr.Databases["mes_runtime"];

            //string object
            StringBuilder output = new StringBuilder();
            
           


            output.AppendLine("/*\n****************\nSTARTING USPs\n****************\n*/");

            //loop through all sps
            foreach (StoredProcedure sp in db.StoredProcedures)
            {
                //only want usps
                if (sp.Owner != "sys")
                {
                    output.AppendLine("\n--**********START " + sp.Name + "*********\n");
                    //print em out
                    foreach (string usp in sp.Script())
                    {
                        output.AppendLine(usp + "\n");
                    }
                    output.AppendLine("--**********END " + sp.Name + "*********\n");
                }
            }

            output.AppendLine("/*\n\n****************\nSTARTING TABLES\n****************\n\n*/");
            //for each tbl
            foreach (Table tbl in db.Tables)
            {
                //only want our tables, g3_calendar is huge so forget about it
                if (tbl.Name.StartsWith("G3") && tbl.Name != "G3_Calendar")
                {
                    output.AppendLine("\n--**********START " + tbl.Name + "*********\n");
                    //set the options for the scripter, getdata, constraints and all keys
                    Scripter scripter = new Scripter(svr) { Options = { ScriptData = false, AgentAlertJob=true, DriAllConstraints = true, DriAllKeys = true, Triggers = true } };
                    var script = scripter.EnumScript(new SqlSmoObject[] { tbl });
                    //print em out
                    foreach (string line in script)
                    {
                        output.Append(line + "\n");
                    }
                    output.AppendLine("--**********END " + tbl.Name + "*********\n");
                }
            }

            File.WriteAllText(path, output.ToString());
            //Console.ReadKey();
        }
    
    }

}
