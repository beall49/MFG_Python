root = system.gui.getWindow('Popups/CheckList').getRootContainer()
tabs={"Shift":0, "Run":1, "Housekeeping":2, "Camera": 3}
tab = root.getComponent('tabType')

def setCntVisible(factor, root=root):
	import system
	root.getComponent('cntSubmit').getComponent('cnt' + factor).visible=True		
	root.getComponent('cntSubmit').val = system.tag.getTagValue(root.lineName + "/Factors/" + factor ) if system.tag.exists(root.lineName + "/Factors/" + factor ) else ""
			
			

# on tab change
if event.propertyName=='selectedTab':
	#put elec/mech to invisible
	root.getComponent('cntSubmit').getComponent('cntElectrician').visible=False
	root.getComponent('cntSubmit').getComponent('cntMechanic').visible=False
	
	#set the chkid
	chkId = tabs.get(tab.selectedTab,0) 
	root.chkId = chkId	
	
	#make the right one visible
	if chkId == 3:
		setCntVisible("Electrician")
	elif chkId == 1:
		setCntVisible("Mechanic")
	
	
	
	

	