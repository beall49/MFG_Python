if event.propertyName == "lineName":
	from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs	, getColumnHeaders as getCols
	root = system.gui.getWindow('Popups/CheckList').getRootContainer()
	"""
		get all the tabs for this line
		in the root container is a dataset with all of the different tabs, this checks to see which one is used on which line
		it returns the ones we need from the db, and then loops through the local dataset and adds the ones we need
	"""
	tbl = shared.SQLs.runQry([], "SELECT [TAB_DATA] FROM [MES_RUNTIME].[DBO].[G3_CHECK_LIST_TABS]  where line = '%s'" % root.lineName  )
	
	#loop through the returned ds and put the values in a python list
	tabs = [str(col) for row in tbl for col in row]
	#get the local tabs dataset (it's too wierd of a ds to store in the db)
	tbl = toPyDs(root.tabData)	
	headers = getCols(root.tabData)
	
	
	"""
		for each row in the local ds
		if the returned ds has that tab name
		add that row to your new ds
	"""
	data=[]
	for row in tbl:
		if row[1] in tabs:
		
			data.append([col for col in row])
	
	#set the dynamic tabs dataset
	data = toPyDs(toDs(headers, data))
	root.getComponent('tabType').tabData = data 
	root.getComponent('tabType').selectedTab = str(data[0][0])