if any([event.propertyName=='selectedRow', event.propertyName=='data']):
	from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs	
		
	root = system.gui.getWindow('Popups/CheckList').getRootContainer()
	tbl=toPyDs(root.getComponent('tblList').data)
	row=event.source.selectedRow
	
	#checking if the line clearance rows have been checked
	if row>-1:
		bool = False if tbl[row][1] == 0 else True
		if tbl[row][2] ==54:		
			root.getComponent('cntSubmit').getComponent('cntMechanic').getComponent('cntLineClearance').visible= bool
		elif tbl[row][2] ==53:
			root.getComponent('cntSubmit').getComponent('cntMechanic').getComponent('cntOpClearance').visible=bool