"""
	This will insert that someone has signed off on line clerance
"""
root = system.gui.getWindow('Popups/CheckList').getRootContainer()
chkId =root.chkId
runId=root.runId
shift=root.shift
lineName=root.lineName
container=root.getComponent('cntSubmit').getComponent('cntMechanic').getComponent('cntLineClearance')
password=container.getComponent('Password')
username=container.getComponent('Username')
success=None
try:
	success = system.security.validateUser(username.text, password.text, 'Network')
except:
	pass
if success:
	password.text, username.text="",""
	txId = system.db.beginTransaction(timeout=5000)	 
	system.db.runPrepUpdate("INSERT INTO [DBO].[G3_CHECK_LIST_LOG]([Q_ID], [RUN_ID], [SHIFT], [TRAN_ID], ENTERED_BY, LINE ) VALUES(54,?,?,?,?,?)", args=[runId, shift, txId, username.text, lineName], txt=txId)
	system.db.commitTransaction(txId)        
	system.db.closeTransaction(txId)  
	container.visible=False
else:
	username.requestFocusInWindow()
	
""" operator version """
root = system.gui.getWindow('Popups/CheckList').getRootContainer()
chkId =root.chkId
runId=root.runId
shift=root.shift
lineName=root.lineName
container=root.getComponent('cntSubmit').getComponent('cntMechanic').getComponent('cntOpClearance')
password=container.getComponent('Password')
username=container.getComponent('Username')
success=None
try:
	success = system.security.validateUser(username.text, password.text, 'default')
except:
	pass
if success:
	password.text, username.text="",""
	txId = system.db.beginTransaction(timeout=5000)	 
	system.db.runPrepUpdate("INSERT INTO [DBO].[G3_CHECK_LIST_LOG]([Q_ID], [RUN_ID], [SHIFT], [TRAN_ID], ENTERED_BY, LINE ) VALUES(53,?,?,?,?,?)", args=[runId, shift, txId, username.text, lineName], txt=txId)
	system.db.commitTransaction(txId)        
	system.db.closeTransaction(txId)  
	container.visible=False
else:
	username.requestFocusInWindow()	