def updateCheckList(tbl, runId, chkId, shift, lineName):
	import app, system        
	txId = system.db.beginTransaction(timeout=5000)	 
	myList=', '.join([str(row[2]) for row in tbl if row[1]==1])
	for id in myList.split(', '):	 
		system.db.runPrepUpdate("INSERT INTO [DBO].[G3_CHECK_LIST_LOG]([Q_ID], [RUN_ID], [SHIFT], [TRAN_ID], ENTERED_BY, LINE ) VALUES(?,?,?,?,?,?)", args=[ str(id), runId, shift, txId, str(system.security.getUsername()).upper(), lineName], txt=txId)
					  
	system.db.commitTransaction(txId)        
	system.db.closeTransaction(txId)        
