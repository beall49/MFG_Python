#set globals for the root and template container
global root; 
global container; 
from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs
from system.user import getUsers
from system.db import runScalarQuery as scalarQry
import re
root = system.gui.getWindow('Popups/CheckList').getRootContainer()
container = root.getComponent('cntTemplates')

def makeTemplate(name, lineName, index):
	import system, app
	from system.gui import moveComponent, resizeComponent
	from com.inductiveautomation.factorypmi.application.components.template import TemplateHolder
	#create instance of the template
	template = TemplateHolder()
	#build the template in memory/which template/enable it/name it
	template.initTemplate(container.getAppContext())	
	template.setTemplatePath("tempDropDown")  
	template.getLoadedTemplate().setEnableLayout(1)	
	template.setName(name)
		
	template.startupComponent(template.getAppContext())
	#add it to the container
	container.addComponent(template)
	#put it where you want it
	moveComponent(template,0,(index *67))
	#correctly size it
	resizeComponent(template,552,65)

#get the template data for this line	552,65
tbl = shared.SQLs.runQry([["LINE", "VARCHAR", root.lineName]], "[MES_RUNTIME].[DBO].[USP_GET_CHK_TEMPLATES]")

if len(tbl)>0:
	#gets the count of how many templates are needed
	templates =0
	
	#pulls all the unique template names out
	templateNames = list(set((row[1]) for row in tbl))
	
	#loop through the template names and make them (pass name and position)
	for name in templateNames: 
		makeTemplate(name, root.lineName, templateNames.index(name))
		cnt = root.getComponent('cntTemplates')
		cnt.getComponent(name).lineName = root.lineName


#the table of questions	
tbl = root.getComponent('tblList')
#use this to move templates down (by size)
sizes={0:350, 1:100}

#reset the column sizes
[tbl.setColumnWidth(x, sizes[x]) for x in range(0,len(sizes))]

#get the tab type (there's multple based on line)
tabs = toPyDs(root.getComponent('tabType').tabData)

#set selected tab
root.getComponent('tabType').selectedTab = str(tabs[0][0])

#gets initial checkid
root.chkId= scalarQry("SELECT ISNULL((SELECT TOP 1 START_TAB FROM [MES_RUNTIME].[DBO].[G3_CHECK_LIST_TABS] WHERE LINE = '%s'), 0) AS CHKID" % root.lineName)

# create lists for electricians and mechanic dropdowns, 
elec, mech=[['<Select One>']],[['<Select One>']]
dicts={"Electrician":elec, "Mechanic":mech}
roles = ["Electrician", "Mechanic"]

Users = getUsers('Network')
Users.addAll(getUsers('default'))

"""
	for each role in the roles
	for each user in Users 
	if this user has this role, 
	add this user to that specific role dictionary (electricians or mechanics)
"""	
[dicts[str(roles[y])].extend ([[user.get(user.Username)]] ) 	for user in Users for y in range(0,len(roles)) if re.search("\\b" + str(roles[y]) + "\\b", str(user.roles), flags=re.IGNORECASE)]

#put each one in their ds	
root.engineers= toPyDs(toDs(["users"],elec))		
root.mechanics= toPyDs(toDs(["users"],mech))
					