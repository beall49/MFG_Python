root = system.gui.getWindow('Popups/CheckList').getRootContainer()
chkId =root.chkId
runId=root.runId
shift=root.shift
lineName=root.lineName
cntSubmit=root.getComponent('cntSubmit')
components={1:cntSubmit.getComponent('cntMechanic').getComponent('ddMechanic'), 3:cntSubmit.getComponent('cntElectrician').getComponent('ddElectrician')}
tbl = root.getComponent('tblList')

def setTblData(tbl=tbl, chkId=chkId, runId=runId, shift=shift, lineName=lineName):
	from  system.dataset import toPyDataSet as toPyDs
	import app
	#inserting the answers
	app.Utils.updateCheckList(toPyDs(tbl.data), runId, chkId, shift, lineName)
	

	

def setAdlFactors(components=components, chkId=chkId, lineName=lineName, cntSubmit=cntSubmit):
	import app, system
	#is it's a type that takes mechancic or electrician
	if chkId in (1,3):		
		#become the dropdown
		dd=components[chkId]
		
		val =dd.selectedLabel
		
		type=dd.toolTipText
		#create a path to the factor tag
		path=(lineName + "/Factors/" + type)
		
		if val != "<Select One>":
			#if someone has been selected, write to the tag and container property
			system.tag.write(path, val)
			cntSubmit.val = val
			#reset the dropdown
			dd.selectedLabel="<Select One>"
			return True
		else:
			#if there's no value let them know
			if not cntSubmit.val:
				system.gui.warningBox("Please fill in %s name" %(type), "Blank Entry Found" )		
				return False
		return True
	else:
		return True

		
if setAdlFactors():
	setTblData()