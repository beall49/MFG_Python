from __future__ import  division
"""
	Please see cntOneACAfterUpdate.py, this is the same thing but at the 3rd screen level
	few small diferences
"""

def getUOM(sku, line):
	from app.Utils  import runStoredProc as EXEC
	parms = [["PRODUCT", "VARCHAR", sku],["CURRENTLINE", "VARCHAR", line]]		
	uom = EXEC(parms, "[MES_RUNTIME].[DBO].[USP_GET_UOM_CONV]")
	if uom:
		return uom[0][0]
	else:
		return 1000
		
def hoursBetween(d1, d2):
	from datetime import datetime, timedelta
	import time
	d1 = time.mktime(d1.timetuple())
	d2 = time.mktime(d2.timetuple())
	return ((d2-d1)/60)	
	
from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs, sort as sort, getColumnHeaders as getCols
from datetime import datetime
import operator, time
now = datetime.now()#time.strptime(str(datetime.now()),"%Y-%m-%d %H:%M")
root=system.gui.getWindow('Main Windows/End of Shift').getRootContainer()
container = root.getComponent('cntData').getComponent('cntTwo')
if container.visible == True:
	ac = container.getComponent('acRun').tableData
	tblRun = container.getComponent('tblRun')
	headers= getCols(ac); 
	tbl = toPyDs(ac)
	data=[]; runs={}; 

	data=[]
	if tbl:	  
		for row in tbl:
			shift, day, run, workOrder, sku, downtime, oee = row["Shift"], row["Shift Date"], row["Run"], row["Work Order"], row["Product Code"], row["Run Down Time"], row["OEE"]
			uom = getUOM(sku, root.line)
			prod = (row[5]/uom)
			waste = (row[6]/uom)
			runStart = datetime.strptime(str(row[9]), "%Y-%m-%d %H:%M") if row[9]!= None else now
			runEnd = datetime.strptime(str(row[10]), "%Y-%m-%d %H:%M") if row[10]!= None else now
	
			runs[workOrder]=(runStart, runEnd)			
			newRow = [shift, day, run, workOrder, sku, round(prod,2), waste, downtime, oee, runStart, runEnd]
	
			data.append(newRow)
			
	tblRun.data = toPyDs(sort(toDs(headers, data)	,1,True))
	
	tbl = toPyDs(tblRun.data)	
	if len(tbl)> 1:	
		start =  min(runs.iteritems(), key=operator.itemgetter(1))[1][0]
		end =  max(runs.iteritems(), key=operator.itemgetter(1))[1][1]
		shift, workOrder, sku,  day = 0, "*", "*",  tbl[0][1] 
		
		prod = sum(row[5] for row in tbl)
		waste = sum(row[6] for row in tbl)
		downtime = sum(row[7] for row in tbl)
		oee = sum(row[8] for row in tbl)/ (len(tbl))

		newRow = [shift, day, "", workOrder, sku, round(prod,2), waste, downtime, oee, start, end]
		tblRun.data= system.dataset.addRow(tblRun.data,0, newRow)	
	
	tblRun.selectedRow =0