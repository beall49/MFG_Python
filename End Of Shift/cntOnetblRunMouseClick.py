#click through event to drill down to that specific run
if event.clickCount == 2:
	from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs
	root=system.gui.getWindow('Main Windows/End of Shift').getRootContainer()
	cntData = root.getComponent('cntData')
	cnt	   = cntData.getComponent('cntOne')
	tblRun = cnt.getComponent('tblRun')
	row = event.source.selectedRow
	if row>-1:
		tbl = toPyDs(tblRun.data)
		if tbl[row][2] != "*":
			event.source.selectedRow = -1
			cnt.visible = False
			root.workOrder = tbl[row][2]
			cntData.getComponent('cntTwo').visible = True
		