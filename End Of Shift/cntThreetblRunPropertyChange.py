from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs
root=system.gui.getWindow('Main Windows/End of Shift').getRootContainer()
container = root.getComponent('cntData').getComponent('cntTwo')
tblRun = container.getComponent('tblRun')
tbl = toPyDs(tblRun.data)

sizes={0:60,1:170,2:85,3:85,4:85,5:85,6:85, 7:85}
#
if event.propertyName == "data":	
	event.source.selectedRow=-1
	[tblRun.setColumnWidth(x, sizes[x]) for x in range(0,len(sizes))]	
##
if event.propertyName == "selectedRow":
	row=event.source.selectedRow
	if row>=0:	
		if tbl[row]['Work Order']=="*":
			startDate = root.beginDate
			endDate = root.finalDate
			root.shift=""
		else:
			root.shift= tbl[row]["Shift"]
			startDate=tbl[row]['Shift Date']
			endDate=(tbl[row]['Actual Production Finish'] if row==len(tbl)-1 else tbl[row+1]['Shift Date'])
		acDowntime=root.getComponent('cntTables').getComponent('acDowntime')	
		acDowntime.startDate=startDate
		acDowntime.endDate=endDate