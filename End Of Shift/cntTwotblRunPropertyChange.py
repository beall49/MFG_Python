from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs
root=system.gui.getWindow('Main Windows/End of Shift').getRootContainer()
tblRun = root.getComponent('cntData').getComponent('cntThree').getComponent('tblRun')
tbl = toPyDs(tblRun.data)
sizes={0:150,1:85,2:100,3:85,4:85,5:85,6:85,7:85,8:140,9:140, 10:85}

#basically filters on new data or row selection
if root.getComponent('cntData').getComponent('cntThree').visible ==True:
	if event.propertyName == "data":	
		root.shift=""
		event.source.selectedRow=-1
		[tblRun.setColumnWidth(x, sizes[x]) for x in range(0,len(sizes))]	
	
	if event.propertyName == "selectedRow":
		row=tblRun.selectedRow
		if row>=0:			
			acDowntime=root.getComponent('cntTables').getComponent('acDowntime')
			if tbl[row]['Work Order']=="*":
				startDate = root.beginDate
				endDate = root.finalDate
				root.shift, root.line="",""
			else:
				root.shift, root.runName, root.line = tbl[row]["Shift"], tbl[row]["Run"], tbl[row]["Line"]
				startDate=tbl[row]['Shift Date']
				endDate=(tbl[row]['Actual Production Finish'] if row==len(tbl)-1 else tbl[row+1]['Shift Date'])
				
			acDowntime.startDate=startDate
			acDowntime.endDate=endDate