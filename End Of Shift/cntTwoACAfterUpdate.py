from __future__ import  division
"""
	Please see cntOneACAfterUpdate.py, 
	this is the same thing but at the 2nd screen level
	few small diferences
"""
def getUOM(sku, line):
	from app.Utils  import runStoredProc as EXEC
	parms = [["PRODUCT", "VARCHAR", sku],["CURRENTLINE", "VARCHAR", line]]		
	uom = EXEC(parms, "[MES_RUNTIME].[DBO].[USP_GET_UOM_CONV]")
	if uom:
		return uom[0][0]
	else:
		return 1000
		
def hoursBetween(d1, d2):
	from datetime import datetime, timedelta
	import time
	d1 = time.mktime(d1.timetuple())
	d2 = time.mktime(d2.timetuple())
	return ((d2-d1)/60)		
	
from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs, sort as sort, getColumnHeaders as getCols
from datetime import datetime
import operator, time
root=system.gui.getWindow('Main Windows/End of Shift').getRootContainer()
now = datetime.now()#time.strptime(str(datetime.now()),"%Y-%m-%d %H:%M")
container = root.getComponent('cntData').getComponent('cntThree')
ac = container.getComponent('acRun').tableData



data=[]; runs={}; 
if container.visible == True:
	tblRun = container.getComponent('tblRun')
	headers= getCols(ac) 
	tbl = toPyDs(ac)
	tblRun.data=toDs(headers, data)
	if tbl:	  
		for row in tbl:
			day, run, shift, workOrder, sku, desc, downtime, oee, line = row["Shift Date"], row["Run"], row["Shift"], row["Work Order"], row["Product Code"], row["Product Code Description"], row["Run Down Time"], row["OEE"], row["Line"]		
			uom = getUOM(sku, line)
			prod = (row["Line Production Count"]/uom)
			waste = (row["Line Waste Count"]/uom)
			runStart = datetime.strptime(str(row["Actual Production Start"]), "%Y-%m-%d %H:%M") if row["Actual Production Start"]!= None else now
			runEnd = datetime.strptime(str(row["Actual Production Finish"]), "%Y-%m-%d %H:%M") if row["Actual Production Finish"]!= None else now
#			prodRate =(row["Line Production Count"] / hoursBetween(runStart, runEnd)) if prod>0 else 0			
			runs[workOrder]=(runStart, runEnd)
			newRow = [day, run, shift, workOrder, sku, desc, line, round(prod,2), waste, downtime, oee, runStart, runEnd]
			
			data.append(newRow)
			
		tbl = toPyDs(toDs(headers, data))
		if len(tbl)>1:	
			start =  min(runs.iteritems(), key=operator.itemgetter(1))[1][0]
			end =  max(runs.iteritems(), key=operator.itemgetter(1))[1][1]
			
			workOrder, sku, desc, day, line, shift = "*", "*", "*",  tbl[0][0], "*", 0 
			prod = sum(row[7] for row in tbl)
#			prodRate = (prod /len(tbl)) if prod>0 else 0
			waste = sum(row[8] for row in tbl)
			downtime = sum(row[9] for row in tbl)
			oee= (sum(row[8] for row in tbl)/len(tbl))
			data.append([day, "", 0, workOrder, sku, desc, line, round(prod,2), waste, downtime, oee, start, end])
			tbl = sort(toDs(headers, data),6, True)
			
		tblRun.data=tbl