from __future__ import  division
#gets the uom from the db
def getUOM(sku, line):
	from app.Utils import runStoredProc as EXEC
	parms = [["PRODUCT", "VARCHAR", sku],["CURRENTLINE", "VARCHAR", line]]		
	uom = EXEC(parms, "[MES_RUNTIME].[DBO].[USP_GET_UOM_CONV]")
	if uom:
		return uom[0][0]
	else:
		return 1000

#time difference		
def hoursBetween(d1, d2):
	from datetime import datetime, timedelta
	import time
	d1 = time.mktime(d1.timetuple())
	d2 = time.mktime(d2.timetuple())
	return ((d2-d1)/60)	
	

from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs, sort as sort, getColumnHeaders as getCols
from datetime import datetime
import operator, time
root=system.gui.getWindow('Main Windows/End of Shift').getRootContainer()
now = datetime.now()
container = root.getComponent('cntData').getComponent('cntOne')

#if im visible
if container.visible == True:
	# get the analysis controller (ac) data
	ac = container.getComponent('acRun').tableData
	tblRun = container.getComponent('tblRun')
	headers= getCols(ac) 
	tbl = toPyDs(ac)
	
	
	data=[]; runs={}; 
	tblRun.data=toDs(headers, data)
	if tbl:	  
		for row in tbl:
			#getting data from the ac, format it for the tables
			day, run, workOrder, sku, desc, line, downtime, oee = row["Day"], row["Run"], row["Work Order"], row["Product Code"], row["Product Code Description"], row["Line"], row["Run Down Time"], row["OEE"]
			
			uom = getUOM(sku, root.line)
			#computed production and waste values
			prod = (row["Line Production Count"]/uom)
			waste = (row["Line Waste Count"]/uom)
			
			runStart = datetime.strptime(str(row["Actual Production Start"]), "%Y-%m-%d %H:%M") if row["Actual Production Start"]!= None else now
			runEnd = datetime.strptime(str(row["Actual Production Finish"]), "%Y-%m-%d %H:%M") if row["Actual Production Finish"]!= None else now
			#add it to the runs dict
			runs[workOrder]=(runStart, runEnd)
			#create a new row
			newRow = [day, run,  workOrder,  sku, desc, line,round(prod,2), waste, downtime, oee, runStart, runEnd]
			#add new row to data list
			data.append(newRow)
		#put all rows into a tbl
		tbl = toPyDs(toDs(headers, data))
		
		#if there's rows
		if len(tbl)>1:	
			#find the first and last run
			start =  min(runs.iteritems(), key=operator.itemgetter(1))[1][0]
			end =  max(runs.iteritems(), key=operator.itemgetter(1))[1][1]
						
			workOrder, sku,  day, shift = "*", "*",  tbl[0][0], 0 
			prod = sum(row[6] for row in tbl)
			waste = sum(row[7] for row in tbl)
			downtime = sum(row[8] for row in tbl)
			#oee avg
			oee= (sum(row[9] for row in tbl)/len(tbl))
			#append new data to tbl
			data.append([day, "",  workOrder, sku, "*", "*", round(prod,2), waste, downtime, oee, start, end ])
			tbl = sort(toDs(headers, data),4, True)

		#insert new data into tbl	
		tblRun.data=tbl