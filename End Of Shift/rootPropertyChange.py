from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs, getColumnHeaders as getCols
root=system.gui.getWindow('Main Windows/End of Shift').getRootContainer()

dataContainer = root.getComponent('cntData')
label = dataContainer.getComponent('lblDescr')
one = dataContainer.getComponent('cntOne')
two = dataContainer.getComponent('cntTwo')
three = dataContainer.getComponent('cntThree')

cnt = root.getComponent('cntSelect').getComponent
date = cnt('ddDate').text

if event.propertyName in ["visibleContainer", "line", "startDate", "runName", "workOrder", "shift"]:
	#just avoiding a big if statement, premaking the description
	strings = {1: ("Date: " + date + " - " + cnt('ddLine').selectedLabel + " - Shift " + cnt('ddShift').selectedLabel), 
	2:("Line: " + cnt('ddLine').selectedLabel + " -  Work Order: " + root.workOrder), 
	3:("Line: " + cnt('ddLine').selectedLabel + " -  Work Order: " + root.workOrder + " - Shift " + root.shift)}
	
	#based on the visible contianer, the label gets specific text
	label.text = strings[root.visibleContainer]
	
	#if event isnt workorder and container 1 is visile , clear the work order
	if all([event.propertyName != "workOrder", three.visible==False, two.visible==False]):
		root.workOrder = "" 


#if a checklist has been selected	
if event.propertyName in ["checkList","ckState"]:	
	tbl = root.checkList
	headers= getCols(tbl)
	state = root.ckState 
	
	#changes whats seen based on the checklist selection
	states ={1:"Completed", 2:"Incomplete"}
	tbl = [[row[0], row[1], row[2], row[3], row[4], row[5]] for row in toPyDs(tbl) if row[2]==states.get(state, row[2])]
	
	#inserts the correct data into the checklist questions table
	root.getComponent('cntTables').getComponent('tblChkList').data = toDs(headers,tbl)
	
