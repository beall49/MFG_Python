from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs
root=system.gui.getWindow('Main Windows/End of Shift').getRootContainer()
tblRun=root.getComponent('cntData').getComponent('cntOne').getComponent('tblRun')
sizes={0:175,1:85,2:80,3:200,4:85,5:125,6:85, 7:125, 8:85, 9:155, 10:155, 11:90}

"""
	resetting the data on new data 
	when a row is selected, affect other components (give them row and workorder to filter too)
"""	
if event.propertyName == "data":
	if root.getComponent('cntData').getComponent('cntOne').visible == True:
		root.runName=""	
		event.source.selectedRow=-1
		[tblRun.setColumnWidth(x, sizes[x]) for x in range(0,len(sizes))]	

if event.propertyName  in ["selectedRow", "selectedColumn"]:
	tbl = toPyDs(tblRun.data)
	row=event.source.selectedRow
	if row>-1:
		if tbl[row][2] == "*":		
			root.workOrder, root.runName = "", "" 
		else :
			root.workOrder, root.runName = tbl[row][2], tbl[row][1]
			 
		
		
