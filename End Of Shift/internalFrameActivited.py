def getDates():
	from java.util import Calendar
	cal = Calendar.getInstance()		
	return cal.getTime()

shared.logEntries.insertUser(system.gui.getParentWindow(event), system.util.getProjectName(), system.security.getUsername())

try:
	root=system.gui.getWindow('Main Windows/End of Shift').getRootContainer()
	cnt = root.getComponent('cntSelect')	
	cnt.getComponent('ddDate').date = getDates()
	root.workOrder, root.runId, root.ckState = "", 0, 0
except:
	pass
