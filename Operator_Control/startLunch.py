#note, there is probably a better way to do this. This depends on a hardcoded gateway tag change script....
lineName = event.source.parent.getComponent('Production Line Selector').selectedLineName
if system.tag.read(lineName + "/Util/lineStateId").value == 1:
	system.tag.writeToTag(lineName + '/Util/Break Start Time', event.source.Now)
	system.tag.writeToTag(lineName + '/Util/Break Minutes', 30)
	system.tag.writeToTag(lineName + "/Util/lineStateId", 12)
else:
	system.tag.writeToTag(lineName + '/Util/Break Minutes', 0)
	system.tag.writeToTag(lineName + "/Util/lineStateId", 1)
	