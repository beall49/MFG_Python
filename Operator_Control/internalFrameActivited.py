def selectLine(event=event):
	import system, app
	#get the parameters passed in from the client launch, 
	lineName 			= system.tag.getTagValue('[Client]LineName')
	systemLineName = system.tag.getTagValue('[Client]SystemLineName')
	systemLinePath = system.tag.getTagValue('[Client]SystemLinePath')
	canSwitchLines = system.tag.getTagValue('[Client]canSwitchLines')
	lineFilter 		= system.tag.getTagValue('[Client]lineFilter')
	
#	print "linename" + lineName, "system line name" + systemLineName, "system line path" + systemLinePath,  canSwitchLines, "linefilter" + lineFilter
	pls = system.gui.getParentWindow(event).getComponentForPath('Root Container.Production Line Selector')
	lcl = system.gui.getParentWindow(event).getComponentForPath('Root Container.LineContainer.LineName')
	
	
	#if there was a line name passed on the URL parameters
	if (len(lineName) > 0):
		#set the line name text in the Line Name Container
		lcl.text = systemLineName	
		#hide the line selector
		pls.visible = 0
		#make the line name text visible
		lcl.visible = 1
		#system.gui.messageBox(systemLinePath)
		pls.selectedLinePath = system.tag.getTagValue('[Client]SystemLinePath')
		pls.selectedIndex = 0 
	else:
		canSwitchLines = True
	
	
	#if at startup, we determined that we can switch lines
	if canSwitchLines:
		#make the line text invisible and make the line selector visible
		lcl.visible = 0	
		pls.visible = 1
		if 	systemLineName ==""		:
			pls.lineNameFilter = "" if lineFilter == None else lineFilter
			pls.selectedIndex = 0 
		else:	
			if systemLinePath!="":
				pls.selectedLinePath = systemLinePath
			else:
				pls.selectedIndex=0
			
	
	
#clear out any exsisting line path information
system.gui.getParentWindow(event).getComponentForPath('Root Container.Production Line Selector').selectedLinePath = ""
#fire the selectLine method and setup the appropriate line paths
system.util.invokeLater(selectLine, 50)	