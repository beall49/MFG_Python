def showMessage(userName):
	import system		
	return system.gui.confirm('You will now become the operator of this line?', 'Take Control of the Line?')

line=event.source.parent.lineName
userName=system.security.getUsername()

if showMessage(userName):
	system.tag.write("%s/Factors/Operator" % line, userName)
	system.tag.write("[Client]SystemLineName", line)
