from system.dataset import  toPyDataSet as toPyDs
import math
try:
	root = system.gui.getWindow('Main Windows/Operator_Control').getRootContainer()
	line=root.lineName
	txt=root.getComponent('txtWOProduced')
	tbl = toPyDs(root.getComponent('acProduced').tableData)
	uom = root.EaToCsConv
	totes=sum(sorted(row[0] for row in tbl))
	prd=int(math.floor(totes/uom))
	waste= int(math.floor(system.tag.getTagValue(line + "/Waste/wasteTotalCs")))
	txt.text =  str(prd-waste)
except:
	pass