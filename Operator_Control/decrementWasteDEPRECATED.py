  
def decrementWaste(userWaste):
	import system, app
	root = system.gui.getWindow('Main Windows/Operator_Control').getRootContainer()
	
	#get the lineName
	lnName = root.lineName
	
	#get the waste from the root container wasteShift1-3
	waste1 = root.wasteShift1; waste2 = root.wasteShift2; waste3 = root.wasteShift3

	#get the current shift from the root container    
	currentShift=  root.currentShift
	
	#easily lookup waste values (no switch/case statement)
	wasteDict ={1:waste1,2:waste2,3:waste3}	
	
	#how to get the prev shift
	shiftDict ={1:3,2:1,3:2}
	
	parms = [
				["RUNID", "INTEGER", system.tag.getTagValue(lnName + "/Util/RunId")],
				["WASTEVALUE", "INTEGER", (userWaste-(userWaste *2))],
				["LINE", "VARCHAR", lnName],
				["USERNAME","VARCHAR", str(system.security.getUsername())],
				["SHIFT", "INTEGER", currentShift]
			]
				
	#if the user waste is < total waste do stuff otherwise set everything to 0
	if userWaste < int(system.tag.getTagValue(lnName + "/Util/ManualWaste")):
		#get the waste for this shift prior to userWaste input
		currShiftWaste = wasteDict[currentShift]
		
		#if userWaste is greater than the current waste of the shift
		if userWaste >  currShiftWaste:		
			yes = system.gui.confirm("If you proceed you may remove waste from previous shifts. ","Subtract from Other Shifts")
		
			if yes:
				#SET CURRENT SHIFT WASTE = 0
				system.tag.write(lnName + "/Util/wasteShift" + str(currentShift), 0)
				app.Utils.runStoredProc(parms, "USP_INSERT_STAGE_WASTE")
				
				userWaste = abs(userWaste-currShiftWaste)        

				#loop through the 2 other shifts
				for x in range(1,3):                    
				
					#get last shift
					lastShift = shiftDict[currentShift]
					
					#get last shift waste
					lastWaste =  wasteDict[lastShift] 
				
					if userWaste > lastWaste:   
						#SET LAST SHIFT TAG TO 0
						print 'setting shift' + str(shiftDict[currentShift]) + '==0'
						system.tag.write(lnName + "/Util/wasteShift" + str(shiftDict[currentShift]), 0)
						
						userWaste = abs(userWaste-lastWaste)            
						currentShift = lastShift
					else:
						userWaste = abs(userWaste-lastWaste)              
						#SET LAST SHIFT TAG TO userWaste THEN EXIT
						print 'setting shift' + str(shiftDict[currentShift]) + ' ' + str(userWaste)
						system.tag.write(lnName + "/Util/wasteShift" + str(shiftDict[currentShift]), userWaste)
						x =3	
		#the waste is less than the current shift waste     
		else:
			userWaste = abs(currShiftWaste-userWaste)
			system.tag.write(lnName + "/Util/wasteShift" + str(currentShift), userWaste)						
	else:
		yes = system.gui.confirm("If you proceed you will remove waste from previous shifts. ","Subtract from Other Shifts")
		if yes:
			app.Utils.runStoredProc(parms, "USP_INSERT_STAGE_WASTE")
			print 'all your tags are belong to us'
			for x in range (1,4): system.tag.writeToTag(lnName + "/Util/wasteShift" + str(x), 0) 