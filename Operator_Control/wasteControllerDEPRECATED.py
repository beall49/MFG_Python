from system.dataset import  toPyDataSet as toPyDs
import datetime, traceback
me = system.gui.findWindow("Main Windows/Operator_Control")
if len(me)>0:
	root = system.gui.getWindow('Main Windows/Operator_Control').getRootContainer()
	cntShift = root.getComponent('ShiftTotals');
	tbl = toPyDs(cntShift.getComponent('WasteAnalysisController').tableData)
	
	try:
		cntShift.wasteCalc = sum(row[1] for row in tbl if row[1]>-1)
	except:
		 self.makeLog("\n Analysis Controller didn't write waste %s\n %s" %(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") , str(traceback.format_exc())))
