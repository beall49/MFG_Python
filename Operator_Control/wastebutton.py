#actionPerformed
#waste button
e = event.source.parent.getComponent
bool = False

#get line name from line path
lnName = event.source.parent.parent.lineName
prodCount = event.source.parent.parent.getComponent('CasesRemaining').ProductionCount

#get new waste add
addedWaste = e('spWaste').intValue
totalWaste = system.tag.getTagValue(lnName + "/Waste/wasteTotal")


#if waste is negative
if addedWaste < 0:
    #if the neg waste is less than current total waste
    if (totalWaste - addedWaste*-1) < 0:
        system.gui.warningBox("You cannot remove more waste than what has been added.", "Negative Waste Error!")
    else: 
        #the prod count is greater than added waste (no neg waste)
        if prodCount > (addedWaste*-1):
            app.Waste.aggregateWaste(lnName, addedWaste)
            bool = True
        else:
            system.gui.warningBox("You cannot add more waste than has been produced.", "Waste greater than production!")            
else:
    #the prod count is greater than added waste (no neg waste)
    if prodCount > (totalWaste + addedWaste):
        app.Waste.aggregateWaste(lnName, addedWaste)
        bool = True
    else:
        system.gui.warningBox("You cannot add more waste than has been produced.", "Waste greater than production!")

#if everything fired correctly    
if bool:
    e('spWaste').intValue = 0
    e('btnWaste').componentEnabled = False
