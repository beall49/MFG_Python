"""
	not used in production
	lets user pass username and password and bypass login window 
	username and password must be base64encoded
	http://g3mesacc:7088/main/system/launch/client/G3Enterprises.jnlp?User=USER:PWORD
"""	
def silentLogin(user):
	import base64
	try:
		userName, passPhrase = user.split(":")	
		if system.security.switchUser(base64.b64decode(userName), base64.b64decode(passPhrase)):
			return True
		return False
	except:
		return False
		
"""
	this lives in shared script folder, it sets the client tags
"""		
def setClientVars():
	import system, app

	lineName = system.tag.read("[Client]LineName").value
	defaultScreen = system.tag.getTagValue("[Client]DefaultScreen")
	
	lineLookup = system.tag.getTagValue("[Client]lineLookup")	
	system.tag.write("[Client]lineFilter",str(shared.login.getLoginLines(lineLookup)))
	
	if len(lineName) > 0:
			#system.gui.messageBox(lineName)
			#if there is a line name, stick the gui to the passed in line
			#this is used on the operator screens to determine whether or not to show the line selector drop down
			system.tag.write("[Client]canSwitchLines",False)
			#get the system line path from the DB for use on later screens	
			results = system.db.runPrepQuery("select MesLinePath from G3_LineLookup where MesLineName = ?", [lineName])
			system.tag.write("[Client]SystemLineName", lineName)
			#if we got a result, save it. 
			if len(results) > 0:
				productionLinePath = results[0][0] 
				system.tag.write("[Client]SystemLinePath",productionLinePath)
		

def getLoginLines(user):
	import system, app
	
	#FIND OUT WHAT LINES THAT PROFILE IS ASSIGNED TOO
	tbl = app.Utils.runStoredProc([["USERNAME", "VARCHAR", user]], "[MES_RUNTIME].[DBO].[USP_GET_LINE_LOGIN]")
	
	#CREATE SB AND APPEND TO IT
	return ', '.join(row[0] for row in tbl)	

"""
	this fires when a client is launched (it's found under the client event scripts)
	http://g3mesprd:8088/main/system/launchfs/client/G3Enterprises.jnlp?LineName=BK1&DefaultScreen=Scoreboard&X=0&Y=0&H=1920&W=1080
	This pull any params out of the URL and trys to decipher if a screen has been passed in, and what line
	
"""

from system.tag import getTagValue as GET
from system.nav import swapTo as SWAP, openWindow as OPEN

#get the line passed in if any
defaultScreen = 'Main Windows/' + ('Plant Overview' if GET('[Client]DefaultScreen') == 'Plant' else GET('[Client]DefaultScreen'))
	
#note, this depends on an 'auto-login' setup for the scoreboard user with a password of test
if (defaultScreen in ["Main Windows/Scoreboard", "Main Windows/Plant Overview"]):	
	from javax.swing import SwingUtilities,JFrame
	from java.awt import GraphicsEnvironment,Rectangle
	
	#Get the screen dimensions from the Client URL
	x,y,h,w=GET('[Client]X'), GET('[Client]Y'), GET('[Client]H'), GET('[Client]W')
	
	#handle setting the common client variables (e.g. line name)
	shared.login.setClientVars()
	
	#switch to the scoreboard
	window = SWAP(defaultScreen)	
	frame = SwingUtilities.getAncestorOfClass(JFrame, window)
	
	#set the dimensions of the scoreboard
	frame.setBounds(x,y,h,w)
	
	#remove the annoying 4px frame border
	window2 = OPEN(defaultScreen)
	window2.parent.parent.parent.setBorder(None)
	
elif (defaultScreen == "Main Windows/Simulator"):
	SWAP("LineSimulator/Sim_BK2")	
else:
	SWAP("Main Windows/Login Window")	