import json, cgi, sqlite3 as sqlite
from urllib import urlopen;
from datetime import datetime, timedelta    



class GetStats():
    def __init__(self):
        self.HOME = "\\\\MODAPP254\\c$\\inetpub\\wwwroot\\Services"
        self.DB_LOCATION = "logEntries.db"
        print self.DB_LOCATION
        self.TBL_NAME = "TBL_LOG_ENTRIES"
        
        self.SELECT = """
                        SELECT 
                            DATETIME(DATEADD, 'LOCALTIME') AS ENTRYDATE, 
                            USER_NAME, 
                            APPLICATION_NAME, 
                            SCREEN_NAME 
                        FROM 
                            {0} 
                        WHERE 
                            UPPER(USER_NAME) NOT IN ('RBEALL','DSADOWSK','REPORT_USER') 
                        ORDER BY 
                            DATEADD DESC
                    """.format(self.TBL_NAME)
                                      
        self.DELETE ="DELETE FROM {0} WHERE UPPER(USER_NAME) IN ('RBEALL','DSADOWSK','REPORT_USER') AND SCREEN_NAME = 'Operator_Control' ".format(self.TBL_NAME)
        self.sqlSelect()
    
    def sqlSelect(self):
        with sqlite.connect(self.DB_LOCATION) as con:
            cur= con.cursor()
            cur.execute(self.SELECT)
            tbl=cur.fetchall()   
            
            tbl= json.dumps({"Num of Entries": len(tbl),
                                "Entries" : [{"Entry Date":row[0],"User":row[01],"App": row[02],"Screen": row[03]} for row in tbl]}, sort_keys = False, indent = 4)
            
        print tbl
                
                
                          
    def sqlDelete(self):
        with sqlite.connect(self.DB_LOCATION)  as con:
            cur= con.cursor()
            cur.execute(self.DELETE)


print "Status: 200 OK;Content-Type: text/plain;charset=utf-8\n"
c=GetStats()

            
