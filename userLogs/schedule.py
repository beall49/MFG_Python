#MUST PRINT THIS LINE FOR HEADER REASONS
print "Status: 200 OK;Content-Type: text/plain;charset=utf-8\n"
def getData(lineName="None"):
	import pyodbc, json, base64    
	cur = pyodbc.connect(base64.b64decode('RFJJVkVSPXtTUUwgU2VydmVyfTtTRVJWRVI9bW9kZGJzMDY2O0RBVEFCQVNFPW1lc19ydW50aW1lO1VJRD1pZ25pdGlvbjtQV0Q9RjNkaWFCMW4=')).cursor()	
	qry = "EXEC [MES_RUNTIME].[DBO].[USP_GET_GEMBA_DATA] '{0}'".format(lineName)
	cursor =cur.execute(qry)
	columns = [str(column[0]) for column in cursor.description]
	tbl= json.dumps({"Line: " + str(lineName) + " Schedule" : [{'SHIFT' :row[0],'QTY':row[01]} for row in cursor.fetchall()]},  indent = 4)
	cur.close()
	return tbl



#http://modapp254:9090/Services/schedule.py?lineName=BK1
import cgi;
form = cgi.FieldStorage()
print getData(form.getvalue("lineName"))