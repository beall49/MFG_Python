﻿using Mes.Cache;
using Mes.Payloads;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace Mes.Models
{
    public enum Workspaces : long
    {
        SKUS = 53,
        Specifications = 69,
        Drawings = 59,
        Tooling = 54,
        MachineSettings = 77
    }


    public class JobJacketModel
    {
        private Dictionary<string, JobJacket> jobDict = new Dictionary<string, JobJacket>();
        private static DateTime lastLogin;
        private static string sessionid;

        private static JobJacketModel instance;
        private Settings sysdef;

        private JobJacketModel()
        {
            lastLogin = DateTime.Now.Subtract(TimeSpan.FromMinutes(60));
            sysdef = Settings.Instance;
        }

        public static JobJacketModel Instance
        {
            get
            {

                if (instance == null)
                {
                    instance = new JobJacketModel();
                }

                if (DateTime.Now.Subtract(lastLogin) >= TimeSpan.FromMinutes(60))
                {
                    //60 minutes have passed since last login
                    sessionid = instance.GetSession();

                }
                lastLogin = DateTime.Now;
                return instance;
            }
        }

        public async Task<JobJacket> GetJobJacketForSKU(string sku)
        {
            JobJacket jj = null;
            if (jobDict.ContainsKey(sku))
                jj = jobDict[sku];

            if (jj == null || DateTime.Now.Subtract(jj.LastUpdated) >= TimeSpan.FromMinutes(60))
            {
                jj = new JobJacket();
                jj.LastUpdated = DateTime.Now;
                jj.Display = new List<Section>();
                jj.Attachments = new List<Attachment>();
                long dms = await GetLatestDMS(sku);
                List<long> childList = GetBOMNodes(dms);

                await PopulateJobHacket(jj, dms, childList);

                jj.Display = jj.Display.OrderBy(r => r.SortOrder).ToList();

                jobDict[sku] = jj;

            }

            return jj;
        }

        private async Task PopulateJobHacket(JobJacket jj, long parentdms, List<long> childList)
        {
            ItemDetail parent = CacheEngine.Instance.GetItemDetailforDMS(parentdms);
            Section sect = new Section();
            sect.Heading = "Parent SKU";
            sect.Elements = new List<Element>();
            sect.Elements.Add(new Element { Name = "SKU", Value = parent.fields["JDE_LONG_ITEM_NUMBER"] });
            sect.Elements.Add(new Element { Name = "DESCRIPTION", Value = parent.fields["DESC_1"] });
            sect.SortOrder = 0;
            jj.Display.Add(sect);
            foreach (long childdms in childList)
            {
                ItemDetail child = CacheEngine.Instance.GetItemDetailforDMS(childdms);

                if (child == null || child.jobjacket == false)
                    continue;

                switch (child.workspaceId)
                {
                    case 53:
                        {
                            Section sect2 = new Section();
                            sect2.Heading = "Bill of Material";
                            sect2.Elements = new List<Element>();
                            if (child.fields.ContainsKey("JDE_LONG_ITEM_NUMBER"))
                            {
                                sect2.Elements.Add(new Element { Name = "SKU", Value = child.fields["JDE_LONG_ITEM_NUMBER"] });
                            }
                            if (child.fields.ContainsKey("DESC_1"))
                            {
                                sect2.Elements.Add(new Element { Name = "DESCRIPTION", Value = child.fields["DESC_1"] });
                            }
                            sect2.SortOrder = 1;
                            jj.Display.Add(sect2);
                            break;
                        }
                    case 69:
                        {
                            string spectype = null;
                            string specsubtype = null;
                            Section sect2 = new Section();
                            sect2.Heading = "Specification Details";
                            sect2.Elements = new List<Element>();
                            if (child.picklistFields["SPEC_TYPE"] != null)
                            {
                                spectype = (child.picklistFields["SPEC_TYPE"])[0].displayName;
                                sect2.Elements.Add(new Element { Name = "SPEC_TYPE", Value = spectype });
                            }
                            if (child.picklistFields["SPEC_SUB_TYPE"] != null)
                            {
                                specsubtype = (child.picklistFields["SPEC_SUB_TYPE"])[0].displayName;
                                sect2.Elements.Add(new Element { Name = "SPEC_SUB_TYPE", Value = specsubtype });
                            }
                            if (child.fields.ContainsKey("DESCRIPTION"))
                            {
                                sect2.Elements.Add(new Element { Name = "DESCRIPTION", Value = child.fields["DESCRIPTION"] });
                            }
                            sect2.SortOrder = 2;
                            jj.Display.Add(sect2);
                            List<Attachment> tatt = await GetFiles2(69, child.id, string.Format("{0} {1}", spectype, specsubtype));
                            jj.Attachments.AddRange(tatt);
                            break;
                        }
                    case 59:
                        {
                            List<Attachment> tatt = await GetFiles2(59, child.id, "Drawing");
                            jj.Attachments.AddRange(tatt);
                            break;
                        }
                    case 54:
                        {
                            List<Attachment> tatt = await GetFiles2(54, child.id, "Tooling List");
                            jj.Attachments.AddRange(tatt);
                            break;
                        }
                    case 78:
                        {
                            Section sect2 = new Section();
                            sect2.Heading = "Quality Tests";
                            sect2.Elements = new List<Element>();
                            if (child.fields.ContainsKey("DESCRIPTION"))
                            {
                                sect2.Elements.Add(new Element { Name = "DESCRIPTION", Value = child.fields["DESCRIPTION"] });
                            }
                            sect2.SortOrder = 3;
                            jj.Display.Add(sect2);
                            List<Attachment> tatt = await GetFiles2(78, child.id, "Quality Tests");
                            jj.Attachments.AddRange(tatt);
                            break;
                        }

                }

            }
        }

        private List<long> GetBOMNodes(long dmsid)
        {
            List<long> childList = new List<long>();
            List<Task> tasks = new List<Task>();

            HttpClientHandler handler = new HttpClientHandler()
            {
                CookieContainer = new CookieContainer()
            };

            handler.CookieContainer.Add(new Uri(sysdef.URL), new Cookie("JSESSIONID", sessionid));

            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(sysdef.URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


                string address = "api/rest/v1/workspaces/53/items/" + dmsid.ToString() + "/boms?depth=1&revisionBias=release";


                // New code:
                HttpResponseMessage response = client.GetAsync(address).Result;
                if (response.IsSuccessStatusCode)
                {
                    string content = response.Content.ReadAsStringAsync().Result;
                   // dynamic elem = JsonConvert.DeserializeObject(content);

                    var elem = JObject.Parse(content);

                    JArray bomArr = (JArray)elem["list"]["data"];

                    Task parentTask = Task.Run(() => CacheEngine.Instance.LoadItemDetail(53, dmsid, sessionid));


                    foreach (JObject jobj in bomArr)
                    {
                        JObject bobj = (JObject)jobj.GetValue("bom-item");
                        long wsId = bobj.Value<long>("workspaceID");
                        long dmsId = bobj.Value<long>("dmsID");
                        childList.Add(dmsId);
                        Task temp = Task.Run(() => CacheEngine.Instance.LoadItemDetail(wsId, dmsId, sessionid));

                        tasks.Add(temp);

                    }

                    parentTask.Wait();
                }
            }


            Task.WaitAll(tasks.ToArray());

            return childList;

        }

        private void GetChildNodes(long dmsid, ref ConcurrentDictionary<long, string> dict)
        {
            ConcurrentDictionary<long, string> cDict = new ConcurrentDictionary<long, string>();
            List<Task> tasks = new List<Task>();
            JobJacket jj = new JobJacket();
            List<Section> bitems = new List<Section>();
            List<Attachment> attch = new List<Attachment>();
            HttpClientHandler handler = new HttpClientHandler()
            {
                CookieContainer = new CookieContainer()
            };

            handler.CookieContainer.Add(new Uri(sysdef.URL), new Cookie("JSESSIONID", sessionid));

            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(sysdef.URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


                string address = "api/rest/v1/workspaces/53/items/" + dmsid.ToString() + "/boms?depth=1&revisionBias=release";


                // New code:
                HttpResponseMessage response = client.GetAsync(address).Result;
                if (response.IsSuccessStatusCode)
                {
                    string content = response.Content.ReadAsStringAsync().Result;
                    dynamic elem = JsonConvert.DeserializeObject(content);

                    foreach (JObject jobj in elem.list.data)
                    {
                        JObject bobj = (JObject)jobj.GetValue("bom-item");
                        long wsId = bobj.Value<long>("workspaceID");
                        long dmsId = bobj.Value<long>("dmsID");

                        Task temp = Task.Run(() => GetItemJson(wsId, dmsId, cDict));

                        tasks.Add(temp);

                    }
                }
            }

            Task.WaitAll(tasks.ToArray());

            dict = cDict;

        }

        private async Task<long> GetLatestDMS(string sku)
        {
            PropertyDefinition pd = new PropertyDefinition();
            pd.type = "ITEM_FIELD";
            pd.id = "JDE_LONG_ITEM_NUMBER";
            FilterCondition fc = new FilterCondition();
            fc.propertyDefinition = pd;
            fc.operator_ = "EQUALS";
            fc.value = sku;
            ItemFilter srch = new ItemFilter();
            srch.sort = null;
            srch.page = null;
            srch.pageSize = null;
            srch.conditions = new FilterCondition[] { fc };

            HttpClientHandler handler = new HttpClientHandler()
                    {
                        CookieContainer = new CookieContainer()
                    };

            handler.CookieContainer.Add(new Uri(sysdef.URL), new Cookie("JSESSIONID", sessionid));
            using (var client = new HttpClient(handler))
            {

                client.BaseAddress = new Uri(sysdef.URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


                // New code:
                HttpResponseMessage response = await client.PostAsJsonAsync("api/v2/workspaces/53/items/query", srch);
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();
                    dynamic elem = JsonConvert.DeserializeObject(content);

                    foreach (var item in elem.elements)
                    {
                        if (item.isLatestVersion == true)
                            return item.id;

                    }
                }
            }

            return 0;
        }

        private async Task ExtractMetaData(JobJacket jj, ConcurrentDictionary<long, string> cDict)
        {
            foreach (KeyValuePair<long, string> kv in cDict)
            {

                var json = JObject.Parse(kv.Value);
                long wsid = (long)json["item"]["details"]["workspaceID"];


                switch (wsid)
                {
                    case 69:
                        Section sect = new Section();
                        JArray ja = (JArray)json["item"]["metaFields"]["entry"];
                        string spec_type = GetFieldValue(ja, "SPEC_TYPE");
                        string spec_sub_type = GetFieldValue(ja, "SPEC_SUB_TYPE");
                        string desc = GetFieldValue(ja, "DESCRIPTION");
                        sect.Heading = spec_type;
                        sect.Elements = new List<Element>();
                        sect.Elements.Add(new Element { Name = "SPEC_TYPE", Value = spec_type });
                        sect.Elements.Add(new Element { Name = "SPEC_SUB_TYPE", Value = spec_sub_type });
                        sect.Elements.Add(new Element { Name = "DESCRIPTION", Value = desc });
                        jj.Display.Add(sect);
                        break;
                    case 59:
                        long dms = (long)json["item"]["id"];
                        List<Attachment> tatt = await GetFiles(59, dms, json);
                        jj.Attachments = tatt;
                        break;

                }



                //     IEnumerable<JToken> jtk = jo.Values()


            }
        }

        private string GetFieldValue(JArray ja, string fieldName)
        {
            var jitems = from x in ja where (x["key"].ToString() == fieldName) select x;
            //        var jitem = ja.Single(x => x["key"].ToString() == fieldName);

            if (jitems.Count() <= 0)
                return null;

            var jitem = jitems.First();
            string value = null;
            foreach (JProperty tobj in jitem)
            {

                if (tobj.HasValues && tobj.Name == "fieldData")
                {
                    value = tobj.Value["value"].ToString();
                    return value;
                }

            }

            return null;
        }

        private async Task<JobJacket> GetBOM(long dmsid)
        {

            JobJacket jj = new JobJacket();
            List<Section> bitems = new List<Section>();
            List<Attachment> attch = new List<Attachment>();
            HttpClientHandler handler = new HttpClientHandler()
            {
                CookieContainer = new CookieContainer()
            };

            handler.CookieContainer.Add(new Uri(sysdef.URL), new Cookie("JSESSIONID", sessionid));

            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(sysdef.URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


                string address = "api/rest/v1/workspaces/53/items/" + dmsid.ToString() + "/boms";


                // New code:
                HttpResponseMessage response = await client.GetAsync(address);
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();

                    dynamic elem = JsonConvert.DeserializeObject(content);

                    foreach (JObject jobj in elem.list.data)
                    {
                        Section hd = new Section();
                        hd.Elements = new List<Element>();
                        Element el = new Element();
                        el.Name = "descriptor";
                        JObject bobj = (JObject)jobj.GetValue("bom-item");
                        el.Value = bobj.Value<string>("descriptor");

                        //           List<Attachment> tatt = await GetFiles(bobj.Value<long>("workspaceID"), bobj.Value<long>("dmsID"));

                        hd.Elements.Add(el);
                        bitems.Add(hd);
                        //          attch.AddRange(tatt);
                    }
                }
            }

            jj.Display = bitems;
            jj.Attachments = attch;
            return jj;
        }

        public async Task<string> GetItemDetails(long wsid, long dmsid, params string[] attributes)
        {

            List<Attachment> bitems = new List<Attachment>();
            HttpClientHandler handler = new HttpClientHandler()
            {
                CookieContainer = new CookieContainer()
            };

            handler.CookieContainer.Add(new Uri(sysdef.URL), new Cookie("JSESSIONID", sessionid));

            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(sysdef.URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


                string address = "api/rest/v1/workspaces/" + wsid.ToString() + "/items/" + dmsid.ToString();


                // New code:
                HttpResponseMessage response = await client.GetAsync(address);
                if (response.IsSuccessStatusCode)
                {

                    string content = await response.Content.ReadAsStringAsync();
                    var json = JObject.Parse(content);
                    JArray ja = (JArray)json["item"]["metaFields"]["entry"];


                    //     IEnumerable<JToken> jtk = jo.Values()

                    var jitem = ja.Single(x => x["key"].ToString() == "DESC_1");

                    //            var jh = jitem[1];

                    string value = null;
                    foreach (JProperty tobj in jitem)
                    {

                        if (tobj.HasValues && tobj.Name == "fieldData")
                        {
                            value = tobj.Value["value"].ToString();
                            break;
                        }


                    }

                    //        var item = jo.Values("key").Where(x => x.ToString() == "DESC_1"); 
                    //          jtk.Single(x => x.Value<string>("key") == "DESC_1");
                    //          var fielddata = jitem["fieldData"];
                    //    var value = jitem["fieldData"]["value"].ToString();
                    return value;
                }
            }

            return null;


        }

        private async Task GetItemJson(long wsid, long dmsid, ConcurrentDictionary<long, string> dict)
        {

            List<Attachment> bitems = new List<Attachment>();
            HttpClientHandler handler = new HttpClientHandler()
            {
                CookieContainer = new CookieContainer()
            };

            handler.CookieContainer.Add(new Uri(sysdef.URL), new Cookie("JSESSIONID", sessionid));

            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(sysdef.URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


                string address = "api/rest/v1/workspaces/" + wsid.ToString() + "/items/" + dmsid.ToString();


                // New code:
                HttpResponseMessage response = await client.GetAsync(address);
                if (response.IsSuccessStatusCode)
                {

                    string content = await response.Content.ReadAsStringAsync();
                    dict[dmsid] = content;
                }
            }



        }

        private async Task<List<Attachment>> GetFiles2(long wsid, long dmsid, string displayName)
        {

            List<Attachment> bitems = new List<Attachment>();

            PagedCollection<File> fileCollection = await CacheEngine.Instance.GetFilesForDMS(wsid, dmsid, sessionid);

            foreach (File f in fileCollection.elements)
            {
                Attachment attch = new Attachment();
                attch.DmsID = dmsid.ToString();
                attch.Filename = f.fileName;
                attch.Type = f.title;
                attch.DisplayName = displayName;
                attch.FileId = f.id.ToString();
                attch.DocumentFormat = f.url;
                attch.WorkSpace = f.workspaceId.ToString();
                bitems.Add(attch);

            }


            return bitems;
        }
        private async Task<List<Attachment>> GetFiles(long wsid, long dmsid, string displayName)
        {

            List<Attachment> bitems = new List<Attachment>();
            HttpClientHandler handler = new HttpClientHandler()
            {
                CookieContainer = new CookieContainer()
            };

            handler.CookieContainer.Add(new Uri(sysdef.URL), new Cookie("JSESSIONID", sessionid));

            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(sysdef.URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string address = "api/rest/workspaces/" + wsid.ToString() + "/items/" + dmsid.ToString() + "/files";


                // New code:
                HttpResponseMessage response = await client.GetAsync(address);
                if (response.IsSuccessStatusCode)
                {

                    string content = await response.Content.ReadAsStringAsync();
                    dynamic elem = JsonConvert.DeserializeObject(content);

                    if (((JObject)elem).Count != 0)
                    {


                        foreach (JObject jobj in elem.elements)
                        {
                            Attachment attch = new Attachment();
                            attch.DmsID = dmsid.ToString();
                            attch.Filename = jobj.Value<string>("fileName");
                            attch.Type = displayName;
                            attch.DisplayName = displayName;
                            bitems.Add(attch);
                        }
                    }
                }
            }

            return bitems;
        }
        private async Task<List<Attachment>> GetFiles(long wsid, long dmsid, JObject itemObj)
        {

            List<Attachment> bitems = new List<Attachment>();
            HttpClientHandler handler = new HttpClientHandler()
            {
                CookieContainer = new CookieContainer()
            };

            handler.CookieContainer.Add(new Uri(sysdef.URL), new Cookie("JSESSIONID", sessionid));

            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(sysdef.URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string address = "api/rest/workspaces/" + wsid.ToString() + "/items/" + dmsid.ToString() + "/files";


                // New code:
                HttpResponseMessage response = await client.GetAsync(address);
                if (response.IsSuccessStatusCode)
                {

                    string content = await response.Content.ReadAsStringAsync();
                    dynamic elem = JsonConvert.DeserializeObject(content);

                    if (((JObject)elem).Count != 0)
                    {
                        JArray ja = (JArray)itemObj["item"]["metaFields"]["entry"];
                        string desc = GetFieldValue(ja, "DESCRIPTION");


                        foreach (JObject jobj in elem.elements)
                        {
                            Attachment attch = new Attachment();
                            attch.DmsID = dmsid.ToString();
                            attch.Filename = jobj.Value<string>("fileName");
                            attch.DisplayName = desc;
                            bitems.Add(attch);
                        }
                    }
                }
            }

            return bitems;
        }

        private string GetSession()
        {
            using (var client = new HttpClient())
            {
                var pbody = new { userID = sysdef.UserName, password = sysdef.Password };


                client.BaseAddress = new Uri(sysdef.URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


                // New code:
                HttpResponseMessage response = client.PostAsJsonAsync("rest/auth/1/login.json", pbody).Result;
                if (response.IsSuccessStatusCode)
                {
                    string content = response.Content.ReadAsStringAsync().Result;
                    dynamic elem = JsonConvert.DeserializeObject(content);
                    return elem.sessionid;
                }

            }

            return null;
        }
    }
}