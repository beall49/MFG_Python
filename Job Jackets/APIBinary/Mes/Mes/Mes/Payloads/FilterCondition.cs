﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mes.Payloads
{
    [DataContract]
    public class FilterCondition
    {
        [DataMember]
        public PropertyDefinition propertyDefinition;

        [DataMember(Name = "operator")]
        public string operator_;

        [DataMember]
        public string value;
    }
}