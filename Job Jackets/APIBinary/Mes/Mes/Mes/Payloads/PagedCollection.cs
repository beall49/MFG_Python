﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mes.Payloads
{
    [DataContract]
    public class PagedCollection<T>
    {
        [DataMember]
        public Page page { set; get; }

        [DataMember]
        public IEnumerable<T> elements { set; get; }
    }
}