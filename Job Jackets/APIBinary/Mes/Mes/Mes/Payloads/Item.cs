﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mes.Payloads
{
    [DataContract]
    public class Item
    {
        public bool jobjacket = false;
        [DataMember]
        public long id { set; get; }

        [DataMember]
        public int? rootId { set; get; }

        [DataMember]
        public int? version { set; get; }

        [DataMember]
        public string revision { set; get; }

        [DataMember]
        public long? workspaceId { set; get; }

        [DataMember]
        public string url { set; get; }

        [DataMember]
        public string itemDescriptor { set; get; }

        [DataMember]
        public bool? deleted { set; get; }

        [DataMember]
        public bool? isWorkingVersion { set; get; }

        [DataMember]
        public bool? isLatestVersion { set; get; }

    }

}