﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mes.Payloads
{

    public class Section
    {
        public string Heading;
        public List<Element> Elements;
        [IgnoreDataMember] 
        public int SortOrder;
    }

    public class Element
    {
        public string Name;
        public string Value;
    }

    public class Attachment
    {
        public string Type;
        public string DisplayName;
        public string DocumentFormat;
        public string Filename;
        public string FileId;
        public string DmsID;
        public string WorkSpace;
        public string PrintViewId;
    }

    [DataContract(Namespace="")]
    public class JobJacket
    {
        public DateTime LastUpdated;
        
        [DataMember]
        public List<Section> Display;

        [DataMember]
        public List<Attachment> Attachments;

    }
}