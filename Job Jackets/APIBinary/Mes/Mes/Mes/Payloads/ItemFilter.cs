﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mes.Payloads
{
    [DataContract]
    public class ItemFilter
    {
        [DataMember]
        public FilterSort[] sort;

        [DataMember]
        public FilterCondition[] conditions;

        [DataMember]
        public int? page;

        [DataMember]
        public int? pageSize;
    }
}