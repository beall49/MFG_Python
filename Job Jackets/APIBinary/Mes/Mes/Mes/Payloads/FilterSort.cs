﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mes.Payloads
{
    [DataContract]
    public class FilterSort
    {
        [DataMember]
        public PropertyDefinition propertyDefinition;

        [DataMember]
        public bool? sortAscending;
    }
}