﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mes.Payloads
{
    [DataContract]
    public class PicklistValue
    {
        [DataMember]
        public long? id { set; get; }

        [DataMember]
        public string displayName { set; get; }

        [DataMember]
        public string itemUrl { set; get; }
    }
}