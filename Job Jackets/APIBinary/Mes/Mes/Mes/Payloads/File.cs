﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mes.Payloads
{
    [DataContract]
    public class File
    {
        [DataMember]
        public long? id;

        [DataMember]
        public string url;

        [DataMember]
        public int? itemId;

        [DataMember]
        public int? workspaceId;

        [DataMember]
        public string fileName;

        [DataMember]
        public string title;

        [DataMember]
        public string description;

        [DataMember]
        public int? size;

        [DataMember]
        public int? version;
    }
}