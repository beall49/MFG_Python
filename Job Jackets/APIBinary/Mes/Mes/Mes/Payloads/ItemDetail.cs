﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mes.Payloads
{
    [DataContract]
    public class ItemDetail : Item
    {
        public DateTime LastUpdated;
        [DataMember]
        public Dictionary<string, string> fields { set; get; }

        [DataMember]
        public Dictionary<string, PicklistValue[]> picklistFields { set; get; }


    }
}