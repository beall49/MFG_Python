﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mes.Payloads
{
    [DataContract]
    public class Page
    {
        [DataMember]
        public long? index { set; get; }

        [DataMember]
        public long? size { set; get; }

        [DataMember]
        public string nextUrl { set; get; }

        [DataMember]
        public string prevUrl { set; get; }
    }
}