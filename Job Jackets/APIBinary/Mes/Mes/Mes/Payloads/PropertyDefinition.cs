﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mes.Payloads
{
    [DataContract]
    public class PropertyDefinition
    {
        [DataMember]
        public string type;

        [DataMember]
        public string id;
    }
}