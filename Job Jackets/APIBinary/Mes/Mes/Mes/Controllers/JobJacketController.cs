﻿using JsonPath;
using Mes.Models;
using Mes.Payloads;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using System.Xml.Serialization;

namespace Mes.Controllers
{
    public class JobJacketController : ApiController
    {
        // GET: api/JobJacket
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/JobJacket/5
        public async Task<JobJacket> Get(string id)
        {
            /**
            string sessionid = await GetSession();
            long dms = await GetLatestDMS(id, sessionid);
            JobJacket jb = await GetBOM(dms, sessionid);
            Dictionary<string, object> dObj = await GetItemDetails(53, dms, sessionid);
            return JsonConvert.SerializeObject(jb);
             * **/

    //        List<Task> tasks = new List<Task>();

    //        Task.WaitAll(tasks.ToArray());

            JobJacketModel jModel = JobJacketModel.Instance;
            
            JobJacket jj = await jModel.GetJobJacketForSKU(id);
            return jj;

            //        string sessionid = await GetSession();
            //        return sessionid;
            //       return "";

        }

        // POST: api/JobJacket
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/JobJacket/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/JobJacket/5
        public void Delete(int id)
        {
        }

        private async Task<string> GetSession()
        {
            using (var client = new HttpClient())
            {
                var pbody = new { userID = "dharmar", password = "Autodesk#1" };


                client.BaseAddress = new Uri("https://g3enterprises.autodeskplm360.net/");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


                // New code:
                HttpResponseMessage response = await client.PostAsJsonAsync("rest/auth/1/login.json", pbody);
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();
                    dynamic elem = JsonConvert.DeserializeObject(content);
                    return elem.sessionid;
                }

            }

            return null;
        }


    }
}
