﻿using Mes.Payloads;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace Mes.Cache
{
    public class CacheEngine
    {
        private ConcurrentDictionary<long, ItemDetail> itemCache = new ConcurrentDictionary<long, ItemDetail>();
        private ConcurrentDictionary<long, PagedCollection<File>> fileCache = new ConcurrentDictionary<long, PagedCollection<File>>();
        private static CacheEngine instance;
        private CacheEngine()
        {

        }

        public static CacheEngine Instance
        {
            get
            {

                if (instance == null)
                {
                    instance = new CacheEngine();
                }


                return instance;
            }
        }
        public ItemDetail GetItemDetailforDMS(long dmsid)
        {
            return itemCache[dmsid];
        }

        public async Task LoadItemDetail(long wsid, long dmsid, string sessionid)
        {
            if (itemCache.ContainsKey(dmsid))
            {
                ItemDetail temp = itemCache[dmsid];
                if (DateTime.Now.Subtract(temp.LastUpdated) <= TimeSpan.FromMinutes(120))
                    return;

            }


            HttpClientHandler handler = new HttpClientHandler()
            {
                CookieContainer = new CookieContainer()
            };

            handler.CookieContainer.Add(new Uri(Settings.Instance.URL), new Cookie("JSESSIONID", sessionid));
            using (var client = new HttpClient(handler))
            {

                client.BaseAddress = new Uri(Settings.Instance.URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // New code:
                HttpResponseMessage response = await client.GetAsync(string.Format("/api/v2/workspaces/{0}/items/{1}", wsid, dmsid));
                if (response.IsSuccessStatusCode)
                {
                    ItemDetail iDetail = Util.JsonDeserialize<ItemDetail>(response);
                    iDetail.LastUpdated = DateTime.Now;
                    itemCache[iDetail.id] = iDetail;

                    if (iDetail.picklistFields.ContainsKey("JOB_JACKET") && iDetail.picklistFields["JOB_JACKET"] != null)
                    {
                        if ((iDetail.picklistFields["JOB_JACKET"])[0].id == 1)
                            iDetail.jobjacket = true;
                    }
                    else if (iDetail.picklistFields.ContainsKey("ON_JOB_JACKET") && iDetail.picklistFields["ON_JOB_JACKET"] != null)
                    {
                        if ((iDetail.picklistFields["ON_JOB_JACKET"])[0].id == 1)
                            iDetail.jobjacket = true;
                    }
                    else if (iDetail.fields.ContainsKey("JOB_JACKET"))
                    {
                        if (iDetail.fields["JOB_JACKET"].Equals("true"))
                            iDetail.jobjacket = true;                     
                    }

                }

            }

        }

        public async Task<PagedCollection<File>> GetFilesForDMS(long wsid, long dmsid, string sessionid)
        {
   //         if (fileCache.ContainsKey(dmsid))
   //             return fileCache[dmsid];

            HttpClientHandler handler = new HttpClientHandler()
            {
                CookieContainer = new CookieContainer()
            };

            handler.CookieContainer.Add(new Uri(Settings.Instance.URL), new Cookie("JSESSIONID", sessionid));
            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(Settings.Instance.URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync(string.Format("/api/v2/workspaces/{0}/items/{1}/files", wsid, dmsid));
                if (response.IsSuccessStatusCode)
                {
                    PagedCollection<File> fDetail = Util.JsonDeserialize<PagedCollection<File>>(response);
                    fileCache[dmsid] = fDetail;
                    return fDetail;
                }


            }

            return null;
        }



    }
}