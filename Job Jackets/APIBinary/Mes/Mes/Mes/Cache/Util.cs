﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Json;
using System.Security;
using System.Text;
using System.Threading.Tasks;


namespace Mes.Cache
{
    internal class Util
    {
        public static readonly string JSON_CONTENT_TYPE = "application/json";
        public static readonly string XML_CONTENT_TYPE = "application/xml";
        public static readonly string MIXED_CONTENT_TYPE = "mixed";

        public static void DoAction(Action a)
        {
            try
            {
                a();
            }
            catch (Exception ex)
            {

            }
        }

        internal static SecureString ToSecureString(string str)
        {
            SecureString retVal = new SecureString();
            if (!string.IsNullOrEmpty(str))
            {
                char[] charArray = str.ToCharArray();
                foreach (char aChar in charArray)
                {
                    retVal.AppendChar(aChar);
                }
            }

            return retVal;
        }

        internal static string FromSecureString(SecureString secStr)
        {
            int stringSize = secStr.Length * sizeof(char);
            byte[] plain = new byte[stringSize];
            IntPtr pPlain = Marshal.SecureStringToGlobalAllocUnicode(secStr); // do this last to minimize the availability of "pPlain"
            try // free "pPlain" even if Copy() throws
            {
                Marshal.Copy(pPlain, plain, 0, stringSize);
            }
            finally
            {
                Marshal.ZeroFreeGlobalAllocUnicode(pPlain); // clear plaintext from unmanged memory ASAP
            }

            UnicodeEncoding encoder = new UnicodeEncoding();

            return encoder.GetString(plain);
        }


        static readonly DataContractJsonSerializerSettings UseSimpleDictionaryFormatSettings = new DataContractJsonSerializerSettings()
        {
            UseSimpleDictionaryFormat = true
        };

        internal static HttpContent JsonSerialize(object o)
        {

            DataContractJsonSerializer serializer = new DataContractJsonSerializer(o.GetType(), UseSimpleDictionaryFormatSettings);
            MemoryStream ms = null;
            try
            {
                ms = new MemoryStream();
                serializer.WriteObject(ms, o);
                ms.Position = 0;

                StreamContent retval = new StreamContent(ms);
                ms = null;
                return retval;
            }
            finally
            {
                if (ms != null)
                    ms.Dispose();
            }
        }

        internal static T JsonDeserialize<T>(HttpResponseMessage response)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T), UseSimpleDictionaryFormatSettings);
            return (T)serializer.ReadObject(response.Content.ReadAsStreamAsync().Result);
        }

        internal static T JsonDeserialize<T>(string jsonString)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T), UseSimpleDictionaryFormatSettings);

            UnicodeEncoding enc = new UnicodeEncoding();
            byte[] bytes = enc.GetBytes(jsonString);

            System.IO.MemoryStream stream = new MemoryStream(bytes);
            return (T)serializer.ReadObject(stream);
        }

        /// <summary>
        /// Deserialize from a string to an object.  Return null if there is a serialization failure.
        /// </summary>
        internal static T TryJsonDeserialize<T>(string jsonString)
        {
            try
            {
                return JsonDeserialize<T>(jsonString);
            }
            catch
            {
                return default(T);
            }
        }

        internal static string ReadString(Stream stream)
        {
            // assuming 100k will hold all the data
            byte[] buffer = new byte[1024 * 100];
            int bytesRead = stream.Read(buffer, 0, buffer.Length);
            return System.Text.UTF8Encoding.UTF8.GetString(buffer, 0, bytesRead);
        }

        internal static Dictionary<string, string> ParseOAuthResponse(string response)
        {
            Dictionary<string, string> retval = new Dictionary<string, string>();

            string[] attributes = response.Split(new char[] { '&' });
            foreach (string attribute in attributes)
            {
                string[] values = attribute.Split(new char[] { '=' });
                if (values.Length == 2)
                    retval.Add(values[0], values[1]);
            }

            return retval;
        }
    }
}