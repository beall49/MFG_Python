﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Mes.Cache
{
    [XmlRoot]
    public class Settings
    {

        public string url;
        public String username;
        public string password;

        private static Settings instance;
        private Settings()
        {

        }

        public static Settings Instance
        {
            get
            {

                if (instance == null)
                {
                    instance = Settings.Load();
                }


                return instance;
            }
        }

        public string URL
        {
            get
            {
                return url;
            }
        }

        public string UserName
        {
            get
            {
                return username;
            }
        }

        public string Password
        {
            get
            {
                return password;
            }
        }

        private static Settings Load()
        {
            Settings retVal = new Settings();

            try
            {
                string xmlPath = System.Web.HttpContext.Current.Server.MapPath(@"~\App_Data\settings.xml");

                using (System.IO.StreamReader reader = new System.IO.StreamReader(xmlPath))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Settings));
                    retVal = (Settings)serializer.Deserialize(reader);
                }
            }
            catch
            { }

            return retVal;
        }

    }



    [Serializable]
    [XmlRoot]
    public class LoginSettings
    {
        public String URL;
        public String userName;
        public String password;
    }
}
