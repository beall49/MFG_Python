"""
	if theres xml, this transforms it against the template
	and creates html from it
"""

	try:
		from java.io import StringReader, StringWriter
		from javax.xml.transform import Transformer, TransformerFactory
		from javax.xml.transform.stream import StreamResult, StreamSource
		transformer = TransformerFactory.newInstance().newTransformer(StreamSource(StringReader(root.jjXLST)))
		output_buffer = StringWriter()
		transformer.transform(StreamSource(StringReader(XML)), StreamResult(output_buffer))
		content=output_buffer.buffer.toString()
		root.getComponent('cntJobJacket').getComponent('webXML').getBrowser().loadHTML(content)
	except:
		try:
			root.getComponent('cntJobJacket').getComponent('webXML').getBrowser().loadHTML("<HTML> AN ERROR HAS OCCURED</HTML>")
		except:
			pass