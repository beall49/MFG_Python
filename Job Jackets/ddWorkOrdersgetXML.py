"""
	this can be called from anywhere,
	goes out to g3 api and gets the sku/plm data
"""
	try:
		import urllib2
		root=system.gui.getWindow('Main Windows/JobJackets').getRootContainer()
		opener = urllib2.build_opener(urllib2.HTTPSHandler)
		request = urllib2.Request('http://g3mesprd:9090/api/JobJacket/' + self.productCode)
		request.add_header('Content-Type', 'application/xml')	
	
		xml=opener.open(request).read()   
		if xml:
			root.jjXML=xml
			self.getAttachments(root, xml)
			self.setXML(root, xml)
			root.state=0	
	except:
		try:
			root=system.gui.getWindow('Main Windows/JobJackets').getRootContainer()
			root.getComponent('cntJobJacket').getComponent('webXML').getBrowser().loadHTML("<HTML> AN ERROR HAS OCCURED</HTML>")
		except:
			pass