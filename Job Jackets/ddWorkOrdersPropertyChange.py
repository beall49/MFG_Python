"""
	when a sku is selected, this starts the process
	of getting the sku data and attachment xml
"""
if event.propertyName=="selectedLabel":	
	if event.source.selectedLabel!="<Select One>":
		
		from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs
		root=system.gui.getWindow('Main Windows/JobJackets').getRootContainer()
#		root.getComponent('imgCountdown').visible=True
		root.getComponent('cntJobJacket').getComponent('webXML').getBrowser().loadHTML(root.loadingHTML)
		doc=root.getComponent('cntDocuments').getComponent('webDoc')
		doc.getBrowser().loadHTML(doc.startingHtml)
		doc.mode=1
		root.getComponent('ddDocs').fileName=""	
		dd=root.getComponent('ddWorkOrders')
		tbl=toPyDs(dd.data)
		row=dd.selectedIndex
		if row>-1:
			dd.productCode=str(tbl[row]["PRODUCTCODE"])#"C217628-01"
			dd.workOrder=str(tbl[row]["WORKORDER"])
			try:
				system.util.invokeAsynchronous(event.source.getXML)
			except:
				pass