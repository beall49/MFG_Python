	'''/*
			*These are good records if needed to test the viewing method
			*#"69","TPSTK+GLD1010-FlxPrt-2855approved.pdf","206793","14381"
			*#"59","319023003C.pdf","182329","19801"
	*/'''
	import system
	from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs, getColumnHeaders as getHeaders
	root=system.gui.getWindow('Main Windows/JobJackets').getRootContainer()
	browser=root.getComponent('cntDocuments').getComponent('webDoc')
	
	tbl=toPyDs(self.data)
	row=self.selectedIndex
	root.getComponent('btnRefresh').enabled=False
	
	##make sure something was selected
	if row>-1:
		strURL=str(tbl[row]['URL'])
		browser.getBrowser().loadURL((strURL).encode('utf-8') )
		self.selectedLabel="<Select One>"
		self.fileName=str(tbl[row]['DISPLAY NAME'])