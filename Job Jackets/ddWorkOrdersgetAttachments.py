"""
	loops through xml and finds each attachment, appends it to dropdown
	@param xml = incoming xml document
"""	
	try:
		import xml.dom.minidom
		from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs
		docs=[]	
		strURL='http://g3mesprd:9090/api/File/%s/%s/%s'
			
		dom = xml.dom.minidom.parseString(XML)
		dd= root.getComponent('ddDocs')		
		dd.selectedLabel="<Select One>"
		dd.selectedIndex=-1	
		dd.data = toPyDs(toDs(["URL", "DISPLAY NAME"], [])) 
		count=0		
		
		for nodes in dom.getElementsByTagName("Attachments"):
			for node in nodes.getElementsByTagName("d2p1:Attachment"):
				try:
					dmsid=node.getElementsByTagName("d2p1:DmsID")[0].firstChild.data
					fileId=node.getElementsByTagName("d2p1:FileId")[0].firstChild.data
					fileName=node.getElementsByTagName("d2p1:Filename")[0].firstChild.data
					workSpace=node.getElementsByTagName("d2p1:WorkSpace")[0].firstChild.data
					displayName=node.getElementsByTagName("d2p1:Type")[0].firstChild.data + str(count*" ")
					count+=1
					docs.append([strURL % (workSpace, dmsid, fileId), displayName])
				except:
					'broken file'
		if docs: 
			dd.data= toPyDs(toDs(["URL", "DISPLAY NAME"], docs))
		else:
			dd.data =toPyDs(toDs(["URL", "DISPLAY NAME"], [])) 
			dd.fileName="None"
	except:
		pass
		