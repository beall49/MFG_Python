#print event.propertyName
import base64, pprint, sys
from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs, getColumnHeaders as getHeaders
from system.util import jsonDecode as jsonD, jsonEncode as jsonE
from system.net import httpPost as POST, httpGet as GET
from system.db import beginTransaction as beginTx, runPrepUpdate as UPDATE, commitTransaction as commitTX, closeTransaction as closeTx

if system.gui.findWindow("Main Windows/JobJackets"):
	if all([event.propertyName=="selectedLabel", event.source.selectedLabel!="<Select One>"]):
		#render the selected doc
		tbl=toPyDs(event.source.data)
		row=event.source.selectedIndex	
		
		if row>-1:
			root=system.gui.getWindow('Main Windows/JobJackets').getRootContainer()
			#GET SELECTED ROW FROM DROWNDOWN AND PASS THAT URL TO BROWSER
			browser=root.getComponent('cntDocuments').getComponent('webDoc')
			strURL=str(tbl[row]['URL'])
			fileName=str(tbl[row]['DISPLAY NAME'])
			event.source.fileName=fileName			
			browser.mode=0
			browser.getBrowser().loadURL((strURL).encode('utf-8') )
			
		