import base64, pprint, sys
	from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs, getColumnHeaders as getHeaders
	from system.util import jsonDecode as jsonD
	from system.net import httpGet as GET
	
	root=system.gui.getWindow('Main Windows/JobJackets').getRootContainer()
	getUrl="https://g3enterprises.autodeskplm360.net/api/rest/v1/workspaces/82/items/%s/grids" % root.getComponent('ddWorkOrders').dmsid 
	sessionId=system.tag.getTagValue('[Client]sessionId')
	headers={"Accept": "application/json", "Cookie": str("customer=G3ENTERPRISES; JSESSIONID=%s" % (sessionId))}
	dd=self
	
	try:
		tbl=str(jsonD(GET(url=getUrl,
							connectTimeout=10000,
							readTimeout=10000,
							username="",
							password="",
							headerValues=headers,
							bypassCertValidation=True)))
	except:
		tbl=None
		
	if tbl:
		contents= list(jsonD(tbl)['grid']['row'])
		documents, ds=[], []
		
		for content in contents:
			docs={}
			data= list((content['columns'].items()[0])[1])
		
			for d in data:
				docs[d['key']]=d['fieldData']['value']
			documents.append(docs)
	  
		ds.extend([[doc['WORKSPACE_ID'],doc['FILE_NAME'],doc['DMSID'],doc['FILEID']] for doc in documents])
	
		dd.data= toPyDs(toDs(getHeaders(dd.data), ds)) if ds else toPyDs(toDs([], []))
		dd.refreshed=0