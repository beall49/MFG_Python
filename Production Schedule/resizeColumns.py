	try:
		from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs, getColumnHeaders as getHeaders
		root=system.gui.getWindow("Main Windows/ProductionSchedule").getRootContainer()
		sizes = {0:15, 1:35, 2:205,3:80, 4:90, 5:45,6:45,7:40, 8:40, 9:30, 10:40, 11:45, 12:60, 13:45, 14:45, 15:45, 16:45, 17:45, 18:45, 19:45, 20:45, 21:45, 22:45, 23:45, 24:45, 25:45, 26:45, 27:45, 28:45, 29:45, 30:45, 31:45, 32:45, 33:45, 34:225}
		
		tbl = self
		#loop through the columns and set them to sizes
		for x in range(0,len(sizes)):		
			tbl.setColumnWidth(x,sizes.get(x,45))
			if x>25 and root.ShiftPattern == 'DayNight':
				tbl.setColumnWidth(27,500 )
				break;
	except:
		from org.apache.log4j import Logger; import traceback
		log = Logger.getLogger("you errored on resize columns")    
		log.error(str(traceback.format_exc()))  