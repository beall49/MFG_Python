#misc
def getJdeLine(lineName):
	results = shared.SQLs.runQry([], ("SELECT DISTINCT JDEPRODUCTIONLINE FROM G3_LINELOOKUP WHERE MESLINENAME = '" + lineName + "'"))
	return results[0][0] if results else ''	

def getShortLineName(lineName):
	results = shared.SQLs.runQry([], ("SELECT DISTINCT SHORTNAME FROM G3_LINELOOKUP WHERE MESLINENAME = '" + lineName + "'"))
	return results[0][0] if results else ''

def getWorkOrderId(workOrder):
	results = shared.SQLs.runQry([], ("SELECT DISTINCT ID FROM WORKORDER WHERE WORKORDER = '" + workOrder + "'"))
	return results[0][0] if results else -1

#Notes	
def setNote(id, note):	
	shared.SQLs.runQry([], ("UPDATE G3_WORKORDER SET NOTES = '" + note + "', TIMESTAMP = CURRENT_TIMESTAMP WHERE WORKORDERID = " + str(id) ))

def getNote(id):
	results = shared.SQLs.runQry([], ("SELECT DISTINCT NOTES FROM G3_WORKORDER WHERE WORKORDERID = '" + str(id) + "'"))
	return results[0][0] if results else ''

#Alerts
def getAlert(id):
	results = shared.SQLs.runQry([], ("SELECT DISTINCT ALERT FROM G3_WORKORDER WHERE WORKORDERID = " + str(id)))
	return results[0][0] if results else 0

def setAlert(id, alert):
	print id, alert
	shared.SQLs.runQry([], ("UPDATE G3_WORKORDER SET ALERT = " + str(alert) + ", TIMESTAMP = CURRENT_TIMESTAMP WHERE WORKORDERID = " + str(id) ))

#customer name
def getCustomerNameForWorkOrder(workOrder):
		#go to JDE/GIW and get the customer name for the work order based on the query logic
		return ''
		
def getCustomerName(id):
	results = shared.SQLs.runQry([], ("SELECT DISTINCT CUSTOMER FROM G3_WORKORDER WHERE WORKORDERID = '" + str(id) + "'"))
	return results[0][0] if results else ''
	
def setCustomerName(id, customerName):
	shared.SQLs.runQry([], ("UPDATE G3_WORKORDER SET CUSTOMER = '" + customerName + "', TIMESTAMP = CURRENT_TIMESTAMP WHERE WORKORDERID = " + str(id) ))

#ship date
def getShipDate(id):
		results = shared.SQLs.runQry([], ("SELECT DISTINCT SHIPDATEOVERRIDE FROM G3_WORKORDER WHERE WORKORDERID = '" + str(id) + "'"))
		if results:
			return results[0][0] if results[0][0] else 1
	
def setShipDate(id, shipDate):
	shared.SQLs.runQry([], ("UPDATE G3_WORKORDER SET SHIPDATEOVERRIDE = '" + shipDate + "', TIMESTAMP = CURRENT_TIMESTAMP WHERE WORKORDERID = " + str(id)) )
	
	
def shiftStart(dayOfWeek=7, shiftStyle="GraveDaySwing"):
	import datetime; 
	#when shift starts based on sihft type
	timeOfDay = "23:00:00" if shiftStyle=="GraveDaySwing" else "06:00:00"
	#7 is monday of current week 6 is last sunday 
	return (datetime.date.today() - datetime.timedelta(days=datetime.date.today().weekday()) + datetime.timedelta(days=dayOfWeek, weeks=-1)).strftime("%m/%d/%Y " + timeOfDay)
	
def shiftEnd(dayOfWeek=7, shiftStyle="GraveDaySwing"):
	import datetime; 
	#when shift starts based on sihft type
	timeOfDay = "15:00:00" if shiftStyle=="GraveDaySwing" else "18:00:00"
	#7 is monday of current week 6 is last sunday 
	return (datetime.date.today() - datetime.timedelta(days=datetime.date.today().weekday()) + datetime.timedelta(days=dayOfWeek, weeks=-1)).strftime("%m/%d/%Y " + timeOfDay)			
	
	
	
	
	
	
	
	
	