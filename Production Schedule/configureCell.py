import datetime
shifts={"MON_3":13,"MON_1":14,"MON_2":15,"TUE_3":16,"TUE_1":17,"TUE_2":18,"WED_3":19,"WED_1":20,"WED_2":21,"THU_3":22,"THU_1":23,"THU_2":24,"FRI_3":25,"FRI_1":26,"FRI_2":27,"SAT_3":28,"SAT_1":29,"SAT_2":30,"SUN_3":31,"SUN_1":32,"SUN_2":33}

root=system.gui.getWindow("Main Windows/ProductionSchedule").getRootContainer()
week=root.getComponent('btnWeeks').indicatorValue
today=datetime.datetime.now().strftime("%a").upper() + "_" + str(root.CurrentShift)

def defaultRow(rowView):
	return {'background': '#D5D5D5'} if rowView % 2 else {'background': 'white'}

#BLACK SEPERATOR LINES
if self.data.getValueAt(rowIndex, 3)==None:
	return {'background': '#6E7B8B'} #'black'

#WHEN SELECTED		
if selected:
	return {'background': self.selectionBackground}	

#BOLD MACHINE NAME	 #1705F5BD
if colName.upper()=="MACH":
	background = defaultRow(rowView)
	return {'font' : 'DokChampa, Bold, 11', 'foreground':'black', background.keys()[0]: background.values()[0]}
	
#IF VAR <0
if colName.upper()=="VAR":
	if self.data.getValueAt(rowIndex, colIndex)<0:
		background = defaultRow(rowView)
		return {'foreground': '#F75D59', background.keys()[0]: background.values()[0]}
		
#IF PRWK >0
if colName.upper()=="PRWK":
	if self.data.getValueAt(rowIndex, colIndex)>0: #F75D5975
		return {'background': '#F75D59'}	 if rowView % 2 else {'background': '#F75D5975'}				
#SKU AND DESCRIPTION
if colIndex in[3,4]:		
	if self.data.getValueAt(rowIndex, 0)==1:
		return {'background': '#FF9933'}
						
#THIS WEEK
if week==0:	
	#IF TODAY
	if colName==today:	
		#IF IN CHANGEOVER	
		if self.data.getValueAt(rowIndex, 36)>0:
			return {'background': '#FFFF47', 'foreground': '#000000'}
		#IF ACTIVE
		elif self.data.getValueAt(rowIndex, 37)>0:
			return {'background': '#33FF66', 'foreground': '#000000'}	
		#NORMAL TODAY
		else: 	#original color == #6AF7DEC9
			background = {'background': '#6AF7DE'} if rowView % 2 else {'background': '#6AF7DE87'}			
			return {background.keys()[0]: background.values()[0], 'foreground': '#000000'}
			
	#PRIOR TO CURRENT SHIFT
	elif colIndex> 12 and colIndex < shifts.get(today):		 #C6AEC775	
		return {'background': '#C6AEC7'} if rowView % 2 else {'background': '#C6AEC775'}	
				
	#NORMAL ROW VIEW
	else:
		return defaultRow(rowView)
		
#NORMAL ROW VIEW
else:
	return  {'background': '#C6AEC7'} if all([week==1, colIndex> 12, colIndex <35]) else defaultRow(rowView)
	
