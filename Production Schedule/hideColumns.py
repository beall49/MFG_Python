	#add a day
	def addDay(date, add):
		from java.util import Calendar
		from java.text import SimpleDateFormat
		df = SimpleDateFormat("MM/dd")	
		cal = Calendar.getInstance()
		cal.setTime(date);	
		cal.add(Calendar.DATE, add)
		return df.format(cal.getTime())
		
	#gets day of week
	def getIntOfWeek(date):
		from java.util import Calendar
		c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.DAY_OF_WEEK);
	
	#add an hour
	def addHour(date):
		from java.util import Calendar
		from java.text import SimpleDateFormat
		df = SimpleDateFormat("MM/dd")	
		cal = Calendar.getInstance()
		cal.setTime(date);	
		cal.add(Calendar.HOUR, 1)
		return (cal.getTime())
			
	try:
		from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs, getColumnHeaders as getHeaders
		root=system.gui.getWindow("Main Windows/ProductionSchedule").getRootContainer()
		data, newRow=[], []
		
		#set the day to corresponding int (gets rid of need for if statements)
		dateAdd={"MON":0, "TUE":1, "WED":2, "THU":3, "FRI":4, "SAT":5, "SUN":6}

		#columns the user doesn't need but we do
		hiddencols =["ACTIVEWO","INCHANGEOVER","WORKORDERID"]

		#get a weekstart data time
		weekStart = addHour(root.WeekStartDate) if getIntOfWeek(root.WeekStartDate)==1 else root.WeekStartDate 
		
		#the actual table object
		tbl = root.getComponent('tblProd')	
		headers = getHeaders(tbl.getColumnAttributesData())

		#column attribute data, very icky dataset, basically settings for the table
		attr = toPyDs(tbl.columnAttributesData)
		
		#for row in attributes
		for row in attr:
			label = row["label"] 
			#if it's a shift label (column header)
			if row["name"].endswith(("1","2","3")):		
				#create label from html to center and have a <br/>
				label = "<html> " + str(addDay(weekStart, dateAdd.get(str(row[0])[:3],0))) + "<br/>" + row["name"] + "</html>"
			
			#if there's only 2 shifts hide the 3rd shift column or if this specific column is in the hidden columns
			hidden=True if all([root.ShiftPattern == 'DayNight',str(row['name']).endswith("3")]) or str(row['name']).upper()in hiddencols else False	
			
			#create a new row with the settings you want
			newRow= [row["name"],row["dateFormat"],row["editable"],row["filterable"],hidden,row["horizontalAlignment"],label,row["numberFormat"],row["prefix"],row["sortable"],row["suffix"],row["treatAsBoolean"],row["verticalAlignment"],row["wrapText"]]		
			data.append(newRow)
		
		tbl.columnAttributesData = toPyDs(toDs(headers, data))

	
		#hide blank rows, this is happening bc it gets the same workorder set for all 3 weeks
		if root.getComponent('btnHide').selected==False:
			rows=[]
			tblProd=toPyDs(tbl.data)	
			for row in range(0, len(tblProd)):	
				if tblProd[row][3]!=None:				
					if sum(float(tblProd[row][col]) if (type(tblProd[row][col])==unicode) else 0 for col in range(13,34))<1:
						rows.append(row)
				
						
			tbl.data=system.dataset.deleteRows(tbl.data, rows)	if len(rows)>0 else tbl.data	
		self.createSeperator()	
	except:
		from org.apache.log4j import Logger; import traceback
		log = Logger.getLogger("you errored on hide columns")    
		log.error(str(traceback.format_exc()))  