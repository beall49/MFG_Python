"""
	when data refreshes call the hide and resize functions
"""
if event.propertyName=="data":
	event.source.hideColumns()
	event.source.resizeColumns()
	
"""
	when a row is selected
	get the workorderid and put it at the root
"""	
if event.propertyName == "selectedRow":
	row = event.source.selectedRow
	col = event.source.selectedColumn
	if row>-1:
		if event.source.data.getColumnName(34) == "WORKORDERID":
			event.source.workOrderId=event.source.data.getValueAt(row, 34)

"""
	When a column is selected, try and sum, avg all the selected columns
"""				
if event.propertyName == "selectedColumns":
	root=system.gui.getWindow("Main Windows/ProductionSchedule").getRootContainer()		
	tblProd = root.getComponent('tblProd')
	rows,cols,count = tblProd.selectedRows,tblProd.selectedColumns,0
	mySum, myAvg = 0, 0
	for row in rows:
		for col in cols:
			if type(tblProd.data.getValueAt(row, col))in [unicode, int, float]:
				try:
					temp = float(tblProd.data.getValueAt(row, col))
				except:
					temp = 0
				mySum += temp
				count = count +1 if temp>0 else count 
				
	root.getComponent('txtSum').value= mySum
	root.getComponent('txtAvg').value= (mySum/count) if count>1 else mySum