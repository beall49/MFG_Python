import system
"""
	This should update certain columns in the database
"""
if newValue == None:
	newValue = ""
if colName.upper() == "!":
	bool = 1 if newValue else 0		
	project.prodSched.setAlert( self.workOrderId, bool)
elif colName.upper() == "NOTES":
	project.prodSched.setNote( self.workOrderId, newValue)
elif colName.upper() == "CUST":
	project.prodSched.setCustomerName( self.workOrderId, newValue)
elif colName.upper() == "SHIPDATE":
	project.prodSched.setShipDate( self.workOrderId, newValue)	
					
root=system.gui.getWindow("Main Windows/ProductionSchedule").getRootContainer()
tbl=root.getComponent('tblProd')
system.db.refresh(tbl,"data")	