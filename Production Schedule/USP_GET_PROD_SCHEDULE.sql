USE [mes_runtime]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
;


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[USP_GET_PROD_SCHEDULE]
	 @PRODAREA   VARCHAR(50),	
	 @SHIFT varchar(1),
	 /*
		THIS IS BACKWARDS. -1 GOES FORWARD. +1 GOES BACKWARDS. 0 STAYS THE SAME WHO KONWS WHY
	 */
	 @WEEK INT
AS
BEGIN

 SET NOCOUNT ON

 /*IF TODAY IS SUNDAY, MOVE THE DATE BACK 1 DAY (IT'S FASTER THAN THE SP)*/
 DECLARE @TODAY			DATETIME = (CASE DATEPART ( DW , GETDATE()) WHEN   1 THEN GETDATE()-1 ELSE GETDATE() END)

 DECLARE @WEEKSTART1	DATETIME = DATEADD(wk, DATEDIFF(wk,1 * (7*@WEEK),@TODAY), 0), 		 
		 @WEEKSTART2	DATETIME, 
		 @WEEKSTART3	DATETIME, 
		 @BEG_DATE		DATETIME, 
		 @END_DATE		DATETIME,
		 @SHIFT_PATTERN VARCHAR(50)
		 --,		 @CURRENTSHIFT DATETIME
		 

 /*
	GET PRD METHOD
 */
DECLARE @TBLPRD TABLE(PRODUCTIONMETHOD VARCHAR(10), PRODUCTCODE VARCHAR(50))
INSERT INTO @TBLPRD
	SELECT 
		PRODUCTIONMETHOD, PRODUCTCODE
	FROM 
		[MES_RUNTIME].[DBO].[G3_PRODUCTCODE] G3P, [MES_RUNTIME].[DBO].[PRODUCTCODE]  P
	WHERE 
		P.ID = G3P.PRODUCTCODEID 
 
 /*
	GET LINES
 */
 DECLARE @TBL TABLE( LINES VARCHAR(20))
 INSERT INTO @TBL EXEC [MES_RUNTIME].[dbo].[USP_GET_WO_LOOKUP] @PRODAREA

 /*
	SET SHIFT PATTERN
 */
 SELECT @SHIFT_PATTERN = [PATTERNNAME]  FROM [MES_RUNTIME].[DBO].[G3_SHIFTLINE]  WHERE LINE = (SELECT TOP 1 LINES FROM @TBL)

/*
	FIGURE OUT START TIMES
*/
IF @SHIFT_PATTERN != 'DayNight' 
	BEGIN
		SET @WEEKSTART3 =DATEADD(HOUR, -1, @WEEKSTART1);
		SET @WEEKSTART1 =DATEADD(HOUR, +7, @WEEKSTART1);
		SET @WEEKSTART2 =DATEADD(HOUR, +8, @WEEKSTART1);
		SET @BEG_DATE = @WEEKSTART3;
		--SET @CURRENTSHIFT =(CASE WHEN @SHIFT = 1 THEN CAST(CONVERT(VARCHAR(19), GETDATE()-1 , 101) + ' 23:00:00' AS DATETIME) ELSE	
		--					CASE WHEN @SHIFT = 2 THEN CAST(CONVERT(VARCHAR(19), GETDATE() , 101) + ' 6:00:00' AS DATETIME) ELSE
		--					CASE WHEN @SHIFT = 3 THEN CAST(CONVERT(VARCHAR(19), GETDATE() , 101) + ' 15:00:00' AS DATETIME) END END END)
	END
ELSE
	BEGIN
		SET @WEEKSTART1 =DATEADD(HOUR, +6, @WEEKSTART1);
		SET @WEEKSTART2 =DATEADD(HOUR, 12, @WEEKSTART1);
		SET @WEEKSTART3 =DATEADD(HOUR, 12, @WEEKSTART2);
		SET @BEG_DATE = @WEEKSTART1;
		--SET @CURRENTSHIFT =(CASE WHEN @SHIFT = 1 THEN CAST(CONVERT(VARCHAR(19), GETDATE() , 101) + ' 06:00:00' AS DATETIME) ELSE	
		--					CAST(CONVERT(VARCHAR(19), GETDATE() , 101) + ' 18:00:00' AS DATETIME)  END)
	END

/*
	end date
*/
SET @END_DATE = CAST( DATEADD(DAY, 7, @BEG_DATE) AS DATETIME)



SELECT 
	ALERT AS "!",
	MACH,
	INNERTBL.DESCR AS "DESC",
	ITEM,
	WO,
	ROUND(TOTALSCHEDULED/UOMCONV,0) AS QTY,
	(
	--WHEN WEEK IS NEXT WEEK THEN 0 
	CASE WHEN @WEEK = -1 THEN 0 
		ELSE 
			--WHEN WEEK IS LAST WEEK DO MATH FOR VAR
			CASE WHEN @WEEK =  1 THEN (ROUND(TOTALPRODUCEDPRWEEK/UOMCONV,0) - ROUND(TOTALSCHEDULED/UOMCONV,0)) 
				ELSE 
					/*  THIS GETS THE SHIFT TARGET STARTING AT THE CURRENT SHIFT*/
					CASE WHEN @WEEK =  0 THEN (
						SELECT 
							ROUND
								(
									(
										(
											(
												SELECT 
													SUM( SHIFTTARGET ) 
												FROM 
													[MES_RUNTIME].[DBO].[G3_PRODUCTIONSCHEDULE] 
												WHERE 
													MESLINE IN  ( SELECT * FROM @TBL ) AND 
													SHIFTSTART > cast(getdate() as datetime) AND 
													SHIFTSTART < @END_DATE AND 
													WORKORDERID = INNERTBL.WORKORDERID 
											)   +  (TOTALPRODUCEDCURWEEK + TOTALPRODUCEDPRWEEK ) 
										)  - TOTALSCHEDULED )/(UOMCONV),0)
					) END END END 
	)AS "VAR", 
	SHIPDATE,
	BINS,
	LN,
	CUST,
	(TOTALPRODUCEDPRWEEK/UOMCONV) AS PRWK,
	--GET PROD METHOD FOR THIS ITEM
	(SELECT TOP 1 PRODUCTIONMETHOD  FROM @TBLPRD WHERE PRODUCTCODE = ITEM) AS MTHD,

	--FORMATTING COLUMNS
    CASE WHEN SUM(MON_3/UOMCONV) <1 THEN NULL ELSE FORMAT(CAST(SUM(CAST(MON_3 AS FLOAT)/UOMCONV) AS FLOAT),'#') END AS MON_3,
	CASE WHEN SUM(MON_1/UOMCONV) <1 THEN NULL ELSE FORMAT(CAST(SUM(CAST(MON_1 AS FLOAT)/UOMCONV) AS FLOAT),'#') END AS MON_1,
	CASE WHEN SUM(MON_2/UOMCONV) <1 THEN NULL ELSE FORMAT(CAST(SUM(CAST(MON_2 AS FLOAT)/UOMCONV) AS FLOAT),'#') END AS MON_2,
	

	CASE WHEN SUM(TUE_3/UOMCONV) <1 THEN NULL ELSE FORMAT(CAST(SUM(CAST(TUE_3 AS FLOAT)/UOMCONV) AS FLOAT),'#') END AS TUE_3,
	CASE WHEN SUM(TUE_1/UOMCONV) <1 THEN NULL ELSE FORMAT(CAST(SUM(CAST(TUE_1 AS FLOAT)/UOMCONV) AS FLOAT),'#') END AS TUE_1,
	CASE WHEN SUM(TUE_2/UOMCONV) <1 THEN NULL ELSE FORMAT(CAST(SUM(CAST(TUE_2 AS FLOAT)/UOMCONV) AS FLOAT),'#') END AS TUE_2,
	
	
	CASE WHEN SUM(WED_3/UOMCONV) <1 THEN NULL ELSE FORMAT(CAST(SUM(CAST(WED_3 AS FLOAT)/UOMCONV)AS FLOAT),'#') END AS WED_3,
	CASE WHEN SUM(WED_1/UOMCONV) <1 THEN NULL ELSE FORMAT(CAST(SUM(CAST(WED_1 AS FLOAT)/UOMCONV)AS FLOAT),'#') END AS WED_1,
	CASE WHEN SUM(WED_2/UOMCONV) <1 THEN NULL ELSE FORMAT(CAST(SUM(CAST(WED_2 AS FLOAT)/UOMCONV)AS FLOAT),'#') END AS WED_2,
	
	
	CASE WHEN SUM(THU_3/UOMCONV) <1 THEN NULL ELSE FORMAT(CAST(SUM(CAST(THU_3 AS FLOAT)/UOMCONV)AS FLOAT),'#') END AS THU_3,
	CASE WHEN SUM(THU_1/UOMCONV) <1 THEN NULL ELSE FORMAT(CAST(SUM(CAST(THU_1 AS FLOAT)/UOMCONV)AS FLOAT),'#') END AS THU_1,
	CASE WHEN SUM(THU_2/UOMCONV) <1 THEN NULL ELSE FORMAT(CAST(SUM(CAST(THU_2 AS FLOAT)/UOMCONV)AS FLOAT),'#') END AS THU_2,
	

	CASE WHEN SUM(FRI_3/UOMCONV) <1 THEN NULL ELSE FORMAT(CAST(SUM(CAST(FRI_3 AS FLOAT)/UOMCONV)AS FLOAT),'#') END AS FRI_3,
	CASE WHEN SUM(FRI_1/UOMCONV) <1 THEN NULL ELSE FORMAT(CAST(SUM(CAST(FRI_1 AS FLOAT)/UOMCONV)AS FLOAT),'#') END AS FRI_1,
	CASE WHEN SUM(FRI_2/UOMCONV) <1 THEN NULL ELSE FORMAT(CAST(SUM(CAST(FRI_2 AS FLOAT)/UOMCONV)AS FLOAT),'#') END AS FRI_2,
	
	
	CASE WHEN SUM(SAT_3/UOMCONV) <1 THEN NULL ELSE FORMAT(CAST(SUM(CAST(SAT_3 AS FLOAT)/UOMCONV) AS FLOAT),'#') END AS SAT_3,
	CASE WHEN SUM(SAT_1/UOMCONV) <1 THEN NULL ELSE FORMAT(CAST(SUM(CAST(SAT_1 AS FLOAT)/UOMCONV) AS FLOAT),'#') END AS SAT_1,
	CASE WHEN SUM(SAT_2/UOMCONV) <1 THEN NULL ELSE FORMAT(CAST(SUM(CAST(SAT_2 AS FLOAT)/UOMCONV) AS FLOAT),'#') END AS SAT_2,
	
	
	CASE WHEN SUM(SUN_3/UOMCONV) <1 THEN NULL ELSE FORMAT(CAST(SUM(CAST(SUN_3 AS FLOAT)/UOMCONV) AS FLOAT),'#') END AS SUN_3,
	CASE WHEN SUM(SUN_1/UOMCONV) <1 THEN NULL ELSE FORMAT(CAST(SUM(CAST(SUN_1 AS FLOAT)/UOMCONV) AS FLOAT),'#') END AS SUN_1,
	CASE WHEN SUM(SUN_2/UOMCONV) <1 THEN NULL ELSE FORMAT(CAST(SUM(CAST(SUN_2 AS FLOAT)/UOMCONV) AS FLOAT),'#') END AS SUN_2,
	
	WORKORDERID, 
	NOTES,
	INCHANGEOVER,
	ACTIVEWO
FROM (
		SELECT
			  Alert,
			  CASE WHEN (TOTALPRODUCED+TOTALSCHEDULED) > 0 THEN SHORTNAME ELSE '' END AS Mach,
			  CASE WHEN TOTALSCHEDULED = 0 THEN 0 ELSE 1 END AS SCHEDULED,
			  CASE WHEN JDELINE IN ('91','92') THEN TOTALSCHEDULED / (UOMCONV * 10)  ELSE 0 END AS BINS,
			  CASE WHEN JDELINE IS NULL THEN (SELECT TOP 1 [JDEPRODUCTIONLINE] FROM [MES_RUNTIME].[DBO].[G3_LINELOOKUP] G WHERE G.MESLINENAME = MesLine) ELSE JDELINE END AS LN,
			  WORKORDER AS WO,
			  WORKORDERID,
			  PRODUCTCODE as ITEM,
			  CUSTOMER AS CUST,
			  PRODUCTCODEDESC as "DESCR",
			  UOMCONV,
			  ACTIVEWO,	  
			  INCHANGEOVER,
			  CLOSED,
			  CUSTOMER,
			  NOTES,
			  SHIPDATEOVERRIDE AS SHIPDATE,
			  TOTALPRODUCEDPRWEEK,
			  TOTALSCHEDULED,
			  TOTALPRODUCED, 
			  /*
				--THIS IS A PRIMER FOR WHAT'S HAPPENING BELOW
				--ITS THE SAME FOR EACH COLUMN, JUST DIFFERENT START/SHIFT TIMES
				IF SHIFT START = CALCUATED SHIT START TIME FOR THAT DAY THEN
						--for 3rd shift only
					IF SHIFT PATTERN = DAYNIGHT THEN 
						RETURN 0 (NO 3RD SHIFT)

						--for shifts 1/2
					IF SHIFT START > NOW 
						GET TARGET
					ELSE
						GET ACTUAL
			  */
			  --MON
			  CASE WHEN ShiftStart = CAST(  @WEEKSTART1 AS DATETIME)  THEN (CASE WHEN SHIFTSTART>CURRENT_TIMESTAMP THEN SHIFTTARGET ELSE SHIFTACTUAL END)  ELSE 0 END  AS MON_1,
			  CASE WHEN ShiftStart = CAST(  @WEEKSTART2 AS DATETIME)  THEN (CASE WHEN SHIFTSTART>CURRENT_TIMESTAMP THEN SHIFTTARGET ELSE SHIFTACTUAL END)  ELSE 0 END  AS MON_2,
			  CASE WHEN @SHIFT_PATTERN = 'DayNight' THEN 0 ELSE CASE WHEN ShiftStart = CAST( DATEADD(DAY, 0, @WEEKSTART3) AS DATETIME)  THEN (CASE WHEN SHIFTSTART>CURRENT_TIMESTAMP THEN SHIFTTARGET ELSE SHIFTACTUAL END)  ELSE 0 END END  AS MON_3,
			  --TUE
			  CASE WHEN ShiftStart = CAST(  DATEADD(DAY, 1, @WEEKSTART1) AS DATETIME) THEN (CASE WHEN SHIFTSTART>CURRENT_TIMESTAMP THEN SHIFTTARGET ELSE SHIFTACTUAL END)  ELSE 0 END  AS TUE_1,
			  CASE WHEN ShiftStart = CAST(  DATEADD(DAY, 1, @WEEKSTART2) AS DATETIME) THEN (CASE WHEN SHIFTSTART>CURRENT_TIMESTAMP THEN SHIFTTARGET ELSE SHIFTACTUAL END)  ELSE 0 END  AS TUE_2,
			  CASE WHEN @SHIFT_PATTERN = 'DayNight' THEN 0 ELSE CASE WHEN ShiftStart = CAST( DATEADD(DAY, 1, @WEEKSTART3) AS DATETIME)  THEN (CASE WHEN SHIFTSTART>CURRENT_TIMESTAMP THEN SHIFTTARGET ELSE SHIFTACTUAL END)  ELSE 0 END END AS TUE_3,
			  --WED																								  
			  CASE WHEN ShiftStart = CAST(  DATEADD(DAY, 2, @WEEKSTART1) AS DATETIME) THEN (CASE WHEN SHIFTSTART>CURRENT_TIMESTAMP THEN SHIFTTARGET ELSE SHIFTACTUAL END)  ELSE 0 END  AS WED_1,
			  CASE WHEN ShiftStart = CAST(  DATEADD(DAY, 2, @WEEKSTART2) AS DATETIME) THEN (CASE WHEN SHIFTSTART>CURRENT_TIMESTAMP THEN SHIFTTARGET ELSE SHIFTACTUAL END)  ELSE 0 END  AS WED_2,
			  CASE WHEN @SHIFT_PATTERN = 'DayNight' THEN 0 ELSE CASE WHEN ShiftStart  = CAST( DATEADD(DAY, 2, @WEEKSTART3) AS DATETIME)  THEN (CASE WHEN SHIFTSTART>CURRENT_TIMESTAMP THEN SHIFTTARGET ELSE SHIFTACTUAL END)  ELSE 0 END END AS WED_3,
			  --THU																							  
			  CASE WHEN ShiftStart = CAST( DATEADD(DAY, 3, @WEEKSTART1) AS DATETIME) THEN (CASE WHEN SHIFTSTART>CURRENT_TIMESTAMP THEN SHIFTTARGET ELSE SHIFTACTUAL END)  ELSE 0 END  AS THU_1,
			  CASE WHEN ShiftStart= CAST(  DATEADD(DAY, 3, @WEEKSTART2) AS DATETIME) THEN (CASE WHEN SHIFTSTART>CURRENT_TIMESTAMP THEN SHIFTTARGET ELSE SHIFTACTUAL END)  ELSE 0 END  AS THU_2,
			  CASE WHEN @SHIFT_PATTERN = 'DayNight' THEN 0 ELSE CASE WHEN ShiftStart = CAST( DATEADD(DAY, 3, @WEEKSTART3) AS DATETIME) THEN (CASE WHEN SHIFTSTART>CURRENT_TIMESTAMP THEN SHIFTTARGET ELSE SHIFTACTUAL END)  ELSE 0 END END AS THU_3,
			  --FRI																								  
			  CASE WHEN ShiftStart = CAST( DATEADD(DAY, 4, @WEEKSTART1) AS DATETIME)  THEN (CASE WHEN SHIFTSTART>CURRENT_TIMESTAMP THEN SHIFTTARGET ELSE SHIFTACTUAL END)  ELSE 0 END  AS FRI_1,
			  CASE WHEN ShiftStart = CAST( DATEADD(DAY, 4, @WEEKSTART2) AS DATETIME) THEN (CASE WHEN SHIFTSTART>CURRENT_TIMESTAMP THEN SHIFTTARGET ELSE SHIFTACTUAL END)  ELSE 0 END  AS FRI_2,
			  CASE WHEN @SHIFT_PATTERN = 'DayNight' THEN 0 ELSE CASE WHEN ShiftStart = CAST( DATEADD(DAY, 4, @WEEKSTART3) AS DATETIME)  THEN (CASE WHEN SHIFTSTART>CURRENT_TIMESTAMP THEN SHIFTTARGET ELSE SHIFTACTUAL END)  ELSE 0 END END AS FRI_3,
			  --SAT																								  
			  CASE WHEN ShiftStart = CAST( DATEADD(DAY, 5, @WEEKSTART1) AS DATETIME) THEN (CASE WHEN SHIFTSTART>CURRENT_TIMESTAMP THEN SHIFTTARGET ELSE SHIFTACTUAL END)  ELSE 0 END  AS SAT_1,
			  CASE WHEN ShiftStart = CAST( DATEADD(DAY, 5, @WEEKSTART2) AS DATETIME) THEN (CASE WHEN SHIFTSTART>CURRENT_TIMESTAMP THEN SHIFTTARGET ELSE SHIFTACTUAL END)  ELSE 0 END  AS SAT_2,
			  CASE WHEN @SHIFT_PATTERN = 'DayNight' THEN 0 ELSE CASE WHEN ShiftStart  = CAST( DATEADD(DAY, 5, @WEEKSTART3) AS DATETIME)  THEN (CASE WHEN SHIFTSTART>CURRENT_TIMESTAMP THEN SHIFTTARGET ELSE SHIFTACTUAL END)  ELSE 0 END END AS SAT_3,
			  --SUN																								 
			  CASE WHEN ShiftStart = CAST(  DATEADD(DAY, 6, @WEEKSTART1)AS DATETIME)  THEN (CASE WHEN SHIFTSTART>CURRENT_TIMESTAMP THEN SHIFTTARGET ELSE SHIFTACTUAL END)  ELSE 0 END  AS SUN_1,
			  CASE WHEN ShiftStart = CAST(  DATEADD(DAY, 6, @WEEKSTART2)AS DATETIME)  THEN (CASE WHEN SHIFTSTART>CURRENT_TIMESTAMP THEN SHIFTTARGET ELSE SHIFTACTUAL END)  ELSE 0 END  AS SUN_2,
			  CASE WHEN @SHIFT_PATTERN = 'DayNight' THEN 0 ELSE CASE WHEN ShiftStart = CAST(DATEADD(DAY, 6, @WEEKSTART3) AS DATETIME)  THEN (CASE WHEN SHIFTSTART>CURRENT_TIMESTAMP THEN SHIFTTARGET ELSE SHIFTACTUAL END)  ELSE 0 END END AS SUN_3,
			  @SHIFT_PATTERN as SHIFTPATTERN, 
			  DATENAME(DW,GETDATE()) as DAYNAME, 
			  TOTALPRODUCEDCURWEEK ,
			  RUNSTARTDT,
			  JDESCHEDDT
			FROM
				G3_PRODUCTIONSCHEDULE
			WHERE
				MESLINE IN (SELECT * FROM @TBL) AND 
				SHIFTSTART BETWEEN @BEG_DATE AND @END_DATE 

	) AS INNERTBL
GROUP BY
	ALERT,
	MACH,
	INNERTBL.DESCR,
	ITEM,
	WO,
	UOMCONV,
	SHIPDATE,
	BINS,
	LN,
	CUST,
	TOTALPRODUCEDPRWEEK,
	TOTALSCHEDULED,
	TOTALPRODUCED,
	WORKORDERID, 
	NOTES,
	INCHANGEOVER,
	ACTIVEWO,
	TOTALPRODUCEDCURWEEK,
	RUNSTARTDT,
	JDESCHEDDT

ORDER BY 
	MACH ASC , (case when RunStartDt is null then JdeSchedDt else RunStartDt end)
	--SHIPDATE, 12 desc, 13 DESC,14 DESC,15 DESC,16 DESC,17 DESC,18 DESC,19 DESC,20 DESC,21 DESC,22 DESC,23 DESC,24 DESC,25 DESC,26 DESC,27 DESC,28 DESC,29 DESC,30 DESC,31 DESC,32 DESC,33 DESC,34 DESC

END