"""
	this should create seperators between different lines
"""

	try:
		from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs, getColumnHeaders as getHeaders
		root=system.gui.getWindow("Main Windows/ProductionSchedule").getRootContainer()
		newDataSet=[]
		bool=False
		tblProd = root.getComponent('tblProd')
		headers = getHeaders(tblProd.data)
		tbl = toPyDs(tblProd.data)
		# create a row of blank entries
		blankRow = [None]*38
		if tbl:
			line = tbl[0][1]
			#check if there's any blanks (so it doesn't recursively keep calling itself)
			bool = map(lambda row : row[3]==None , tbl)		
			
			#if there's no blank lines already
			if True not in bool:
				
				"""
					loop through and if the line matches the prev line
					then add it to the dataset
					else insert a blank, reset the line, and add the line to the dataset					
				"""
				for row in tbl:
					newRow=[]
					if row[1]==line:
						newRow.extend(col for col in row)
					else:
						newDataSet.append(blankRow)
						newRow.extend(col for col in row)
						line = row[1]
				
					newDataSet.append(newRow)
				
				root.getComponent('tblProd').data = toPyDs(toDs(headers, newDataSet))		
	except:
		pass